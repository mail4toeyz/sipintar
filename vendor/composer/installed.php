<?php return array(
    'root' => array(
        'pretty_version' => 'dev-main',
        'version' => 'dev-main',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '54f4ab12ca1280901556088382d98d41138f8c65',
        'name' => '__root__',
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '54f4ab12ca1280901556088382d98d41138f8c65',
            'dev_requirement' => false,
        ),
        'brick/math' => array(
            'pretty_version' => '0.9.3',
            'version' => '0.9.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../brick/math',
            'aliases' => array(),
            'reference' => 'ca57d18f028f84f777b2168cd1911b0dee2343ae',
            'dev_requirement' => false,
        ),
        'firebase/php-jwt' => array(
            'pretty_version' => 'v6.7.0',
            'version' => '6.7.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../firebase/php-jwt',
            'aliases' => array(),
            'reference' => '71278f20b0a623389beefe87a641d03948a38870',
            'dev_requirement' => false,
        ),
        'google/access-context-manager' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.4.1',
            ),
        ),
        'google/analytics-admin' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.14.0',
            ),
        ),
        'google/analytics-data' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.10.0',
            ),
        ),
        'google/apiclient' => array(
            'pretty_version' => 'v2.15.0',
            'version' => '2.15.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../google/apiclient',
            'aliases' => array(),
            'reference' => '49787fa30b8d8313146a61efbf77ed1fede723c2',
            'dev_requirement' => false,
        ),
        'google/apiclient-services' => array(
            'pretty_version' => 'v0.304.0',
            'version' => '0.304.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../google/apiclient-services',
            'aliases' => array(),
            'reference' => '6731fd0d3e2f1ff2794f36108b55c0a3480edf3d',
            'dev_requirement' => false,
        ),
        'google/auth' => array(
            'pretty_version' => 'v1.28.0',
            'version' => '1.28.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../google/auth',
            'aliases' => array(),
            'reference' => '07f7f6305f1b7df32b2acf6e101c1225c839c7ac',
            'dev_requirement' => false,
        ),
        'google/cloud' => array(
            'pretty_version' => 'v0.208.0',
            'version' => '0.208.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../google/cloud',
            'aliases' => array(),
            'reference' => '35dc85e98b8d6d4937c5bc3b8131c4c15f5757fa',
            'dev_requirement' => false,
        ),
        'google/cloud-access-approval' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.1.0',
            ),
        ),
        'google/cloud-advisorynotifications' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.3.0',
            ),
        ),
        'google/cloud-ai-platform' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.18.0',
            ),
        ),
        'google/cloud-alloydb' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.2.1',
            ),
        ),
        'google/cloud-api-gateway' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.1.0',
            ),
        ),
        'google/cloud-api-keys' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.2.1',
            ),
        ),
        'google/cloud-apigee-connect' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.1.0',
            ),
        ),
        'google/cloud-apigee-registry' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.3.1',
            ),
        ),
        'google/cloud-appengine-admin' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.1.0',
            ),
        ),
        'google/cloud-artifact-registry' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.4.0',
            ),
        ),
        'google/cloud-asset' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.12.0',
            ),
        ),
        'google/cloud-assured-workloads' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.9.0',
            ),
        ),
        'google/cloud-automl' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.5.0',
            ),
        ),
        'google/cloud-bare-metal-solution' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.3.1',
            ),
        ),
        'google/cloud-batch' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.7.0',
            ),
        ),
        'google/cloud-beyondcorp-appconnections' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.2.1',
            ),
        ),
        'google/cloud-beyondcorp-appconnectors' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.2.1',
            ),
        ),
        'google/cloud-beyondcorp-appgateways' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.2.1',
            ),
        ),
        'google/cloud-beyondcorp-clientconnectorservices' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.2.1',
            ),
        ),
        'google/cloud-beyondcorp-clientgateways' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.2.1',
            ),
        ),
        'google/cloud-bigquery' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.25.0',
            ),
        ),
        'google/cloud-bigquery-analyticshub' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.2.0',
            ),
        ),
        'google/cloud-bigquery-connection' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.3.0',
            ),
        ),
        'google/cloud-bigquery-data-exchange' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.3.0',
            ),
        ),
        'google/cloud-bigquery-datapolicies' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.3.0',
            ),
        ),
        'google/cloud-bigquery-migration' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.3.0',
            ),
        ),
        'google/cloud-bigquery-reservation' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.2.0',
            ),
        ),
        'google/cloud-bigquery-storage' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.4.0',
            ),
        ),
        'google/cloud-bigquerydatatransfer' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.5.0',
            ),
        ),
        'google/cloud-bigtable' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.24.0',
            ),
        ),
        'google/cloud-billing' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.6.0',
            ),
        ),
        'google/cloud-billing-budgets' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.2.0',
            ),
        ),
        'google/cloud-binary-authorization' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.5.11',
            ),
        ),
        'google/cloud-build' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.7.2',
            ),
        ),
        'google/cloud-certificate-manager' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.4.1',
            ),
        ),
        'google/cloud-channel' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.5.0',
            ),
        ),
        'google/cloud-common-protos' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.4.2',
            ),
        ),
        'google/cloud-compute' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.9.0',
            ),
        ),
        'google/cloud-confidentialcomputing' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.2.0',
            ),
        ),
        'google/cloud-contact-center-insights' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.4.0',
            ),
        ),
        'google/cloud-container' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.15.0',
            ),
        ),
        'google/cloud-container-analysis' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.3.1',
            ),
        ),
        'google/cloud-core' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.51.2',
            ),
        ),
        'google/cloud-data-catalog' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.5.0',
            ),
        ),
        'google/cloud-data-fusion' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.4.0',
            ),
        ),
        'google/cloud-datacatalog-lineage' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.2.0',
            ),
        ),
        'google/cloud-dataflow' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.4.0',
            ),
        ),
        'google/cloud-dataform' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.2.0',
            ),
        ),
        'google/cloud-datalabeling' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.3.0',
            ),
        ),
        'google/cloud-dataplex' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.5.0',
            ),
        ),
        'google/cloud-dataproc' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '3.7.0',
            ),
        ),
        'google/cloud-dataproc-metastore' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.8.0',
            ),
        ),
        'google/cloud-datastore' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.22.0',
            ),
        ),
        'google/cloud-datastore-admin' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.7.0',
            ),
        ),
        'google/cloud-datastream' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.2.0',
            ),
        ),
        'google/cloud-debugger' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.5.1',
            ),
        ),
        'google/cloud-deploy' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.8.0',
            ),
        ),
        'google/cloud-dialogflow' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.5.0',
            ),
        ),
        'google/cloud-discoveryengine' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.3.0',
            ),
        ),
        'google/cloud-dlp' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.9.0',
            ),
        ),
        'google/cloud-dms' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.2.0',
            ),
        ),
        'google/cloud-document-ai' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.3.0',
            ),
        ),
        'google/cloud-domains' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.3.0',
            ),
        ),
        'google/cloud-error-reporting' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.20.0',
            ),
        ),
        'google/cloud-essential-contacts' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.3.0',
            ),
        ),
        'google/cloud-eventarc' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.1.0',
            ),
        ),
        'google/cloud-eventarc-publishing' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.4.0',
            ),
        ),
        'google/cloud-filestore' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.2.0',
            ),
        ),
        'google/cloud-firestore' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.34.0',
            ),
        ),
        'google/cloud-functions' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.3.0',
            ),
        ),
        'google/cloud-game-servers' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.1.0',
            ),
        ),
        'google/cloud-gke-backup' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.3.0',
            ),
        ),
        'google/cloud-gke-connect-gateway' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.2.0',
            ),
        ),
        'google/cloud-gke-hub' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.7.0',
            ),
        ),
        'google/cloud-gke-multi-cloud' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.4.0',
            ),
        ),
        'google/cloud-gsuite-addons' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.2.0',
            ),
        ),
        'google/cloud-iam' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.3.0',
            ),
        ),
        'google/cloud-iam-credentials' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.1.0',
            ),
        ),
        'google/cloud-iap' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.3.0',
            ),
        ),
        'google/cloud-ids' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.3.0',
            ),
        ),
        'google/cloud-iot' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.6.0',
            ),
        ),
        'google/cloud-kms' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.18.0',
            ),
        ),
        'google/cloud-kms-inventory' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.2.1',
            ),
        ),
        'google/cloud-language' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.30.0',
            ),
        ),
        'google/cloud-life-sciences' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.5.0',
            ),
        ),
        'google/cloud-logging' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.27.0',
            ),
        ),
        'google/cloud-managed-identities' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.1.0',
            ),
        ),
        'google/cloud-media-translation' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.3.0',
            ),
        ),
        'google/cloud-memcache' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.1.0',
            ),
        ),
        'google/cloud-monitoring' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.5.0',
            ),
        ),
        'google/cloud-network-connectivity' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.1.0',
            ),
        ),
        'google/cloud-network-management' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.1.0',
            ),
        ),
        'google/cloud-network-security' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.4.0',
            ),
        ),
        'google/cloud-notebooks' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.4.0',
            ),
        ),
        'google/cloud-optimization' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.3.0',
            ),
        ),
        'google/cloud-orchestration-airflow' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.2.0',
            ),
        ),
        'google/cloud-org-policy' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.5.0',
            ),
        ),
        'google/cloud-osconfig' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.1.0',
            ),
        ),
        'google/cloud-oslogin' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.5.0',
            ),
        ),
        'google/cloud-policy-troubleshooter' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.1.0',
            ),
        ),
        'google/cloud-private-catalog' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.3.0',
            ),
        ),
        'google/cloud-profiler' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.2.0',
            ),
        ),
        'google/cloud-pubsub' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.43.1',
            ),
        ),
        'google/cloud-rapidmigrationassessment' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.1.0',
            ),
        ),
        'google/cloud-recaptcha-enterprise' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.4.0',
            ),
        ),
        'google/cloud-recommendations-ai' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.6.0',
            ),
        ),
        'google/cloud-recommender' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.0',
            ),
        ),
        'google/cloud-redis' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.8.0',
            ),
        ),
        'google/cloud-resource-manager' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.6.0',
            ),
        ),
        'google/cloud-resource-settings' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.1.0',
            ),
        ),
        'google/cloud-retail' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.3.0',
            ),
        ),
        'google/cloud-run' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.5.1',
            ),
        ),
        'google/cloud-scheduler' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.9.0',
            ),
        ),
        'google/cloud-secret-manager' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.12.0',
            ),
        ),
        'google/cloud-security-center' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.16.0',
            ),
        ),
        'google/cloud-security-private-ca' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.3.0',
            ),
        ),
        'google/cloud-security-public-ca' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.2.0',
            ),
        ),
        'google/cloud-service-control' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.1.0',
            ),
        ),
        'google/cloud-service-directory' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.1.0',
            ),
        ),
        'google/cloud-service-management' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.1.0',
            ),
        ),
        'google/cloud-service-usage' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.1.0',
            ),
        ),
        'google/cloud-shell' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.1.0',
            ),
        ),
        'google/cloud-spanner' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.61.0',
            ),
        ),
        'google/cloud-speech' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.13.0',
            ),
        ),
        'google/cloud-sql-admin' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.3.0',
            ),
        ),
        'google/cloud-storage' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.31.2',
            ),
        ),
        'google/cloud-storage-transfer' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.1.0',
            ),
        ),
        'google/cloud-storageinsights' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.2.0',
            ),
        ),
        'google/cloud-support' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.1.0',
            ),
        ),
        'google/cloud-talent' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.1.0',
            ),
        ),
        'google/cloud-tasks' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.12.0',
            ),
        ),
        'google/cloud-text-to-speech' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.6.0',
            ),
        ),
        'google/cloud-tpu' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.1.0',
            ),
        ),
        'google/cloud-trace' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.6.0',
            ),
        ),
        'google/cloud-translate' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.14.0',
            ),
        ),
        'google/cloud-video-live-stream' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.4.1',
            ),
        ),
        'google/cloud-video-stitcher' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.6.1',
            ),
        ),
        'google/cloud-video-transcoder' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.8.0',
            ),
        ),
        'google/cloud-videointelligence' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.13.0',
            ),
        ),
        'google/cloud-vision' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.7.0',
            ),
        ),
        'google/cloud-vm-migration' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.4.1',
            ),
        ),
        'google/cloud-vmware-engine' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.2.1',
            ),
        ),
        'google/cloud-vpc-access' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.1.0',
            ),
        ),
        'google/cloud-web-risk' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.4.0',
            ),
        ),
        'google/cloud-web-security-scanner' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.7.15',
            ),
        ),
        'google/cloud-workflows' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.2.16',
            ),
        ),
        'google/common-protos' => array(
            'pretty_version' => 'v4.1.0',
            'version' => '4.1.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../google/common-protos',
            'aliases' => array(),
            'reference' => '4c74dd56310b835115b939a561f7c1f0e3b36364',
            'dev_requirement' => false,
        ),
        'google/crc32' => array(
            'pretty_version' => 'v0.2.0',
            'version' => '0.2.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../google/crc32',
            'aliases' => array(),
            'reference' => '948f7945d803dcc1a375152c72f63144c2dadf23',
            'dev_requirement' => false,
        ),
        'google/gax' => array(
            'pretty_version' => 'v1.21.0',
            'version' => '1.21.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../google/gax',
            'aliases' => array(),
            'reference' => '1462f84ab92acf6aa329288ede750b5e33b78318',
            'dev_requirement' => false,
        ),
        'google/grafeas' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.8.0',
            ),
        ),
        'google/grpc-gcp' => array(
            'pretty_version' => 'v0.3.0',
            'version' => '0.3.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../google/grpc-gcp',
            'aliases' => array(),
            'reference' => '4d8b455a45a89f57e1552cadc41a43bc34c40456',
            'dev_requirement' => false,
        ),
        'google/longrunning' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '0.2.6',
            ),
        ),
        'google/protobuf' => array(
            'pretty_version' => 'v3.23.3',
            'version' => '3.23.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../google/protobuf',
            'aliases' => array(),
            'reference' => 'e611e00ffc99123fcbddc77774f26c813ead9294',
            'dev_requirement' => false,
        ),
        'grpc/grpc' => array(
            'pretty_version' => '1.52.0',
            'version' => '1.52.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../grpc/grpc',
            'aliases' => array(),
            'reference' => '98394cd601a587ca68294e6209bd713856969105',
            'dev_requirement' => false,
        ),
        'guzzlehttp/guzzle' => array(
            'pretty_version' => '7.7.0',
            'version' => '7.7.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/guzzle',
            'aliases' => array(),
            'reference' => 'fb7566caccf22d74d1ab270de3551f72a58399f5',
            'dev_requirement' => false,
        ),
        'guzzlehttp/promises' => array(
            'pretty_version' => '1.5.3',
            'version' => '1.5.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/promises',
            'aliases' => array(),
            'reference' => '67ab6e18aaa14d753cc148911d273f6e6cb6721e',
            'dev_requirement' => false,
        ),
        'guzzlehttp/psr7' => array(
            'pretty_version' => '2.5.0',
            'version' => '2.5.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/psr7',
            'aliases' => array(),
            'reference' => 'b635f279edd83fc275f822a1188157ffea568ff6',
            'dev_requirement' => false,
        ),
        'monolog/monolog' => array(
            'pretty_version' => '2.9.1',
            'version' => '2.9.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../monolog/monolog',
            'aliases' => array(),
            'reference' => 'f259e2b15fb95494c83f52d3caad003bbf5ffaa1',
            'dev_requirement' => false,
        ),
        'paragonie/constant_time_encoding' => array(
            'pretty_version' => 'v2.6.3',
            'version' => '2.6.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../paragonie/constant_time_encoding',
            'aliases' => array(),
            'reference' => '58c3f47f650c94ec05a151692652a868995d2938',
            'dev_requirement' => false,
        ),
        'paragonie/random_compat' => array(
            'pretty_version' => 'v9.99.100',
            'version' => '9.99.100.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../paragonie/random_compat',
            'aliases' => array(),
            'reference' => '996434e5492cb4c3edcb9168db6fbb1359ef965a',
            'dev_requirement' => false,
        ),
        'phpseclib/phpseclib' => array(
            'pretty_version' => '3.0.20',
            'version' => '3.0.20.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpseclib/phpseclib',
            'aliases' => array(),
            'reference' => '543a1da81111a0bfd6ae7bbc2865c5e89ed3fc67',
            'dev_requirement' => false,
        ),
        'psr/cache' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/cache',
            'aliases' => array(),
            'reference' => 'd11b50ad223250cf17b86e38383413f5a6764bf8',
            'dev_requirement' => false,
        ),
        'psr/http-client' => array(
            'pretty_version' => '1.0.2',
            'version' => '1.0.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-client',
            'aliases' => array(),
            'reference' => '0955afe48220520692d2d09f7ab7e0f93ffd6a31',
            'dev_requirement' => false,
        ),
        'psr/http-client-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/http-factory' => array(
            'pretty_version' => '1.0.2',
            'version' => '1.0.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-factory',
            'aliases' => array(),
            'reference' => 'e616d01114759c4c489f93b099585439f795fe35',
            'dev_requirement' => false,
        ),
        'psr/http-factory-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/http-message' => array(
            'pretty_version' => '1.1',
            'version' => '1.1.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-message',
            'aliases' => array(),
            'reference' => 'cb6ce4845ce34a8ad9e68117c10ee90a29919eba',
            'dev_requirement' => false,
        ),
        'psr/http-message-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/log' => array(
            'pretty_version' => '1.1.4',
            'version' => '1.1.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/log',
            'aliases' => array(),
            'reference' => 'd49695b909c3b7628b6289db5479a1c204601f11',
            'dev_requirement' => false,
        ),
        'psr/log-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0.0 || 2.0.0 || 3.0.0',
            ),
        ),
        'ralouphie/getallheaders' => array(
            'pretty_version' => '3.0.3',
            'version' => '3.0.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../ralouphie/getallheaders',
            'aliases' => array(),
            'reference' => '120b605dfeb996808c31b6477290a714d356e822',
            'dev_requirement' => false,
        ),
        'ramsey/collection' => array(
            'pretty_version' => '1.3.0',
            'version' => '1.3.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../ramsey/collection',
            'aliases' => array(),
            'reference' => 'ad7475d1c9e70b190ecffc58f2d989416af339b4',
            'dev_requirement' => false,
        ),
        'ramsey/uuid' => array(
            'pretty_version' => '4.2.3',
            'version' => '4.2.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../ramsey/uuid',
            'aliases' => array(),
            'reference' => 'fc9bb7fb5388691fd7373cd44dcb4d63bbcf24df',
            'dev_requirement' => false,
        ),
        'rhumsaa/uuid' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '4.2.3',
            ),
        ),
        'rize/uri-template' => array(
            'pretty_version' => '0.3.5',
            'version' => '0.3.5.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../rize/uri-template',
            'aliases' => array(),
            'reference' => '5ed4ba8ea34af84485dea815d4b6b620794d1168',
            'dev_requirement' => false,
        ),
        'symfony/deprecation-contracts' => array(
            'pretty_version' => 'v2.5.2',
            'version' => '2.5.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/deprecation-contracts',
            'aliases' => array(),
            'reference' => 'e8b495ea28c1d97b5e0c121748d6f9b53d075c66',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-ctype' => array(
            'pretty_version' => 'v1.27.0',
            'version' => '1.27.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-ctype',
            'aliases' => array(),
            'reference' => '5bbc823adecdae860bb64756d639ecfec17b050a',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php80' => array(
            'pretty_version' => 'v1.27.0',
            'version' => '1.27.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php80',
            'aliases' => array(),
            'reference' => '7a6ff3f1959bb01aefccb463a0f2cd3d3d2fd936',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php81' => array(
            'pretty_version' => 'v1.27.0',
            'version' => '1.27.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php81',
            'aliases' => array(),
            'reference' => '707403074c8ea6e2edaf8794b0157a0bfa52157a',
            'dev_requirement' => false,
        ),
    ),
);
