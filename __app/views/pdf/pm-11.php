<?php
  $levels = array('', 'RAUDHATUL ATHFAL', 'MADRASAH IBTIDAIYAH', 'MADRASAH TSANAWIYAH', 'MADRASAH ALIYAH');
  $daerah = ((preg_match('/^KOTA /',$this->Reff->get_kondisi(array("id"=>$madrasah->kota_id),"kota","nama"))?'':'KABUPATEN ').$this->Reff->get_kondisi(array("id"=>$madrasah->kota_id),"kota","nama")).' PROVINSI '.$this->Reff->get_kondisi(array("id"=>$madrasah->provinsi_id),"provinsi","nama");

  $provinsi =$this->Reff->get_kondisi(array("id"=>$madrasah->provinsi_id),"provinsi","nama");
?>

<?php
$documentroot = $this->Reff->documentroot();
//$documentroot = base_url();
$kantor         = $this->Reff->get_where("tr_kantor",array("linkid"=>$madrasah->provinsi_id));
?>

<?php 
   $piagam = $this->db->get_where("tr_piagam",array("tmyayasan_id"=>$yayasan->id))->row();
 $level_print ="Madrasah";  

  ?>
<html>
<head>

</head>
<body>
<div style="display: block;width:155mm; min-height:250mm; padding-left: 8.3mm;  padding-top: 30mm; line-height: 100%;">
<p align="center; font-size: 16px;font-weight:bold">KEMENTERIAN AGAMA REPUBLIK INDONESIA</p>
<p align="center; font-size: 14px;font-weight:bold">PIAGAM IZIN OPERASIONAL MADRASAH</p>
<p style="margin-top:-10; text-align:center; font-size: 14px;font-weight:bold">Nomor : <?php echo (strtoupper($piagam->nomor_piagam)); ?></p>
<p align="center"><b>Diberikan kepada:</b></p>
<p></p>
<div style="display: block; padding-left: 40px; width: 100%;font-size:11px;font-weight:bold" id="content">
<table>
<tbody>
<tr>
<td style="width: 46mm">Nama Madrasah</td>
<td style="width: 2mm">&nbsp;:&nbsp;</td>
<td style="width: 100mm"><?php echo strtoupper($this->Reff->namajenjang($madrasah->jenjang)).' '.strtoupper($madrasah->nama); ?></td>
</tr>
<tr>
<td style="width: 46mm">Alamat</td>
<td style="width: 2mm">&nbsp;:&nbsp;</td>
<td style="width: 100mm"><?php echo strtoupper($madrasah->alamat); ?></td>
</tr>
<tr>
<td style="width: 46mm">Desa/Kelurahan</td>
<td style="width: 2mm">&nbsp;:&nbsp;</td>
<td style="width: 100mm"><?php echo $this->Reff->get_kondisi(array("id"=>$madrasah->desa_id),"desa","nama"); ?></td>
</tr>
<tr>
<td style="width: 46mm">Kecamatan</td>
<td style="width: 2mm">&nbsp;:&nbsp;</td>
<td style="width: 100mm"><?php echo $this->Reff->get_kondisi(array("id"=>$madrasah->kecamatan_id),"kecamatan","nama"); ?></td>
</tr>
<tr>
<td style="width: 46mm">Kabupaten/Kota</td>
<td style="width: 2mm">&nbsp;:&nbsp;</td>
<td style="width: 100mm"><?php echo (((preg_match('/^KOTA /',$this->Reff->get_kondisi(array("id"=>$madrasah->kota_id),"kota","nama"))?'':'KABUPATEN ').$this->Reff->get_kondisi(array("id"=>$madrasah->kota_id),"kota","nama"))); ?></td>
</tr>
<tr>
<td style="width: 46mm">Provinsi</td>
<td style="width: 2mm">&nbsp;:&nbsp;</td>
<td style="width: 100mm"><?php echo $provinsi; ?></td>
</tr>
<tr>
<td style="width: 46mm">Penyelenggara Madrasah</td>
<td style="width: 2mm">&nbsp;:&nbsp;</td>
<td style="width: 100mm"><?php echo strtoupper($yayasan->nama); ?></td>
</tr>
<tr>
<td style="width: 46mm">Akte Notaris Penyelenggara</td>
<td style="width: 2mm">&nbsp;:&nbsp;</td>
<td style="width: 100mm">No. <?php echo $yayasan->notaris_no.' '.$yayasan->notaris_nama.' Tanggal '.$this->Reff->formattanggalstring($yayasan->notaris_tgl); ?></td>
</tr>
<tr>
<td style="width: 46mm">Pengesahan Akte Notaris</td>
<td style="width: 2mm">&nbsp;:&nbsp;</td>
<td style="width: 100mm"><?php 
$skumhamno = trim($yayasan->menkumham_sk);
$skumhamtgl = $yayasan->menkumham_tgl;

echo $skumhamno.' Tanggal '.$this->Reff->formattanggalstring($skumhamtgl); ?></td>
</tr>
<tr>
<td style="width: 46mm">Berdiri Sejak</td>
<td style="width: 2mm">&nbsp;:&nbsp;</td>
<td style="width: 100mm"><?php echo strtoupper($this->Reff->formattanggalstring($madrasah->tgl_berdiri)); ?></td>
</tr>
</tbody>
</table>
<!--
<?php
list($n1,$n2,$n3,$n4,$n5,$n6,$n7,$n8,$n9,$na,$nb,$nc) = str_split($madrasah->nsm);
?>
-->
<br>
<p>Dengan Nomor Statistik Madrasah (NSM): <br /></p>
<p align="center">
<center>
<table border="1" cellspacing="0" cellpadding="0">
<tbody>
<tr valign="middle">
<td align="center" style="width:10mm;height:6mm;"><?php echo $n1; ?></td>
<td align="center" style="width:10mm;"><?php echo $n2; ?></td>
<td align="center" style="width:10mm;"><?php echo $n3; ?></td>
<td align="center" style="width:10mm;"><?php echo $n4; ?></td>
<td align="center" style="width:10mm;"><?php echo $n5; ?></td>
<td align="center" style="width:10mm;"><?php echo $n6; ?></td>
<td align="center" style="width:10mm;"><?php echo $n7; ?></td>
<td align="center" style="width:10mm;"><?php echo $n8; ?></td>
<td align="center" style="width:10mm;"><?php echo $n9; ?></td>
<td align="center" style="width:10mm;"><?php echo $na; ?></td>
<td align="center" style="width:10mm;"><?php echo $nb; ?></td>
<td align="center" style="width:10mm;"><?php echo $nc; ?></td>
</tr>
</tbody>
</table>
</center>
</p>
<br><br><br><br><br><br>
<table style="width:100%" >
<tbody>
<tr valign="top" ">
<td align="right" style="width:50%;">&nbsp;&nbsp;</td>
<td align="justify" style="margin-right:10px"><?php echo $kantor->nama_kota; ?>,&nbsp;<?php echo $this->Reff->formattanggalstring($piagam->tanggal); ?></td>
</tr>
<tr valign="top">
<td align="right"></td>
<td align="justify">KEPALA KANTOR WILAYAH<br />KEMENTERIAN AGAMA<br /><span> PROVINSI <?php echo $provinsi; ?></span></td>
</tr>


<tr>
<td rowspan="5" style="width:70%"><img src="<?php echo $documentroot; ?>/__statics/upload/qrcode/<?php echo $yayasan->id; ?>.png" style="width: 30mm;"></td>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
</tr>
<tr>
<td>&nbsp;</td>
</tr>
<tr>
<td><?php echo $kantor->kepala_nama; ?></td>
</tr>
</tbody>
</table>
</div>
</div>
</body>
</html>
