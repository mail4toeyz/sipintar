
<style>

#bodinya { margin-right: 40px;
  margin-left: 60px; 
  font-family: Bookman Old Style;
font-size: 16px;
  }

</style>
<?php 
$kantor         = $this->Reff->get_where("tr_kantor",array("linkid"=>$madrasah->kota_id));
  $surat = $this->db->get_where("tr_surat",array("tmyayasan_id"=>$yayasan->id,"jenis"=>"vl"))->row();
  ?>
<div id="bodinya">
<p>LAMPIRAN BERITA ACARA VERIFIKASI LAPANGAN<br />PERMOHONAN IZIN PENDIRIAN MADRASAH</p>
<br>
<p align="center">LAPORAN HASIL VERIFIKASI LAPANGAN</p>
<table style="width:100%;" class="mceItemTable">
<tr valign="top">
<td style="width: 45mm;">Nama Calon Madrasah</td>
<td style="width: 5mm;">:</td>
<td style="width: 103mm;"><?php echo $this->Reff->jenjangsingkat($madrasah->jenjang).' '.$madrasah->nama; ?></td>
</tr>
<tr valign="top">
<td style="width: 45mm;">Alamat Calon Madrasah</td>
<td style="width: 5mm;">:</td>
<td style="width: 103mm;"><?php echo ($madrasah->alamat); ?>,&nbsp;Desa/Kelurahan&nbsp;
            <?php echo ucwords(strtolower($this->Reff->get_kondisi(array("id"=>$madrasah->desa_id),"desa","nama")));  ?> Kecamatan
            <?php echo ucwords(strtolower($this->Reff->get_kondisi(array("id"=>$madrasah->kecamatan_id),"kecamatan","nama")));  ?>&nbsp;
            <?php echo ucwords(strtolower((preg_match('/^KOTA /',$this->Reff->get_kondisi(array("id"=>$madrasah->kota_id),"kota","nama"))?'':'KABUPATEN ').$this->Reff->get_kondisi(array("id"=>$madrasah->kota_id),"kota","nama"))); ?>&nbsp;<?php echo ucwords(strtolower($this->Reff->get_kondisi(array("id"=>$madrasah->provinsi_id),"provinsi","nama"))); ?>
 </td> 
  </tr>
<tr valign="top">
<td style="width: 45mm;">Nama Calon Penyelenggara</td>
<td style="width: 5mm;">:</td>
<td style="width: 103mm;"><?php echo $yayasan->nama; ?></td>
</tr>
<tr valign="top">
<td style="width: 45mm;">Akte Notaris</td>
<td style="width: 5mm;">:</td>
<td style="width: 103mm;">No. <?php echo $yayasan->notaris_no.' '.$yayasan->notaris_nama.' Tanggal '.$this->Reff->formattanggalstring($yayasan->notaris_tgl); ?></td>
</tr>
<tr valign="top">
<td style="width: 45mm;">Pengesahan Akte Notaris</td>
<td style="width: 5mm;">:</td>
<td style="width: 103mm;">
<?php 
$skumhamno = trim($yayasan->menkumham_sk);
$skumhamtgl = $yayasan->menkumham_tgl;

echo $skumhamno.' Tanggal '.$this->Reff->formattanggalstring($skumhamtgl); ?>

</td>
</tr>

</table>
<br>
<p>A. Persyaratan Administratif</p>
<table  width="100%" border="1" style="border-collapse: collapse;line-height: 1;">
<tr>
<th width="5%" align="center">No</th>
<th width="35%">Persyaratan yang<br>diverifikasi</th>
<th width="50%">Indikator/Keterangan</th>
<th width="10%">Skor</th>
</tr>
<tr>
<td valign="top" valign="top" align="center">1</td>
<td valign="top" >Dokumen Akte Notaris Lembaga Penyelenggara Pendidikan:<br /> 1. Ada<br /> 0. Tidak ada</td>
<td class="indikator">
<u>Catatan:</u><br> <u>Ada:</u> Jika terdapat bukti dokumen asli Akte Notaris yang telah mendapat pengesahan dari Kemkumham RI/sesuai ketentuan peraturan perundang-undangan yang berlaku<br>
<u>Tidak Ada:</u> Jika tidak terdapat bukti dokumen asli Akte Notaris yang telah mendapat pengesahan dari Kemkumham/sesuai ketentuan peraturan perundang-undangan yang berlaku&nbsp;</td>
<td class="skor"></td>
</tr>
<tr>
<td valign="top" align="center">2</td>
<td valign="top">SK Struktur Organisasi dan Susunan Pengurus lembaga calon penyelenggara:<br />1. Ada<br />0. Tidak ada</td>
<td class="indikator">
<u>Catatan:</u><br> <u>Ada:</u> Jika terdapat bukti dokumen asli SK Struktur Organisasi dan Susunan Pengurus lembaga calon penyelenggara<br>
<u>Tidak Ada:</u> Jika tidak terdapat bukti dokumen asli SK Struktur Organisasi dan Susunan Pengurus lembaga calon penyelenggara
</td>
<td class="skor"></td>
</tr>

<tr style="border-bottom:1px solid black">
<td valign="top" align="center">3</td>
<td valign="top">KTP Pengurus lembaga calon penyelenggara:<br />1. Ada<br />0. Tidak ada</td>
<td class="indikator"><u>Catatan:</u><br> <u>Ada:</u> Jika terdapat bukti dokumen asli KTP Pengurus Lembaga Calon Penyelenggara&nbsp;<br>
<u>Tidak Ada:</u> Jika tidak terdapat bukti dokumen asli KTP Pengurus Lembaga Calon Penyelenggara&nbsp;</td>
<td class="skor"></td>
</tr>

<tr>
<td valign="top" width="5%" align="center">4</td>
<td valign="top" width="35%">Dokumen Anggaran Dasar/Anggaran Rumah Tangga (AD/ART) lembaga calon penyelenggara:<br />1. Ada<br />0. Tidak ada</td>
<td class="indikator" width="50%">
<u>Catatan:</u><br> <u>Ada:</u> Jika terdapat bukti dokumen asli dokumen Anggaran Dasar/Anggaran Rumah Tangga (AD/ART) lembaga calon penyelenggara<br>
<u>Tidak Ada:</u> Jika tidak terdapat bukti dokumen asli dokumen Anggaran Dasar/Anggaran Rumah Tangga (AD/ART) lembaga calon penyelenggara</td>
<td class="skor" width="10%"></td>
</tr>
</table>

<table  width="100%" border="1" style="border-collapse: collapse; line-height: 1;">
<tr>
<td valign="top" width="5%" align="center">5</td>
<td valign="top" width="35%">SK Struktur Manajemen dan Personalia Madrasah yang akan didirikan:<br />1. Ada<br />0. Tidak ada</td>
<td class="indikator" width="50%">
<u>Catatan:</u><br> <u>Ada:</u> Jika terdapat bukti dokumen asli SK Manajemen dan Personalia Madrasah yang akan didirikan<br>
<u>Tidak Ada:</u> Jika tidak terdapat bukti dokumen asli SK Manajemen dan Personalia Madrasah yang akan didirikan</td>
<td class="skor" width="10%"></td>
</tr>
<tr>
<td valign="top" align="center">6</td>
<td valign="top">Surat Pernyataan Kesanggupan Pembiayaan Pendidikan:<br />1. Meyakinkan<br />0. Tidak Meyakinkan</td>
<td class="indikator">
Catatan:<br> Periksa dokumen asli buku rekening atas nama lembaga dan aset atau bukti lainnya yang menunjukkan kesanggupan pembiayaan penyelenggaraan pendidikan minimal 1 tahun pelajaran berikutnya.<br>
<u>Meyakinkan:</u> Jika mempunyai jaminan sumber pendanaan penyelenggaraan pendidikan madrasah baik berupa uang yang disimpan di rekening atas nama lembaga ataupun aset lainnya yang menunjukkan jaminan pendanaan.<br>
<u>Tidak Meyakinkan:</u> Jika tidak mempunyai jaminan sumber pendanaan penyelenggaraan pendidikan madrasah baik berupa uang yang disimpan di rekening atas nama lembaga ataupun aset lainnya yang menunjukkan jaminan pendanaan.
</td>
<td class="skor"></td>
</tr>
</table>
<br>

</div>
<div style="page-break-before: always;"></div>

<div id="bodinya">
<p>B. Persyaratan Teknis</p>
<table  width="100%" border="1" style="border-collapse:collapse;line-height: 1;">
<tr>
<th width="5%" align="center">No</th>
<th width="35%">Persyaratan yang<br>diverifikasi</th>
<th width="50%">Indikator/Keterangan</th>
<th width="10%">Skor</th>
</tr>


<tr>
<td valign="top" align="center" align="center">1</td>
<td valign="top">Dokumen Kurikulum:<br />1. Lengkap<br />0. Tidak Lengkap</td>
<td class="indikator">
<u>Lengkap:</u> Jika terdapat 1 set dokumen kurikulum yang meliputi standar kompetensi lulusan, standar isi, standar proses, standar penilaian pendidikan, kerangka dasar kurikulum, dan kurikulum tingkat satuan pendidikan.<br>
<u>Tidak Lengkap:</u> Jika tidak terdapat dokumen kurikulum yang meliputi standar kompetensi lulusan, standar isi, standar proses, standar penilaian pendidikan, kerangka dasar kurikulum, dan kurikulum tingkat satuan pendidikan</td>
<td class="skor"></td>
</tr>
<tr>
<td valign="top" align="center">2</td>
<td valign="top">Dokumen Rencana Induk Pengembangan Madrasah yang dilengkapi dengan Master Plan Pengembangan Madrasah:<br>1. Lengkap<br>0. Tidak Lengkap</td>
<td class="indikator">
<u>Lengkap:</u> Jika terdapat 1 set dokumen Rencana Induk Pengembangan Madrasah.<br>
<u>Tidak Lengkap:</u> Jika tidak terdapat 1 set dokumen Rencana Induk Pengembangan Madrasah yang dilengkapi dengan <i>Master Plan</i> Pengembangan Madrasah.</td>
<td class="skor"></td>
</tr>
<tr>
<td valign="top" align="center">3</td>
<td valign="top">Jumlah minimal guru<br />1. Memenuhi Syarat<br />0. Tidak Memenuhi Syarat</td>
<td class="indikator">
Catatan:<br />Persyaratan jumlah minimal guru:
<table border="1" style="border-collapse: collapse; width: 68mm;">
<tr valign="top" >
<td style="width: 12mm;">RA</td>
<td style="width: 60mm;">1 Orang/rombel</td>
</tr>
<tr valign="top">
<td>MI</td>
<td style="width: 60mm;">1 orang /rombel ditambah<br />1 orang guru PAI dan Penjaskes</td>
</tr>
<tr valign="top">
<td>MTs</td>
<td style="width: 60mm;">1 orang guru untuk setiap matapelajaran</td>
</tr>
<tr valign="top">
<td>MA</td>
<td style="width: 60mm;">1 orang guru untuk setiap matapelajaran</td>
</tr>
<tr valign="top">
<td>MAK</td>
<td style="width: 60mm;">
1 orang guru untuk setiap matapelajaran<br>
1 (satu) orang instruktur sesuai dengan bidang kejuruan yang diselenggarakan
</td>
</tr>
</table>
<u>Memenuhi Syarat:</u> Jika mempunyai jumlah minimal guru sesuai dengan ketentuan jenjang pendidikan yang akan didirikan di atas<br>
<u>Tidak Memenuhi Syarat:</u> Jika tidak mempunyai jumlah minimal guru sesuai dengan ketentuan jenjang pendidikan yang akan didirikan di atas
</td>
<td class="skor"></td>
</tr>
<tr>
<td valign="top" align="center">4</td>
<td valign="top">Prosentase Kualifikasi Minimal Guru<br />1. Memenuhi syarat<br />0. Tidak memenuhi syarat</td>
<td class="indikator">
Catatan:<br />Persyaratan prosentase kualifikasi minimal guru:<br>
<table border="1" style="border-collapse: collapse; width: 68mm;">
<tr valign="top">
<td width="34mm">RA</td>
<td width="34mm">50%</td>
</tr>
<tr valign="top">
<td>MI</td>
<td>80%</td>
</tr>
<tr valign="top">
<td>MTs</td>
<td>100%</td>
</tr>
<tr valign="top">
<td>MA</td>
<td>100%</td>
</tr>
<tr valign="top">
<td>MAK</td>
<td>100%</td>
</tr>
</table>
</td>
<td class="skor"></td>
</tr>
</table>

<table  width="100%" border="1" style="border-collapse: collapse;  line-height: 1;">


<tr>
<td valign="top" width="5%"></td>
<td valign="top" width="35%" ></td>
<td class="indikator" width="50%">
<u>Memenuhi Syarat:</u> Jika mempunyai prosentase kualifikasi minimal guru sesuai dengan ketentuan jenjang pendidikan yang akan didirikan di atas<br>
<u>Tidak Memenuhi Syarat:</u> Jika tidak mempunyai jumlah minimal guru sesuai dengan ketentuan jenjang pendidikan yang akan didirikan di atas
</td>
<td class="skor" width="10%"></td>
</tr>


<tr>
<td valign="top" align="center">5</td>
<td valign="top">Kualifikasi minimal Kepala Madrasah<br />1. Memenuhi syarat<br />0. Tidak memenuhi syarat</td>
<td class="indikator">
Catatan:<br />Kualifikasi minimal Kepala Madrasah RA, MI, MTs, MA, dan MAK adalah S1 dari Perguruan Tinggi yang Terakreditasi<br>
<u>Memenuhi Syarat:</u> Jika kualifikasi minimal Kepala Madrasah minimal S1<br>
<u>Tidak Memenuhi Syarat:</u> Jika kualifikasi minimal Kepala Madrasah kurang dari S1</td>
<td class="skor"></td>
</tr>
<tr>
<td valign="top" align="center">6</td>
<td valign="top">Jumlah minimal tenaga administrasi<br />1. Memenuhi syarat<br />0. Tidak memenuhi syarat</td>
<td class="indikator">
Catatan: Jumlah minimal tenaga administrasi madrasah adalah 1 (satu) orang per satuan pendidikan<br>
<u>Memenuhi Syarat:</u> Jika jumlah minimal tenaga administrasi madrasah adalah <u>&gt;</u> 1<br>
<u>Tidak Memenuhi Syarat:</u> Jika jumlah minimal tenaga administrasi madrasah adalah &lt; 1</td>
<td class="skor"></td>
</tr>
<tr>
<td valign="top" align="center">7</td>
<td valign="top">Kualifikasi minimal tenaga administrasi<br />1. Memenuhi syarat<br />0. Tidak memenuhi syarat</td>
<td class="indikator">
Catatan:<br />Kualifikasi minimal tenaga administrasi madrasah:
<table border="1" style="border-collapse: collapse; width: 68mm;">
<tr valign="top">
<td width="12mm">RA</td>
<td width="54mm">MA/SMA/sederajat</td>
</tr>
<tr valign="top">
<td>MI</td>
<td>MA/SMA/sederajat</td>
</tr>
<tr valign="top">
<td>MTs</td>
<td>MA/SMA/sederajat</td>
</tr>
<tr valign="top">
<td>MA</td>
<td>D3</td>
</tr>
<tr valign="top">
<td>MAK</td>
<td>D3</td>
</tr>
</table>
<u>Memenuhi Syarat:</u> Jika kualifikasi minimal tenaga administrasi madrasah sesuai dengan tabel di atas<br>
<u>Tidak Memenuhi Syarat:</u> Jika kualifikasi minimal tenaga administrasi madrasah sesuai dengan tabel di atas
</td>
<td class="skor"></td>
</tr>
<tr>
<td valign="top" align="center">8</td>
<td valign="top">Luas tanah/lahan minimal<br />1. Memenuhi syarat<br />0. Tidak memenuhi syarat</td>
<td class="indikator">
Catatan:<br />Luas tanah/lahan minimal:
<table border="1" style="border-collapse: collapse; width: 68mm;">
<tr valign="top">
<td width="34mm">RA</td>
<td width="34mm">300 m2</td>
</tr>
<tr valign="top">
<td>MI</td>
<td>790 m2</td>
</tr>
<tr valign="top">
<td>MTs</td>
<td>1440 m2</td>
</tr>
<tr valign="top">
<td>MA</td>
<td>2170 m2</td>
</tr>
<tr valign="top">
<td>MAK</td>
<td>2170 m2</td>
</tr>
</table>
<u>Memenuhi Syarat:</u> Jika luas tanah/lahan minimal sesuai dengan tabel di atas<br>
<u>Tidak Memenuhi Syarat:</u> Jika luas tanah/lahan minimal sesuai dengan tabel di atas
</td>
<td class="skor"></td>
</tr>

</table>

<table  width="100%" border="1" style="border-collapse: collapse;  line-height: 1;">


<tr>
<td valign="top" width="5%" align="center">9</td>
<td valign="top" width="35%">Jumlah minimal ruang kelas<br />1. Memenuhi syarat<br />0. Tidak memenuhi syarat</td>
<td class="indikator" width="50%">
Catatan:<br />Jumlah minimal ruang kelas adalah:
<table border="1" style="border-collapse: collapse; width: 68mm;">
<tr valign="top">
<td width="34mm">RA</td>
<td width="34mm">2 unit</td>
</tr>
<tr valign="top">
<td>MI</td>
<td>3 unit</td>
</tr>
<tr valign="top">
<td>MTs</td>
<td>3 unit</td>
</tr>
<tr valign="top">
<td>MA</td>
<td>3 unit</td>
</tr>
<tr valign="top">
<td>MAK</td>
<td>3 unit</td>
</tr>
</table>
<u>Memenuhi Syarat:</u> Jika jumlah minimal ruang kelas sesuai dengan tabel di atas<br>
<u>Tidak Memenuhi Syarat:</u> Jika jumlah minimal ruang kelas sesuai dengan tabel di atas
</td>
<td class="skor" width="10%"></td>
</tr>
<tr>
<td valign="top" align="center">10</td>
<td valign="top">Jumlah Ruang Kepala Madrasah<br />1. Memenuhi syarat<br />0. Tidak memenuhi syarat</td>
<td class="indikator">
Catatan:<br />Setiap madrasah wajib mempunyai 1 (satu) ruang Kepala Madrasah.<br>
<u>Memenuhi Syarat:</u> Jika madrasah mempunyai satu ruang Kepala Madrasah<br>
<u>Tidak Memenuhi Syarat:</u> Jika madrasah tidak mempunyai ruang Kepala Madrasah&nbsp;</td>
<td class="skor"></td>
</tr>
<tr>
<td valign="top" align="center">11</td>
<td valign="top">Ruang guru<br />1. Memenuhi syarat<br />0. Tidak memenuhi syarat</td>
<td class="indikator">
Catatan:<br />Setiap madrasah wajib mempunyai minimal 1 (satu) ruang guru.<br>
<u>Memenuhi Syarat:</u> Jika madrasah mempunyai minimal satu ruang guru<br>
<u>Tidak Memenuhi Syarat:</u> Jika madrasah tidak mempunyai minimal satu ruang guru</td>
<td class="skor"></td>
</tr>
<tr>
<td valign="top" align="center">12</td>
<td valign="top">Ruang tata usaha<br />1. Memenuhi syarat<br />0. Tidak memenuhi syarat</td>
<td class="indikator">
Catatan:<br />Setiap madrasah wajib mempunyai minimal 1 (satu) ruang tata usaha.<br>
<u>Memenuhi Syarat:</u> Jika madrasah mempunyai minimal satu ruang tata usaha<br>
<u>Tidak Memenuhi Syarat:</u> Jika madrasah tidak mempunyai minimal satu ruang tata usaha</td>
<td class="skor"></td>
</tr>
<tr>
<td valign="top" align="center">13</td>
<td valign="top">Tempat beribadah<br />1. Memenuhi syarat<br />0. Tidak memenuhi syarat</td>
<td class="indikator">
Catatan:<br />Setiap madrasah wajib mempunyai minimal satu tempat beribadah<br>
<u>Memenuhi Syarat:</u> Jika madrasah mempunyai minimal satu ruang tempat beribadah<br>
<u>Tidak Memenuhi Syarat:</u> Jika madrasah tidak mempunyai minimal satu ruang tempat beribadah</td>
<td class="skor"></td>
</tr>
<tr>
<td valign="top" align="center">14</td>
<td valign="top">Jumlah minimal toilet peserta didik dan guru dan tenaga kependidikan<br />1. Memenuhi syarat<br />0. Tidak memenuhi syarat</td>
<td class="indikator">
Catatan:<br />Jumlah minimal toilet:
<table border="1" style="border-collapse: collapse; width: 68mm;">
<tr valign="top">
<td width="34mm">RA</td>
<td width="34mm">1 buah</td>
</tr>
<tr valign="top">
<td>MI</td>
<td>1 buah</td>
</tr>
<tr valign="top">
<td>MTs</td>
<td>1 buah</td>
</tr>
<tr valign="top">
<td>MA</td>
<td>1 buah</td>
</tr>
<tr valign="top">
<td>MAK</td>
<td>1 buah</td>
</tr>
</table>
<u>Memenuhi Syarat:</u> Jika jumlah minimal toilet sesuai dengan tabel di atas<br>
<u>Tidak Memenuhi Syarat:</u> Jika jumlah minimal toilet tidak sesuai dengan tabel di atas
</td>
<td class="skor"></td>
</tr>
</table>

<table  width="100%" border="1" style="border-collapse: collapse;  line-height: 1;">



<tr>
<td valign="top" align="center" width="5%">15</td>
<td valign="top" width="35%">Sarana minimal bermain/berolah raga<br />1. Memenuhi syarat<br />0. Tidak memenuhi syarat</td>
<td class="indikator" width="50%">
Catatan:<br />Luas sarana minimal bermain/berolah raga:
<table border="1" style="border-collapse: collapse; width: 68mm;">
<tr valign="top">
<td width="34mm">RA</td>
<td width="34mm">150 m2</td>
</tr>
<tr valign="top">
<td>MI</td>
<td>300 m2</td>
</tr>
<tr valign="top">
<td>MTs</td>
<td>500 m2</td>
</tr>
<tr valign="top">
<td>MA</td>
<td>500 m2</td>
</tr>
<tr valign="top">
<td>MAK</td>
<td>500 m2</td>
</tr>
</table>
<u>Memenuhi Syarat:</u> Jika luas sarana minimal bermain/berolah raga sesuai dengan tabel di atas<br>
<u>Tidak Memenuhi Syarat:</u> Jika luas sarana minimal bermain/berolah raga tidak sesuai dengan tabel di atas</td>
<td class="skor" width="10%"></td>
</tr>
<tr>
<td valign="top" align="center">16</td>
<td valign="top">Ketersediaan Buku/bahan ajar<br />1. Memenuhi syarat<br />0. Tidak memenuhi syarat</td>
<td class="indikator">
Catatan:<br />Ketersediaan jumlah buku/bahan ajar minimal 1 set/guru.<br>
<u>Memenuhi Syarat:</u> Jika ketersediaan buku/bahan ajar minimal sesuai dengan tabel di atas<br>
<u>Tidak Memenuhi Syarat:</u> Jika ketersediaan buku/bahan ajar minimal kurang dari tabel di atas</td>
<td class="skor"></td>
</tr>
<tr>
<td valign="top" align="center">17</td>
<td valign="top">Jumlah minimal buku pengayaan dan referensi<br />1. Memenuhi syarat<br />0. Tidak memenuhi syarat</td>
<td class="indikator">
Catatan:<br />Jumlah minimal buku pengayaan dan referensi:
<table border="1" style="border-collapse: collapse; width: 68mm;">
<tr valign="top">
<td style="width:12mm";>RA</td>
<td style="vertical-align:justify; width:54mm;">10 judul buku pengayaan dan 5 judul buku referensi</td>
</tr>
<tr valign="top">
<td>MI</td>
<td style="vertical-align:justify; width:54mm;">50 judul buku pengayaan dan 5 judul buku referensi</td>
</tr>
<tr valign="top">
<td valign="top">MTs</td>
<td style="vertical-align:justify; width:54mm;">100 judul buku pengayaan dan 10 judul buku referensi</td>
</tr>
<tr valign="top">
<td valign="top">MA</td>
<td style="vertical-align:justify; width:54mm;">100 judul buku pengayaan dan 10 judul buku referensi</td>
</tr>
<tr valign="top">
<td valign="top">MAK</td>
<td style="vertical-align:justify; width:54mm;">100 judul buku pengayaan dan 10 judul buku referensi</td>
</tr>
</table>
<u>Memenuhi Syarat:</u> Jika jumlah minimal buku pengayaan dan referensi sesuai dengan tabel di atas<br>
<u>Tidak Memenuhi Syarat:</u> Jika jumlah minimal buku pengayaan dan referensi tidak sesuai dengan tabel di atas</td>
<td class="skor"></td>
</tr>
<tr>
<td valign="top" align="center">18</td>
<td valign="top">Jumlah minimal peralatan belajar/ laboratorium<br />1. Memenuhi syarat<br />0. Tidak memenuhi syarat</td>
<td class="indikator">
Catatan:<br />Jumlah minimal peralatan belajar/ laboratorium:
<table border="1" style="border-collapse: collapse; width: 68mm;">
<tr>
<td style="width: 5mm;">RA</td>
<td style="width: 65mm;">
1. Satu set alat peraga edukatif RA di dalam ruangan paling sedikit terdiri dari:
<ul >
<li>balok bangunan</li>
<li>mainan kontruksi</li>
<li>permainan palu</li>
<li>menara gelang</li>
<li>kotak menara</li>
<li>alat pertukangan</li>
<li>Permainan puzzle.</li>
</ul>
</td></tr></table>
</td>
<td class="skor"></td>
</tr>
</table>

<table  width="100%" border="1" style="border-collapse: collapse; line-height: 1;">
<tr>
<td valign="top" width="5%"></td>
<td valign="top" width="35%"></td>
<td class="indikator" width="50%">


<table border="1" width="100%" style="border-collapse: collapse;">
<tr>
<td style="width: 30%">RA</td>
<td style="width: 70%;">
2. Satu set alat bermain di luar ruangan bagi RA terdiri dari:
<ul >
<li>papan peluncur</li>
<li>papan jungkitan</li>
<li>ayunan</li>
<li>papan titian</li>
</ul></td>
</tr>
<tr>
<td>MI</td>
<td>
Satu set alat peraga IPA dan bahannya bagi MI minimal terdiri dari:
<ul >
<li>model kerangka manusia</li>
<li>model tubuh manusia</li>
<li>bola dunia (globe)</li>
<li>contoh peralatan optik</li>
<li>kit IPA untuk eksperimen dasar</li>
<li>poster/carta IPA</li>
</ul>
</td>
</tr>
<tr>
<td>MTs</td>
<td >Satu set minimalis peralatan laboratorium Multimedia</td>
</tr>
<tr>
<td>MA</td>
<td>Satu set minimalis peralatan laboratorium Multimedia</td>
</tr>
<tr>
<td>MAK</td>
<td >Satu set peralatan minimalis laboratorium produktif MAK terdiri dari peralatan-peralatan yang digunakan untuk praktik peserta didik sesuai dengan program keahlian yang dipilih</td>
</tr>
</table>
<u>Memenuhi Syarat:</u> Jika jumlah minimal peralatan belajar/laboratorium sesuai dengan tabel di atas<br>
<u>Tidak Memenuhi Syarat:</u> Jika jumlah minimal peralatan belajar/laboratorium tidak sesuai dengan tabel di atas


</td>
<td align="center" width="10%"></td>
</tr>


<tr>
<td valign="top">19</td>
<td valign="top">Jumlah minimal peralatan penunjang administrasi<br />1. Memenuhi syarat<br />0. Tidak memenuhi syarat</td>
<td class="indikator">
Catatan:<br />Jumlah minimal peralatan penunjang administrasi: berupa komputer/laptop/alat pengolah data
<table border="1" style="border-collapse: collapse; width: 68mm;">
<tr>
<td>RA</td>
<td>1 unit</td>
</tr>
<tr valign="top">
<td>MI</td>
<td>1 unit</td>
</tr>
<tr valign="top">
<td>MTs</td>
<td>2 unit</td>
</tr>
<tr valign="top">
<td>MA</td>
<td>2 unit</td>
</tr>
<tr valign="top">
<td>MAK</td>
<td>2 unit</td>
</tr>
</table>
<u>Memenuhi Syarat:</u> Jika jumlah minimal peralatan penunjang administrasi sesuai dengan ketentuan di atas<br>
<u>Tidak Memenuhi Syarat:</u> Jika jumlah minimal peralatan penunjang administrasi tidak sesuai dengan ketentuan di atas</u>
</td>
<td class="skor"></td>
</tr>
</table>




</table>

</div>
<div style="page-break-before: always;"></div>

<div id="bodinya">


<p>C. Persyaratan Kelayakan</p>
<table  width="100%" border="1" style="border-collapse: collapse;line-height: 1;">
<tr>
<th width="5%">No</th>
<th width="35%">Persyaratan yang<br>diverifikasi</th>
<th width="40%">Indikator/Keterangan</th>
<th width="10%">Skor</th>
</tr>
<tr>
<td valign="top" align="center">1</td>
<td valign="top" >Kelayakan dilihat dari aspek tata ruang<br />1. Memenuhi syarat<br />0. Tidak memenuhi syarat</td>
<td class="indikator">
Catatan:<br />Tata ruang lokasi madrasah harus memenuhi standar:
<ol>
<li>Keamanan, kebersihan, kesehatan, dan keindahan</li>
<li>Kemudahan akses</li>
<li>memenuhi kualitas struktur bangunan,</li>
</ol>
<u>Memenuhi Syarat:</u> Jika tata ruang lokasi madrasah memenuhi semua aspek sesuai dengan ketentuan di atas<br>
<u>Tidak Memenuhi Syarat:</u> Jika tata ruang lokasi madrasah tidak memenuhi semua aspek sesuai dengan ketentuan di atas</td>
<td class="skor"></td>
</tr>
<tr>
<td valign="top" align="center">2</td>
<td valign="top">Kelayakan dilihat dari aspek geografis<br />1. Memenuhi syarat<br />0. Tidak memenuhi syarat</td>
<td class="indikator">
Catatan:<br />Lokasi pendirian madrasah harus:
<ul>
<li>aman bencana (banjir, longsor, dan jenis bencana lainnya); dan</li>
<li>ramah lingkungan</li>
</ul>
<u>Memenuhi Syarat:</u> Jika lokasi madrasah memenuhi kelayakan aspek geografis aman bencana dan ramah lingkungan.<br>
<u>Tidak Memenuhi Syarat:</u> Jika lokasi madrasah tidak memenuhi kelayakan aspek geografis aman bencana dan ramah lingkungan.</td>
<td class="skor"></td>
</tr>
<tr>
<td valign="top" align="center">3</td>
<td valign="top">Kelayakan dilihat dari aspek ekologis<br />1. Memenuhi syarat<br />0. Tidak memenuhi syarat</td>
<td class="indikator">
Catatan:<br />Lokasi pendirian madrasah harus: tidak berada di daerah resapan air, berada di hutan lindung.<br>
<u>Memenuhi Syarat:</u> Jika lokasi madrasah memenuhi kelayakan aspek ekologis sesuai dengan ketentuan di atas<br>
<u>Tidak Memenuhi Syarat:</u> Jika lokasi madrasah tidak memenuhi kelayakan aspek ekologis sesuai dengan ketentuan di atas
</td>
<td class="skor"></td>
</tr>

</table>
<table  width="100%" border="1" style="border-collapse: collapse;line-height: 1;">
<tr>
<td valign="top" align="center" width="5%">4</td>
<td valign="top" width="35%">Kelayakan dilihat dari prospek pendaftar<br />1. Memenuhi syarat<br />0. Tidak memenuhi syarat</td>
<td class="indikator" width="40%">
Catatan:<br>Kriteria minimal prospek jumlah pendaftar:
<table border="1" style="border-collapse: collapse; width: 68mm;">
<tr valign="top">
<td width="34mm">RA</td>
<td width="34mm">&gt;15 siswa</td>
</tr>
<tr valign="top">
<td>MI</td>
<td>&gt;28 siswa</td>
</tr>
<tr valign="top">
<td>MTs</td>
<td>&gt;32 siswa</td>
</tr>
<tr valign="top">
<td>MA</td>
<td>&gt;32 siswa</td>
</tr>
<tr valign="top">
<td>MAK</td>
<td>&gt;32 siswa</td>
</tr>
</table>
<u>Memenuhi Syarat:</u> Jika prospek jumlah pendaftar memenuhi kriteria minimal sebagai ketentuan di atas.<br>
<u>Tidak Memenuhi Syarat:</u> Jika prospek jumlah pendaftar memenuhi kriteria minimal sebagai ketentuan di atas.</td>
<td class="skor" width="10%"></td>
</tr>

</table>
<table  width="100%" border="1" style="border-collapse: collapse;line-height: 1;">
<tr>
<td valign="top" align="center" width="5%">5</td>
<td valign="top" width="35%">Kelayakan dilihat dari aspek sosial dan budaya<br />1. Memenuhi syarat<br />0. Tidak memenuhi syarat</td>
<td class="indikator" width="40%">
<u>Memenuhi Syarat:</u> Jika lokasi pendirian madrasah mendapat dukungan dan respon positif dari masyarakat dan tidak berpotensi menimbulkan resistensi masyarakat dan masalah sosial budaya lainnya<br>
<u>Tidak Memenuhi Syarat:</u> Jika lokasi pendirian madrasah berpotensi menimbulkan resistensi masyarakat dan masalah sosial budaya lainnya
</td>
<td class="skor" width="10%"></td>
</tr>
<tr>
<td valign="top" align="center">6</td>
<td valign="top">Kelayakan dilihat dari aspek demografi anak usia sekolah dengan ketersediaan lembaga pendidikan formal<br />1. Memenuhi syarat<br />0. Tidak memenuhi syarat</td>
<td class="indikator">
<u>Memenuhi Syarat:</u> Jika jumlah anak usia sekolah di lokasi pendirian madrasah dalam radius 6 Km mencukupi untuk ditampung dalam sebuah satuan pendidikan.<br>
<u>Tidak Memenuhi Syarat:</u> Jika jumlah anak usia sekolah di lokasi pendirian madrasah dalam radius 6 Km tidak mencukupi untuk ditampung dalam sebuah satuan pendidikan.</td>
<td class="skor"></td>
</tr>
</table>
<br>
<p>Tim Verifikasi,</p>
<table width="100%">
<tr>
<td width="40%" style="padding: 10px">1.&nbsp;<?php echo $surat->kasi_nama; ?></td>
<td width="35%" style="padding: 10px">(<?php echo $kantor->kasi_jabatan; ?>)</td>
<td style="padding: 10px">..............................</td>
</tr>
<tr>
<td width="40%" style="padding: 10px">2.&nbsp;<?php echo $this->Reff->get_kondisi(array("id"=>$surat->pengawas_id),"tm_pegawai","nama") ?></td>
<td width="35%" style="padding: 10px">(Pengawas Madrasah)</td>
<td style="padding: 10px">..............................</td>
</tr>
<tr>
<td width="40%" style="padding: 10px">3.&nbsp;<?php echo $this->Reff->get_kondisi(array("id"=>$surat->staff_id),"tm_pegawai","nama") ?></td>
<td width="35%" style="padding: 10px">(Pelaksana)</td>
<td style="padding: 10px">..............................</td>
</tr>
</table>
</div>