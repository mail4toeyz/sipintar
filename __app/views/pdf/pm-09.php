<?php
$documentroot = $this->Reff->documentroot();
//$documentroot = base_url();
$kantor         = $this->Reff->get_where("tr_kantor",array("linkid"=>$madrasah->provinsi_id));
?>
  <style>

#bodinya { margin-right: 40px;
  margin-left: 60px; 
  font-family: Bookman Old Style;
font-size: 16px;
  }
</style>
<?php 
  $surat = $this->db->get_where("tr_surat",array("tmyayasan_id"=>$yayasan->id,"jenis"=>"nd"))->row();
  $suratbava = $this->db->get_where("tr_surat",array("tmyayasan_id"=>$yayasan->id,"jenis"=>"bava"))->row();
  $suratvl = $this->db->get_where("tr_surat",array("tmyayasan_id"=>$yayasan->id,"jenis"=>"vl"))->row();
  $suratrk = $this->db->get_where("tr_surat",array("tmyayasan_id"=>$yayasan->id,"jenis"=>"rk"))->row();
  $suratrp = $this->db->get_where("tr_surat",array("tmyayasan_id"=>$yayasan->id,"jenis"=>"rp"))->row();
   ?>
<table width="100%"  style="margin-top:0px">
			  
			    <tr>
				  <td width="105px"> <img src="<?php echo $documentroot.'/__statics/img/kemenag.jpg';?>" style="width:100px"></td>
				    <td align="center"   valign="top">
				       <span style="font-size:19px;font-weight:bold;font-family:Bookman Old Style">KEMENTERIAN AGAMA REPUBLIK INDONESIA</span><br/>
					   <span style="font-size:17px;font-weight:bold;font-family:Bookman Old Style"><?php echo strtoupper($kantor->nama); ?></span><br/>

					   <span style="font-size:12px;font-family:Bookman Old Style">  <?php echo ($kantor->alamat); ?></span>
					   <span style="font-size:12px;font-family:Bookman Old Style">  Telepon : (<?php echo ($kantor->prefix); ?>) <?php echo ($kantor->telepon); ?> &nbsp; Faksimili : (<?php echo ($kantor->prefix); ?>) <?php echo ($kantor->fax); ?> &nbsp; Email : <?php echo ($kantor->email); ?> </span>
					   <span style="font-size:12px;font-family:Bookman Old Style">   Website : <?php echo ($kantor->website); ?> </span>
					  </span>
				  </td>
				   <td width="20px">&nbsp;</td>
				</tr>
				
			 </table>
			 
			 
			 
			 
<hr style="height: 2px; border: none; color: #000; background-color: #000;" />
<div id="bodinya">
<p align="center">NOTA DINAS<br />NOMOR: <?php echo $surat->nomor; ?></p>
<table style="width:165mm;">
<tr>
<td style="width:12%;">Dari</td>
<td style="width:88%;">:&nbsp;Kepala Bidang Pendidikan Madrasah</td>
</tr>
<tr>
<td style="width:12%;">Kepada</td>
<td style="width:88%;">:&nbsp;Kepala Kantor Wilayah Kementerian Agama</td>
</tr>
<tr>
<td style="width:12%;">Tanggal</td>
<td style="width:88%;">:&nbsp;<?php echo $this->Reff->formattanggalstring($surat->tanggal); ?></td>
</tr>
<tr>
<td style="width:12%;">Sifat</td>
<td style="width:88%;">:&nbsp;<?php echo $surat->sifat_surat; ?></td>
</tr>

<tr>
<td style="width:12%;">Hal</td>
<td style="width:88%;">:&nbsp;Pertimbangan Pemberian Izin Pendirian Madrasah</td>
</tr>
</table>
<p><i>Assalamu&rsquo;alaikum Wr. Wb</i></p>
<table style="width:165mm;">
<tr valign="top">
<td style="width:8%;">Dasar</td>
<td style="width:2%;">:</td>
<td style="width:90%;"><ol style="padding: -20px -20px; width: 145mm;">
<li>Berita Acara Verifikasi Dokumen Persyaratan Administratif, Teknis, dan Kelayakan Nomor: <?php echo $suratbava->nomor; ?>&nbsp;Tanggal&nbsp;<?php echo $this->Reff->formattanggalstring($suratbava->tanggal); ?></li>
<li>Berita Acara Hasil Verifikasi Lapangan Nomor: <?php echo $suratvl->nomor; ?>&nbsp;Tanggal&nbsp;<?php echo $this->Reff->formattanggalstring($suratvl->tanggal); ?></li>
<li>Rekomendasi dari Kepala Kantor Kementerian Agama <?php echo ucwords(strtolower(preg_match('/^KOTA /',$this->Reff->get_kondisi(array("id"=>$madrasah->kota_id),"kota","nama"))?'':'KABUPATEN ').$this->Reff->get_kondisi(array("id"=>$madrasah->kota_id),"kota","nama")); ?>&nbsp;Nomor: <?php echo $suratrk->nomor; ?>&nbsp;Tanggal&nbsp;<?php echo $this->Reff->formattanggalstring($suratrk->tanggal); ?></li>
<li>Berita Acara Rapat Pertimbangan Pemberian Izin Pendirian Madrasah Nomor: <?php echo $suratrp->nomor; ?>&nbsp;Tanggal&nbsp;<?php echo $this->Reff->formattanggalstring($suratrp->tanggal); ?></li>
</ol></td>
</tr>
</table>
<div style="text-align: justify;">Bersama ini kami sampaikan pertimbangan persetujuan pemberian izin pendirian  <?php echo $this->Reff->jenjangsingkat($madrasah->jenjang).' '.$madrasah->nama; ?>&nbsp;yang diajukan oleh:</div>
<table style="width: 152mm; line-height: 1.5;">
<tr valign="top">
<td style="width: 45mm;">Nama Penyelenggara</td>
<td style="width: 5mm;">:</td>
<td style="width: 102mm;"><?php echo $yayasan->nama; ?></td>
</tr>
<tr valign="top">
<td style="width: 45mm;">Alamat Penyelenggara</td>
<td style="width: 5mm;">:</td>
<td style="width: 102mm;"><?php echo $yayasan->alamat; ?>,&nbsp;
<?php echo ucwords(strtolower($this->Reff->get_kondisi(array("id"=>$yayasan->desa_id),"desa","nama")));  ?> Kecamatan
<?php echo ucwords(strtolower($this->Reff->get_kondisi(array("id"=>$yayasan->kecamatan_id),"kecamatan","nama")));  ?>&nbsp;
<?php echo ucwords(strtolower(preg_match('/^KOTA /',$this->Reff->get_kondisi(array("id"=>$yayasan->kota_id),"kota","nama"))?'':'KABUPATEN ').$this->Reff->get_kondisi(array("id"=>$yayasan->kota_id),"kota","nama")); ?> <?php echo ucwords(strtolower($this->Reff->get_kondisi(array("id"=>$yayasan->provinsi_id),"provinsi","nama"))); ?>
</td>
</tr>
<tr valign="top">
<td style="width: 45mm;">Akte Notaris</td>
<td style="width: 5mm;">:</td>
<td style="width: 102mm;">No. <?php echo $yayasan->notaris_no.' '.$yayasan->notaris_nama.' Tanggal '.$this->Reff->formattanggalstring($yayasan->notaris_tgl); ?></td>
</tr>
<tr valign="top">
<td style="width: 45mm;">Pengesahan Akte Notaris</td>
<td style="width: 5mm;">:</td>
<td style="width: 102mm;"><?php 
$skumhamno = trim($yayasan->menkumham_sk);
$skumhamtgl = $yayasan->menkumham_tgl;

echo $skumhamno.' Tanggal '.$this->Reff->formattanggalstring($skumhamtgl); ?></td>
</tr>
</table>


<p>Sebagai bahan pertimbangan Bapak, bersama ini kami sampaikan:</p>
<ol style="padding: -10px -10px">
<li>Berita Acara Verifikasi Dokumen Persyaratan Administratif, Teknis, dan Kelayakan</li>
<li>Berita Acara Hasil Verifikasi Lapangan</li>
<li>Rekomendasi dari Kepala Kantor Kementerian Agama <?php echo ucwords(strtolower(preg_match('/^KOTA /',$this->Reff->get_kondisi(array("id"=>$madrasah->kota_id),"kota","nama"))?'':'KABUPATEN ').$this->Reff->get_kondisi(array("id"=>$madrasah->kota_id),"kota","nama")); ?></li>
<li>Berita Acara Rapat Pertimbangan Pemberian Izin Pendirian Madrasah.</li>
</ol>
</div>
<div style="page-break-before: always;"></div>

<div id="bodinya">
Selanjutnya, kami mohon perkenan Bapak untuk dapat menyetujui dan menandatangani Rancangan Keputusan Menteri Agama tentang Pemberian Izin Operasional Pendirian Madrasah dan Rancangan Piagam Pendirian Madrasah dimaksud.<br>
Demikian untuk menjadikan periksa.<br>
<i>Wassalamu&rsquo;alaikum Wr. Wb.</i><br>
<br>
<br>
<br>
<br>
<br>
<table style="width:100%;">
<tr>
<td style="width:60%;">&nbsp;</td>
<td>Kepala Bidang</td>
</tr>
<tr>
<td style="width:60%;">&nbsp;</td>
<td>Pendidikan Madrasah,</td>
</tr>
<tr>
<td style="width:60%;">&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td style="width:60%;">&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td style="width:60%;">&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<td style="width:60%;">&nbsp;</td>
<td><?php echo $kantor->kabid_nama; ?></td>
</tr>
<tr>
<td style="width:60%;">&nbsp;</td>
<td>NIP.&nbsp;<?php echo $kantor->kabid_nip; ?></td>
</tr>
</table>
</div>
