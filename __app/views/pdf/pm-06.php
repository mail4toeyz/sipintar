<?php
$documentroot = $this->Reff->documentroot();
//$documentroot = base_url();
$kantor         = $this->Reff->get_where("tr_kantor",array("linkid"=>$madrasah->kota_id));
?>
<style>

#bodinya { margin-right: 40px;
  margin-left: 60px; 
  font-family: Bookman Old Style;
font-size: 16px;
  }
</style>
<table width="100%"  style="margin-top:0px">
			  
			    <tr>
				  <td width="105px"> <img src="<?php echo $documentroot.'/__statics/img/kemenag.jpg';?>" style="width:100px"></td>
				    <td align="center"   valign="top">
				       <span style="font-size:19px;font-weight:bold;font-family:Bookman Old Style">KEMENTERIAN AGAMA REPUBLIK INDONESIA</span><br/>
					   <span style="font-size:17px;font-weight:bold;font-family:Bookman Old Style"><?php echo strtoupper($kantor->nama); ?></span><br/>

					   <span style="font-size:12px;font-family:Bookman Old Style">  <?php echo ($kantor->alamat); ?></span>
					   <span style="font-size:12px;font-family:Bookman Old Style">  Telepon : (<?php echo ($kantor->prefix); ?>) <?php echo ($kantor->telepon); ?> &nbsp; Faksimili : (<?php echo ($kantor->prefix); ?>) <?php echo ($kantor->fax); ?> &nbsp; Email : <?php echo ($kantor->email); ?> </span>
					   <span style="font-size:12px;font-family:Bookman Old Style">   Website : <?php echo ($kantor->website); ?> </span>
					  </span>
				  </td>
				   <td width="20px">&nbsp;</td>
				</tr>
				
			 </table>
			 
			 
<hr style="height: 2px; border: none; color: #000; background-color: #000;" />
<div id="bodinya">
<?php 
  $surat = $this->db->get_where("tr_surat",array("tmyayasan_id"=>$yayasan->id,"jenis"=>"rk"))->row();
  $suratbava = $this->db->get_where("tr_surat",array("tmyayasan_id"=>$yayasan->id,"jenis"=>"bava"))->row();
  $suratvl = $this->db->get_where("tr_surat",array("tmyayasan_id"=>$yayasan->id,"jenis"=>"vl"))->row();
  ?>
  
  <p align="center">REKOMENDASI IZIN PENDIRIAN MADRASAH<br /> Nomor: <?php echo $surat->nomor;  ?></p>
<p align="justify">Menindaklanjuti surat dari <?php echo $yayasan->nama; ?>&nbsp;Nomor
      <?php echo $this->Reff->surat_yayasan($yayasan->id); ?>&nbsp;Tanggal
      <?php echo $this->Reff->formattanggalstring($madrasah->tgl_terima); ?>&nbsp;Perihal Permohonan Izin Pendirian Madrasah, setelah kami melakukan verifikasi dokumen persyaratan administratif, teknis, dan kelayakan yang telah ditetapkan sebagaimana tertuang dalam Berita Acara Hasil Verifikasi Dokumen Persyaratan Administratif, Teknis, dan Kelayakan Nomor <?php echo $suratbava->nomor;  ?>&nbsp;Tanggal <?php echo $this->Reff->formattanggalstring($suratbava->tanggal); ?>
  &nbsp;dan hasil verifikasi lapangan Nomor <?php echo $suratvl->nomor; ?>&nbsp;Tanggal <?php echo $this->Reff->formattanggalstring($suratvl->tanggal); ?>, dengan ini kami sampaikan bahwa pada prinsipnya kami tidak keberatan dan mendukung usulan pendirian Madrasah:
</p>
<table border="0" style="width: 152mm; vertical-align: top; line-height: 1.5;">
<tr>
<td style="width: 45mm;">Nama Calon Madrsah</td>
<td style="width: 4mm;">:</td>
<td style="width: 103mm;"><?php echo $this->Reff->jenjangsingkat($madrasah->jenjang).' '.$madrasah->nama; ?></td>
</tr>
<tr>
  <td style="width: 45mm;">Alamat Calon Madrasah</td>
  <td style="width: 4mm;">:</td>
  <td style="width: 103mm;"> <?php echo ($madrasah->alamat); ?>&nbsp;Desa/Kelurahan&nbsp;
            <?php echo ucwords(strtolower($this->Reff->get_kondisi(array("id"=>$madrasah->desa_id),"desa","nama")));  ?> Kecamatan
            <?php echo ucwords(strtolower($this->Reff->get_kondisi(array("id"=>$madrasah->kecamatan_id),"kecamatan","nama")));  ?>&nbsp;
            <?php echo ucwords(strtolower((preg_match('/^KOTA /',$this->Reff->get_kondisi(array("id"=>$madrasah->kota_id),"kota","nama"))?'':'KABUPATEN ').$this->Reff->get_kondisi(array("id"=>$madrasah->kota_id),"kota","nama"))); ?>&nbsp;<?php echo ucwords(strtolower($this->Reff->get_kondisi(array("id"=>$madrasah->provinsi_id),"provinsi","nama"))); ?>   </td>
     </tr>
<tr>
<td style="width: 45mm;">Nama Penyelenggara</td>
<td style="width: 4mm;">:</td>
<td style="width: 103mm;"><?php echo $yayasan->nama; ?></td>
</tr>
<tr>
<td style="width: 45mm;">Alamat Penyelenggara</td>
<td style="width: 4mm;">:</td>
<td style="width: 103mm;"><?php echo $yayasan->alamat; ?>,&nbsp;
<?php echo ucwords(strtolower($this->Reff->get_kondisi(array("id"=>$yayasan->desa_id),"desa","nama")));  ?> Kecamatan
<?php echo ucwords(strtolower($this->Reff->get_kondisi(array("id"=>$yayasan->kecamatan_id),"kecamatan","nama")));  ?>&nbsp;
<?php echo ucwords(strtolower(preg_match('/^KOTA /',$this->Reff->get_kondisi(array("id"=>$yayasan->kota_id),"kota","nama"))?'':'KABUPATEN ').$this->Reff->get_kondisi(array("id"=>$yayasan->kota_id),"kota","nama")); ?> <?php echo ucwords(strtolower($this->Reff->get_kondisi(array("id"=>$yayasan->provinsi_id),"provinsi","nama"))); ?></tr>
</tr>
<tr valign="top">
<td style="width: 45mm;">Akte Notaris</td>
<td style="width: 4mm;">:</td>
<td style="width: 103mm;">No. <?php echo $yayasan->notaris_no.' '.$yayasan->notaris_nama.' Tanggal '.$this->Reff->formattanggalstring($yayasan->notaris_tgl); ?></td>
</tr>
<tr valign="top">
<td style="width: 45mm;">Pengesahan Akte Notaris</td>
<td style="width: 4mm;">:</td>
<td style="width: 103mm;"><?php 
$skumhamno = trim($yayasan->menkumham_sk);
$skumhamtgl = $yayasan->menkumham_tgl;

echo $skumhamno.' Tanggal '.$this->Reff->formattanggalstring($skumhamtgl); ?></td>
</tr>
</table>
<p>Demikian Rekomendasi ini dibuat untuk dapat dipergunakan sebagaimana mestinya.</p>
<p> </p>
 <table style="width:100%" class="mceItemTable">
      
        <tr>
          <td style="width:50%;">&nbsp;</td>
          <td><?php echo $kantor->nama_kota; ?>,&nbsp;<?php echo $this->Reff->formattanggalstring($surat->tanggal); ?></td>
        </tr>
        <tr>
          <td style="width:50%;">&nbsp;</td>
          <td>Kepala,</td>
        </tr>
        <tr>
          <td style="width:50%;">&nbsp;</td><td>&nbsp;</td>
        </tr>
        <tr>
          <td style="width:50%;">&nbsp;</td><td>&nbsp;</td>
        </tr>
        <tr>
          <td style="width:50%;">&nbsp;</td>
          <td><?php echo $kantor->kepala_nama; ?></td>
        </tr>
        <tr>
          <td style="width:50%;">&nbsp;</td>
          <td>NIP.&nbsp;<?php echo $kantor->kepala_nip; ?></td>
        </tr>
      
    </table>
</div>
