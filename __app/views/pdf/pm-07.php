<?php
$documentroot = $this->Reff->documentroot();
//$documentroot = base_url();
$kantor         = $this->Reff->get_where("tr_kantor",array("linkid"=>$madrasah->provinsi_id));
?>

<?php 
  $surat = $this->db->get_where("tr_surat",array("tmyayasan_id"=>$yayasan->id,"jenis"=>"pd"))->row();
  ?>
  <style>

#bodinya { margin-right: 40px;
  margin-left: 60px; 
  font-family: Bookman Old Style;
font-size: 16px;
  }
</style>
<table width="100%"  style="margin-top:0px">
			  
			    <tr>
				  <td width="105px"> <img src="<?php echo $documentroot.'/__statics/img/kemenag.jpg';?>" style="width:100px"></td>
				    <td align="center"   valign="top">
				       <span style="font-size:19px;font-weight:bold;font-family:Bookman Old Style">KEMENTERIAN AGAMA REPUBLIK INDONESIA</span><br/>
					   <span style="font-size:17px;font-weight:bold;font-family:Bookman Old Style"><?php echo strtoupper($kantor->nama); ?></span><br/>

					   <span style="font-size:12px;font-family:Bookman Old Style">  <?php echo ($kantor->alamat); ?></span>
					   <span style="font-size:12px;font-family:Bookman Old Style">  Telepon : (<?php echo ($kantor->prefix); ?>) <?php echo ($kantor->telepon); ?> &nbsp; Faksimili : (<?php echo ($kantor->prefix); ?>) <?php echo ($kantor->fax); ?> &nbsp; Email : <?php echo ($kantor->email); ?> </span>
					   <span style="font-size:12px;font-family:Bookman Old Style">   Website : <?php echo ($kantor->website); ?> </span>
					  </span>
				  </td>
				   <td width="20px">&nbsp;</td>
				</tr>
				
			 </table>


<hr style="height: 2px; border: none; color: #000; background-color: #000;" />
<div id="bodinya">
<br>
<p align="center">TANDA TERIMA DOKUMEN<br /> Nomor:&nbsp;<?php echo $surat->nomor; ?></p>
<p>Telah terima dokumen Proposal Pendirian Madrasah:</p>
<table style="vertical-align: top;" >
<tr>
<td style="width: 37mm;">Nama Calon Madrasah</td><td style="width: 2mm;">:</td>
<td style="width: 100mm;"><?php echo $this->Reff->jenjangsingkat($madrasah->jenjang).' '.$madrasah->nama; ?></td>
</tr>
<tr>
<td style="width: 37mm;">Alamat Calon Madrasah</td><td style="width: 2mm;">:</td>
<td style="width: 100mm;">
 <?php echo ($madrasah->alamat); ?>&nbsp;Desa/Kelurahan&nbsp;
            <?php echo ucwords(strtolower($this->Reff->get_kondisi(array("id"=>$madrasah->desa_id),"desa","nama")));  ?> Kecamatan
            <?php echo ucwords(strtolower($this->Reff->get_kondisi(array("id"=>$madrasah->kecamatan_id),"kecamatan","nama")));  ?>&nbsp;
            <?php echo ucwords(strtolower((preg_match('/^KOTA /',$this->Reff->get_kondisi(array("id"=>$madrasah->kota_id),"kota","nama"))?'':'KABUPATEN ').$this->Reff->get_kondisi(array("id"=>$madrasah->kota_id),"kota","nama"))); ?>&nbsp;<?php echo ucwords(strtolower($this->Reff->get_kondisi(array("id"=>$madrasah->provinsi_id),"provinsi","nama"))); ?>   
   
</td>
</tr>
<tr valign="top">
<td style="width: 37mm;">Nama Penyelenggara</td><td style="width: 2mm;">:</td>
<td style="width: 100mm;"><?php echo $yayasan->nama; ?></td>
</tr>
</table>
<br>
Daftar dokumen yang diterima :
<table border="1" style="border-collapse: collapse; width:165mm; vertical-align: top;">
<tr style="vertical-align: middle; padding: 4mm;">
<th style="width:8mm; height: 32px;" align="center">No.</th>
<th style="width:112mm; height: 32px;" align="center">Jenis Dokumen</th>
<th style="width:20mm; height: 32px;" align="center">Jumlah</th>
</tr>
<tr>
<td align="center">1.</td>
<td style="width:112mm;">Surat Pengantar dan Formulir Permohonan Izin Pendirian Madrasah beserta kelengkapan persyaratan dan dokumen pendukungnya</td>
<td align="center">1 </td>
</tr>
<tr>
<td align="center">2.</td>
<td style="width:112mm;">Rekomendasi Pendirian Madrasah</td>
<td align="center">1 </td>
</tr>
<tr>
<td align="center">3.</td>
<td style="width:112mm;">Berita Acara Hasil Verifikasi Dokumen Persyaratan Administratif, Teknis, dan Kelayakan</td>
<td align="center">1 </td>
</tr>
<tr>
<td align="center">4.</td>
<td style="width:112mm;">Berita Acara Verifikasi Lapangan</td>
<td align="center">1 </td>
</tr>
</table>
<p></p>
<table style="width: 100%;">
<tr>
<td style="width:30%;">Tanggal Penerimaan</td>
<td>:&nbsp;<?php echo $this->Reff->formattanggalstring($surat->tanggal); ?></td>
</tr>
</table>
<p></p>
<br>
<br>
<table class="mceItemTable" style="width: 100%;">

<tr>
<td style=" text-align:right;">Penerima: <?php echo $this->Reff->get_kondisi(array("id"=>$surat->staff_id),"tm_pegawai","nama") ?> (NIP :<?php echo $this->Reff->get_kondisi(array("id"=>$surat->staff_id),"tm_pegawai","nip") ?>)</td>
</tr>

</table>
</div>
