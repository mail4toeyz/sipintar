<?php
$documentroot = $this->Reff->documentroot();
//$documentroot = base_url();
$kantor         = $this->Reff->get_where("tr_kantor",array("linkid"=>$madrasah->provinsi_id));
?>

<?php 
  $surat = $this->db->get_where("tr_surat",array("tmyayasan_id"=>$yayasan->id,"jenis"=>"rp"))->row();
  $suratrk = $this->db->get_where("tr_surat",array("tmyayasan_id"=>$yayasan->id,"jenis"=>"rk"))->row();
  $suratvl = $this->db->get_where("tr_surat",array("tmyayasan_id"=>$yayasan->id,"jenis"=>"vl"))->row();
  ?>
    <style>

#bodinya { margin-right: 40px;
  margin-left: 60px; 
  font-family: Bookman Old Style;
font-size: 16px;
  }
</style>
<table width="100%"  style="margin-top:0px">
			  
			    <tr>
				  <td width="105px"> <img src="<?php echo $documentroot.'/__statics/img/kemenag.jpg';?>" style="width:100px"></td>
				    <td align="center"   valign="top">
				       <span style="font-size:19px;font-weight:bold;font-family:Bookman Old Style">KEMENTERIAN AGAMA REPUBLIK INDONESIA</span><br/>
					   <span style="font-size:17px;font-weight:bold;font-family:Bookman Old Style"><?php echo strtoupper($kantor->nama); ?></span><br/>

					   <span style="font-size:12px;font-family:Bookman Old Style">  <?php echo ($kantor->alamat); ?></span>
					   <span style="font-size:12px;font-family:Bookman Old Style">  Telepon : (<?php echo ($kantor->prefix); ?>) <?php echo ($kantor->telepon); ?> &nbsp; Faksimili : (<?php echo ($kantor->prefix); ?>) <?php echo ($kantor->fax); ?> &nbsp; Email : <?php echo ($kantor->email); ?> </span>
					   <span style="font-size:12px;font-family:Bookman Old Style">   Website : <?php echo ($kantor->website); ?> </span>
					  </span>
				  </td>
				   <td width="20px">&nbsp;</td>
				</tr>
				
			 </table>




<hr style="height: 2px; border: none; color: #000; background-color: #000;" />
<div id="bodinya">
<br>
<p align="center">BERITA ACARA<br>RAPAT PERTIMBANGAN PEMBERIAN IZIN PENDIRIAN MADRASAH<br>
NOMOR: <?php echo $surat->nomor; ?></p>
<p align="justify">Pada hari ini <?php echo strtolower($this->Reff->tgl_sebutan($surat->tanggal)); ?>, berdasarkan Rekomendasi Kepala Kantor Kementerian Agama <?php echo ucwords(strtolower(preg_match('/^KOTA /',$this->Reff->get_kondisi(array("id"=>$madrasah->kota_id),"kota","nama"))?'':'KABUPATEN ').$this->Reff->get_kondisi(array("id"=>$madrasah->kota_id),"kota","nama")); ?> Nomor <?php echo $suratrk->nomor; ?> Tanggal <?php echo $this->Reff->formattanggalstring($suratrk->tanggal); ?>, kami yang bertanda tangan di bawah ini telah mengadakan Rapat Pertimbangan Pemberian Izin Pendirian Madrasah yang diajukan oleh:</p>

<table style="width: 152mm; line-height: 1.5;">
<tr valign="top">
<td style="width: 45mm;">Nama Penyelenggara</td>
<td style="width: 5mm;">:</td>
<td style="width: 102mm;"><?php echo $yayasan->nama; ?></td>
</tr>
<tr valign="top">
<td style="width: 45mm;">Alamat Penyelenggara</td>
<td style="width: 5mm;">:</td>
<td style="width: 102mm;"><?php echo $yayasan->alamat; ?>,&nbsp;
<?php echo ucwords(strtolower($this->Reff->get_kondisi(array("id"=>$yayasan->desa_id),"desa","nama")));  ?> Kecamatan
<?php echo ucwords(strtolower($this->Reff->get_kondisi(array("id"=>$yayasan->kecamatan_id),"kecamatan","nama")));  ?>&nbsp;
<?php echo ucwords(strtolower(preg_match('/^KOTA /',$this->Reff->get_kondisi(array("id"=>$yayasan->kota_id),"kota","nama"))?'':'KABUPATEN ').$this->Reff->get_kondisi(array("id"=>$yayasan->kota_id),"kota","nama")); ?> <?php echo ucwords(strtolower($this->Reff->get_kondisi(array("id"=>$yayasan->provinsi_id),"provinsi","nama"))); ?>
</td>
</tr>
<tr valign="top">
<td style="width: 45mm;">Akte Notaris</td>
<td style="width: 5mm;">:</td>
<td style="width: 102mm;">No. <?php echo $yayasan->notaris_no.' '.$yayasan->notaris_nama.' Tanggal '.$this->Reff->formattanggalstring($yayasan->notaris_tgl); ?></td>
</tr>
<tr valign="top">
<td style="width: 45mm;">Pengesahan Akte Notaris</td>
<td style="width: 5mm;">:</td>
<td style="width: 102mm;"><?php 
$skumhamno = trim($yayasan->menkumham_sk);
$skumhamtgl = $yayasan->menkumham_tgl;

echo $skumhamno.' Tanggal '.$this->Reff->formattanggalstring($skumhamtgl); ?></td>
</tr>
</table>
<ol style="list-style-type: disc; width: 154mm; text-align: justify;">
<li>Rapat dipimpin oleh Kepala Bidang Pendidikan Madrasah, Kanwil Kementerian Agama Provinsi <?php echo ucwords(strtolower($this->Reff->get_kondisi(array("id"=>$yayasan->provinsi_id),"provinsi","nama"))); ?>&nbsp;yang dihadiri oleh Kepala Seksi Kelembagaan dan Kepala Seksi terkait di lingkungan Kanwil Kementerian Agama Provinsi dan Tim Verifikasi Lapangan;</li>
<li>Agenda Rapat adalah:<ol style="list-style-type: lower-alpha;text-align: justify;">
<li>Pembukaan;</li>
<li>Presentasi dan gelar hasil verifikasi persyaratan oleh Ketua Tim Verifikasi Lapangan;</li>
<li>Pembahasan dan Rekomendasi</li>
<li>Penutup</li>
</ol></li>
<li>Hasil pembahasan rapat adalah:<ol style="list-style-type: lower-alpha;text-align: justify;">
<?php if($surat->keputusan==1){ ?>
<li>menetapkan bahwa permohonan izin pendirian <?php echo $this->Reff->jenjangsingkat($madrasah->jenjang).' '.$madrasah->nama; ?>&nbsp;yang diajukan oleh <?php echo $yayasan->nama; ?>&nbsp;TELAH memenuhi segala persyaratan yang ditetapkan berdasarkan hasil verifikasi administratif, teknis, dan kelayakan serta hasil verifikasi lapangan yang dilaksanakan pada&nbsp;<?php echo $this->Reff->formattanggalstring($suratvl->tanggal); ?></li>
<li>Kepala Bidang Pendidikan Madrasah dimohon agar menyampaikan nota dinas pertimbangan pemberian izin pendirian madrasah kepada Kepala Kantor Wilayah paling lambat 3 hari kerja dengan tembusan Kepala Bagian Tata Usaha.</li>
<li>Kepala Bagian Tata Usaha agar menyiapkan:<ol style="list-style-type: lower-roman;">
<li>Rancangan Keputusan Menteri Agama Republik Indonesia tentang Pemberian Izin Operasional Pendirian <?php echo $this->Reff->jenjangsingkat($madrasah->jenjang).' '.$madrasah->nama; ?>;</li>
<li>Rancangan Piagam Pendirian <?php echo $this->Reff->jenjangsingkat($madrasah->jenjang).' '.$madrasah->nama; ?>;</li>
</ol></li>
<?php }else{ ?>
<li>menetapkan bahwa permohonan izin pendirian <?php echo $this->Reff->jenjangsingkat($madrasah->jenjang).' '.$madrasah->nama; ?>&nbsp;yang diajukan oleh <?php echo $yayasan->nama; ?>&nbsp;BELUM memenuhi segala persyaratan yang ditetapkan berdasarkan hasil verifikasi administratif, teknis, dan kelayakan serta hasil verifikasi lapangan yang dilaksanakan pada&nbsp;<?php echo $this->Reff->formattanggalstring($suratvl->tanggal); ?></li>
<?php } ?>
<li>Rapat ditutup oleh Kepala Bidang Pendidikan Madrasah pada pukul&nbsp;<?php echo $surat->selesai_rapat; ?></li>
</ol></li>
</ol>

<p>Demikian Berita Acara ini dibuat dengan sesungguhnya agar dapat dipergunakan sebagaimana mestinya.</p>
<br><br>
<b>Peserta Rapat : </b><br /><br />
<u>Kanwil Kementerian Agama Provinsi</u><br />


<table width="100%">
<tr>
<td style="width:70%; padding: 20px;">1. <?php echo $surat->kabid_nama; ?> &nbsp; (Kabid Pendidikan Madrasah)</td>
<td style="width:30%; padding: 20px;">.........................</td>
</tr>
<?php
$kasinama1 = trim($surat->kasi_nama);
if(strlen($kasinama1)>1){ ?>
<tr>
<td style="width:70%; padding: 20px;">2. <?php echo $surat->kasi_nama; ?> &nbsp; (Kasi Kelembagaan)</td>
<td style="width:30%; padding: 20px;">.........................</td>
</tr>
<?php } ?>
<?php
$peserta1_nama = trim($surat->peserta1_nama);
if(strlen($peserta1_nama)>1){ ?>
<tr>
<td style="width:70%; padding: 20px;">3. <?php echo $peserta1_nama; ?> &nbsp; (<?php echo $surat->peserta1_jabatan; ?>)</td>
<td style="width:30%; padding: 20px;">.........................</td>
</tr>
<?php } ?>
<?php
$peserta2_nama = trim($surat->peserta2_nama);
if(strlen($peserta2_nama)>1){ ?>
<tr>
<td style="width:70%; padding: 20px;">4. <?php echo $peserta2_nama; ?> &nbsp; (<?php echo $surat->peserta2_jabatan; ?>)</td>
<td style="width:30%; padding: 20px;">.........................</td>
</tr>
<?php } ?>
</table>
</div>


