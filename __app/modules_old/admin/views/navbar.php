

<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand" href="#">Computer Based Test (CBT) - <?php echo $this->Reff->owner(); ?>  </a>
			
			
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link" href="<?php echo site_url("admin"); ?>">
                  <i class="fa fa-home"></i>
                  <p class="d-lg-none d-md-block">
                    Stats
                  </p>
                </a>
              </li>
              
              <li class="nav-item dropdown">
                <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="fa fa-sign-out"></i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                   
                   
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="#" id="keluar">Log out</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
	  
<script type="text/javascript">
  $(document).off("click","#cekversi").on("click","#cekversi",function(){
	
	  loading();
	  $.post("<?php echo site_url("schoolmadrasah/cekversi"); ?>",function(data){
		  
		  
		alertify.alert(data);
		  jQuery.unblockUI({ });
		
		  
	  })
	  
	  
  });
</script>