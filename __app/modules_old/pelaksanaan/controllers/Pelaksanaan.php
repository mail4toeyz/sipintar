<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pelaksanaan extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		 if(!$this->session->userdata("status")){
			    
				echo $this->Reff->sessionhabis();
				exit();
		  }
		  $this->load->model('M_siswa','m');
		
	  }
	  
   function _template($data)
	{
		if($_SESSION['status'] =="madrasah"){
			$this->load->view('madrasah/page_header',$data);	
		}else{
	      $this->load->view('admin/page_header',$data);	
		}	
	}
		
	public function statistik()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		
		 $data['title']   = "Statistik Pelaksanaan  ";
	     if(!empty($ajax)){
					    
			 $this->load->view('page_statistik',$data);
		
		 }else{
			 
		     $data['konten'] = "page_statistik";
			 
			 $this->_template($data);
		 }
	



	}


	public function pengaturan()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		
		 $data['title']   = "Statistik pengaturan  ";
	     if(!empty($ajax)){
					    
			 $this->load->view('pengaturan',$data);
		
		 }else{
			 
		     $data['konten'] = "pengaturan";
			 
			 $this->_template($data);
		 }
	



	}

	public function datapeserta()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		
		 $data['title']   = "Data Peserta AKSI   ";
	     if(!empty($ajax)){
					    
			 $this->load->view('page',$data);
		
		 }else{
			 
		     $data['konten'] = "page";
			 
			 $this->_template($data);
		 }
	

	}

	public function grid(){
		
		$iTotalRecords = $this->m->grid(false)->num_rows();
		
		$iDisplayLength = intval($_REQUEST['length']);
		$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		$iDisplayStart = intval($_REQUEST['start']);
		$sEcho = intval($_REQUEST['draw']);
		
		$records = array();
		$records["data"] = array(); 

		$end = $iDisplayStart + $iDisplayLength;
		$end = $end > $iTotalRecords ? $iTotalRecords : $end;
		
		$datagrid = $this->m->grid(true)->result_array();
		 
		 $i= ($iDisplayStart +1);
		 foreach($datagrid as $val) {
		  
			 $st ="<span class='badge badge-danger'> Belum Memilih Sesi </span>";
				if($val['sesi'] != 0){
				  
					$st = "Sesi ".$val['sesi']." - ".$this->Reff->get_kondisi(array("sesi"=>$val['sesi']),"tm_sesi","hari")."-".$this->Reff->get_kondisi(array("sesi"=>$val['sesi']),"tm_sesi","pukul");

				}

				
				if($_SESSION['status'] !="madrasah"){
				 $paket1 = $this->db->query("select nama,paket from tm_ujian where id='".$val['literasi1']."' ")->row();
				 $paket2 = $this->db->query("select nama,paket from tm_ujian where id='".$val['literasi2']."' ")->row();


                 $literasi = $st;
                 $literasi .= "<br> Literasi I : ".$paket1->nama." - Paket ".$paket1->paket;
                 $literasi .= "<br> Literasi II : ".$paket2->nama." - Paket ".$paket2->paket;

				}else{

					$literasi = $st;
				}
			  

			  $no = $i++;
			  $records["data"][] = array(
				  $no,
				  
				  $val['madrasah'],
				  $val['no_aksi'],
				  dekrip($val['password']),
				  $val['nama'],
				  $literasi
				  
			  
				
			

				);
			}
	  
		$records["draw"] = $sEcho;
		$records["recordsTotal"] = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;
		
		echo json_encode($records);
  }


	
  public function analisispeserta()
  {  
	  
		
		
			  
	   $ajax            = $this->input->get_post("ajax",true);	
	  
	   $data['title']   = "Analisis Peserta  AKSI   ";
	   if(!empty($ajax)){
					  
		   $this->load->view('page_hasil',$data);
	  
	   }else{
		   
		   $data['konten'] = "page_hasil";
		   
		   $this->_template($data);
	   }
  

  }

	 public function grid_hasil(){
		
		  $iTotalRecords = $this->m->grid_hasil(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->grid_hasil(true)->result_array();
		   
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
			
			  
				
				  $device   = $this->db->query("select device  from h_ujian where tmsiswa_id='".$val['id']."' and device !=0 limit 1 ")->row();
				  $literasi = $this->db->query("select status  from h_ujian where tmsiswa_id='".$val['id']."' and literasi='1' ")->row();

				  $dliterasi1 ="<span class='badge badge-danger'> Belum Mengerjakan </span>";
				  if(!is_null($literasi)){
			      if($literasi->status =="Y"){
					
					$dliterasi1 = ' <a type="button" class="btn btn-primary btn-sm " href="'.site_url("cbthasil/literasi?start=".base64_encode($val['literasi1'])."&hash".base64_encode($val['literasi2'])."&literasi=1&tmsiswa_id=".$val['id']."").'">
					  <i class="fa fa-file"></i> Analisis
		              </a>
					';


				   }else{
					$dliterasi1 = ' <a type="button" class="btn btn-success btn-sm " href="'.site_url("cbthasil/literasi?start=".base64_encode($val['literasi1'])."&hash".base64_encode($val['literasi2'])."&literasi=1&tmsiswa_id=".$val['id']."").'">
					<i class="fa fa-file"></i> Analisis
					</a>
				  ';


				   }
				}

				  $literasi2 = $this->db->query("select status  from h_ujian where tmsiswa_id='".$val['id']."' and literasi='2' ")->row();

				  $dliterasi2 ="<span class='badge badge-danger'> Belum  Mengerjakan </span>";
				  if(!is_null($literasi2)){
			      if($literasi2->status =="Y"){
					$dliterasi2 = ' <a type="button" class="btn btn-primary btn-sm " href="'.site_url("cbthasil/literasi?start=".base64_encode($val['literasi2'])."&hash".base64_encode($val['literasi2'])."&literasi=2&tmsiswa_id=".$val['id']."").'">
					  <i class="fa fa-file"></i> Analisis
		              </a>
					';


				  }else{
					$dliterasi2 = ' <a type="button" class="btn btn-success btn-sm " href="'.site_url("cbthasil/literasi?start=".base64_encode($val['literasi2'])."&hash".base64_encode($val['literasi2'])."&literasi=2&tmsiswa_id=".$val['id']."").'">
					<i class="fa fa-file"></i> Analisis
					</a>
				  ';


				  }
				}
               $resetall ="Admin pusat";
				if($_SESSION['status'] !="madrasah"){

					$resetall ='<button type="button" class="btn btn-danger btn-sm resetujian" datanya="'.$val['id'].'" urlnya="'.site_url("pelaksanaan/resetujian").'">
					<i class="fa fa-power-off"></i> Reset Semua
					</button>';
				  
				}

				$resetliterasi1 ="Admin pusat";
				if($_SESSION['status'] !="madrasah"){

					$resetliterasi1 ='<button type="button" class="btn btn-danger btn-sm resetujian" datanya="'.$val['id'].'" urlnya="'.site_url("pelaksanaan/resetliterasi1").'">
					<i class="fa fa-power-off"></i> Reset Literasi I
					</button>';
				  
				}

				$resetliterasi2 ="Admin pusat";
				if($_SESSION['status'] !="madrasah"){

					$resetliterasi2 ='<button type="button" class="btn btn-danger btn-sm resetujian" datanya="'.$val['id'].'" urlnya="'.site_url("pelaksanaan/resetliterasi2").'">
					<i class="fa fa-power-off"></i> Reset Literasi II
					</button>';
				  
				}

				$perangkat ='<b style="color:green;font-size:13px"><i class="fa fa-laptop" aria-hidden="true" ></i> Desktop </b>';
				  if($device->device==2){

					$perangkat ='<b style="color:green;font-size:13px"><i class="fa fa-android" aria-hidden="true" ></i> Smartphone </b>';
				  }else{ 
					$perangkat ='<b style="color:blue;font-size:13px"><i class="fa fa-laptop" aria-hidden="true" ></i> Desktop </b>';
				  }


				$no = $i++;
				$records["data"][] = array(
					$no,
					
					$val['madrasah'],
					$val['no_aksi'],					
					$val['nama'],
					$perangkat,
					
					$dliterasi1,
					$dliterasi2,
					$resetliterasi1,
					$resetliterasi2,
					$resetall
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
	public function resetujian(){
		
		$id = $this->input->get_post("id");

		$this->db->query("update tm_siswa set login='0' where id='".$id."'");
		$this->db->query("delete from h_ujian where tmsiswa_id='".$id."'");
	
		echo "sukses";
		
		
	}

	public function resetliterasi1(){
		
		$id = $this->input->get_post("id");

		$this->db->query("update h_ujian set status='Y' where tmsiswa_id='".$id."' and literasi='1'");
	
		echo "sukses";
		
		
	}
	public function resetliterasi2(){
		
		$id = $this->input->get_post("id");

		$this->db->query("update h_ujian set status='Y' where tmsiswa_id='".$id."' and literasi='2'");
	
		echo "sukses";
		
		
	}



	public function aktifkan(){


		$this->db->set("open",$_POST['status']);
		$this->db->where("id",$_POST['id']);
		$this->db->update("tm_sesi");

		echo "sukses";
	}

	public function form(){
		
		$id = $this->input->get_post("id");
		$data = array();
		   if(!empty($id)){
			   
			  $data['data']  = $this->Reff->get_where("tm_siswa",array("id"=>$id));
			   
		   }
		$this->load->view("form",$data);
		
	}
	
	
	
	public function save(){
     
        $this->form_validation->set_message('required', '{field} Wajib diisi. ');
         $this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , silahkan masukan NISN lain.');
			    $this->form_validation->set_message('min_length', '%s: Minimal  %s digit');
         $this->form_validation->set_message('max_length', '%s: Maksimal  %s digit');
    
		 
			
				$config = array(
				    
				   
				    array('field' => 'f[nama]', 'label' => 'Nama Guru  ', 'rules' => 'trim|required'),
				    array('field' => 'f[kelas]', 'label' => 'Kelas  ', 'rules' => 'trim|required'),
					 array('field' => 'password', 'label' => 'Password ', 'rules' => 'trim|required|min_length[3]'),
					
					  array('field' => 'f[tempat]', 'label' => 'Tempat Lahir  ', 'rules' => 'trim|required'),
				   
			
				   
				  
				   
				   
	
					
				);
				
				$this->form_validation->set_rules($config);	
        
        if ($this->form_validation->run() == true) {
			  
			    $id = $this->input->get_post("id",true);
			    $f  = xssArray($this->input->get_post("f",true));
				
				
				 
				
							 if(empty($id)){
								
								 
								   $this->m->insert();
								   echo "Data Berhasil disimpan";	
								 
								
								
							 }else{
								$this->m->update($id); 
								echo "Data Berhasil disimpan";	
								 
							 }
							 
						
							
						
		
		
							
			     
			    
												 
	     } else {
	         
           
                header('Content-Type: application/json');
                echo json_encode(array('error' => true, 'message' => validation_errors()));
            
        }

		
	
	
	}
	

	
	public function form_excel(){
		
		$id = $this->input->get_post("id");
		$data = array();
		   if(!empty($id)){
			   
			  $data['data']  = $this->Reff->get_where("tm_siswa",array("id"=>$id));
			   
		   }
		$this->load->view("form_excel",$data);
		
	}
	
	public function import_excel()
		{
	  
		 	$this->load->library('PHPExcel/IOFactory');
       // error_reporting(0);
	

	    $sheetup   = 0;
	    $rowup     = 2;
		
		$tmmadrasah_id = $this->input->get_post("tmmadrasah_id");
		$kelas         = $this->input->get_post("kelas");
	
		$fileName = time().".xlsx";
         
        $config['upload_path'] = './__statics/excel/tmp/'; //buat folder dengan nama assets di root folder
        $config['file_name'] = $fileName;
        $config['allowed_types'] = '*';
        $config['max_size'] = 100000;
         
        $this->load->library('upload');
        $this->upload->initialize($config);
         
        if(! $this->upload->do_upload('file') ){
			$jupload=0;
              echo $this->upload->display_errors();

			  
		}else{
             
        $media = $this->upload->data('file');
		
        $inputFileName = './__statics/excel/tmp/'.$fileName;
         
        try {
                $inputFileType = IOFactory::identify($inputFileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }
 
            $sheet = $objPHPExcel->getSheet($sheetup);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
        
             
			 $jupload=0;
            for ($row = $rowup; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE);
				
       
			 	
			 	$nisn        =      (trim($rowData[0][1]));
			 	$nama         =      (trim($rowData[0][2]));			 
			    $tempat       =      (trim($rowData[0][3])); 
			     $tgl_lahir       =      (trim($rowData[0][4]));  
			 
			 	
			 
				$error  ="";
				
			
					 
				
					
				
					$jupload++;
                 $data = array(
                    "nisn"=> $nisn ,                 
                    
					"nama"=> $nama,                   
					"kelas"=> $kelas,                                 
					"tempat"=> $tempat,                   
				                 
                    "tgl_lahir"=> $tgl_lahir                 
                           
				  );
				  
				 
				  
				  $passwordgenerate = strtoupper($this->Reff->get_id(3));
				  $no_aksi          = strtoupper($this->Reff->randomangka(6));
                   
                   $this->db->set("tmmadrasah_id",$tmmadrasah_id);               
                   $this->db->set("no_aksi",$no_aksi);               
                   $this->db->set("password",enkrip($passwordgenerate));
                   $this->db->insert("tm_siswa",$data);
				
				   
				   
				
           
		   
			}
			
			
		
			echo "<br><br><center>Data yg di upload : ". $jupload ." Data <br> <a href='".site_url("datasiswa")."' > Kembali ke Aplikasi </a>";
        
        }
   }
	
	
	public function hapus(){
		
		$id = $this->input->get_post("id",true);
		
		$this->db->delete("tm_siswa",array("id"=>$id));
		echo "sukses";
	}
	// sESI 

	public function sesi()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		
		 $data['title']   = "Pemilihan Sesi Siswa ";
	     if(!empty($ajax)){
					    
			 $this->load->view('page_sesi',$data);
		
		 }else{
			 
		     $data['konten'] = "page_sesi";
			 
			 $this->_template($data);
		 }
	

	}
	
	 public function grid_sesi(){
		
		  $iTotalRecords = $this->m->grid(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->grid(true)->result_array();
		   
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {

			$selek ="<select class='form-control sesi' datanya='".$val['id']."'>";
			$selek .="<option value=''>- Pilih Sesi - </option>";
			  $sesi = $this->db->query("select * from tm_sesi")->result();
			    foreach($sesi as $r){
					 $selected="";
					   if($r->sesi==$val['sesi']){
						  $selected="selected";
					   }
					$selek .="<option value='".$r->sesi."' ".$selected."> Sesi ".$r->sesi." - ".$r->hari." pukul ".$r->pukul."</option>";

				}
				
					
					
		    $selek .="</select>";
			
			 


				$no = $i++;
				$records["data"][] = array(
					$no,
					
					$val['madrasah'],
					$val['no_aksi']."",
					$val['nama'],
					$val['tempat'],
					$val['tgl_lahir'],
					$val['namakelas'],
					$selek
				
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	

	public function pilihsesi(){

	 $sesi = $_POST['sesi'];
	 $id    = $_POST['id'];
	 if($sesi !=""){
	   $cek = $this->db->query("select count(id) as jml from tm_siswa where sesi='".$sesi."'")->row();
	   $jml = $cek->jml;
	 }else{
		$jml = 0;

	 }
	     if($jml >= 3000){

             echo 1;
		 }else{

			$this->db->set("sesi",$sesi);
			$this->db->where("id",$id);
			$this->db->update("tm_siswa");
			echo "sukses";


		 }

	}

	public function infosesi()
	{  
	    
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		
		 $data['title']   = "Informasi Sesi Siswa ";
	     if(!empty($ajax)){
					    
			 $this->load->view('page_sesi_info',$data);
		
		 }else{
			 
		     $data['konten'] = "page_sesi_info";
			 
			 $this->_template($data);
		 }
	

	}


	public function hasilujian()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
         $tmujian_id      = $this->input->get_post("kategori",true);	
		
		 $data['title']   	   = "Hasil Ujian ";
		 $data['tmujian_id']   = $this->input->get_post("kategori",true);
	     if(!empty($ajax)){
					    
			 $this->load->view('hasilujian',$data);
		
		 }else{
			 
		     $data['konten'] = "hasilujian";
			 
			 $this->_template($data);
		 }
	



	}

	public function hasilwawancara()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
      	
		
		 $data['title']   	   = "Hasil Ujian ";
	
	     if(!empty($ajax)){
					    
			 $this->load->view('hasilujian_w',$data);
		
		 }else{
			 
		     $data['konten'] = "hasilujian_w";
			 
			 $this->_template($data);
		 }
	



	}

	public function singkron_nilai(){


			  $pedadogis   = $this->db->query("select * from h_ujian where status='N' and kategori='27'")->result();
			    foreach($pedadogis as $row){
					
					$this->db->set("pedadogis",$row->nilai);
					$this->db->where("id",$row->tmsiswa_id);
					$this->db->update("tm_siswa");

				}
			  


			  $profesional = $this->db->query("select * from h_ujian where status='N' and kategori NOT IN (0,27)")->result();
			  
			  foreach($profesional as $row){
				$this->db->set("profesional",$row->nilai);
				$this->db->where("id",$row->tmsiswa_id);
				$this->db->update("tm_siswa");
					
			  }


			  $peserta = $this->db->query("select profesional,id,pedadogis from tm_siswa where pedadogis IS NOT NULL AND profesional IS NOT NULL")->result();

			    foreach($peserta as $row){

					$nilai = ($row->pedadogis + $row->profesional);
					$this->db->set("nilai",$nilai);
					$this->db->where("id",$row->id);
					$this->db->update("tm_siswa");

				}


				echo "sukses";



	}

	public function peringkat_in(){

		  $posisi = $this->db->query("select id,nama,posisi,kategori ,nilai from tm_siswa where posisi=1 and nilai IS NOT NULL order by nilai DESC,pedadogis DESC, profesional DESC")->result();
		  $no=0;
		  foreach($posisi as $rp){
				$no++;
			 $this->db->set("peringkat",$no);
			 $this->db->where("id",$rp->id);
			 $this->db->update("tm_siswa");
		  }


	}


	public function peringkat_fasprov(){

	  $provinsi = $this->db->query("select * from provinsi")->result();
	   
		foreach($provinsi as $rprov){

			$literasi = $this->db->query("select * from literasi where jenjang IN(1,2,3)")->result();

			  foreach($literasi as $rl){

				$posisi = $this->db->query("select id,nama,posisi,kategori ,nilai from tm_siswa where provinsi='".$rprov->id."' AND literasi='".$rl->id."' AND  posisi=2 and nilai IS NOT NULL order by nilai DESC,pedadogis DESC, profesional DESC")->result();
				
				
					$no=0;
							foreach($posisi as $rp){
								$no++;
							$this->db->set("peringkat",$no);
							$this->db->where("id",$rp->id);
							$this->db->update("tm_siswa");
							}

					}


	}


  }

  public function peringkat_fasda(){

	$provinsi = $this->db->query("select * from kota")->result();
	 
	  foreach($provinsi as $rprov){

	//	$literasi = $this->db->query("select * from literasi where jenjang IN(5,6,7)")->result();
		$literasi = $this->db->query("select * from literasi where id IN(47,48)")->result();

			foreach($literasi as $rl){

			  $posisi = $this->db->query("select id,nama,posisi,kategori ,nilai from tm_siswa where kota='".$rprov->id."' AND literasi='".$rl->id."' AND  posisi=3 and nilai IS NOT NULL order by nilai DESC,pedadogis DESC, profesional DESC")->result();
			  
			  
				  $no=0;
						  foreach($posisi as $rp){
							  $no++;
						  $this->db->set("peringkat",$no);
						  $this->db->where("id",$rp->id);
						  $this->db->update("tm_siswa");
						  }

				  }


  }


}
	 
}
