<div class="alert alert-danger">
 Peserta Seleksi TIP dan TIK 2021
</div>



<div class="row">	
<?php 
   foreach($this->db->get("tm_kategori")->result() as $row){
?>
        <div class="col-lg-4 col-md-4 col-sm-4">
		<div class="card card-stats">
			<div class="card-header card-header-warning card-header-icon">
			<div class="card-icon">
				<i class="fa fa-users"></i>
			</div>
			<p class="card-category" style="font-weight:bold;color:black;font-size:15px"><?php echo $row->nama; ?>    </p>
			<h3 class="card-title" style="font-weight:bold;color:black;font-size:16px">
			 <?php 
			$where ="";
			
		

			
			 $d = $this->db->query("select count(id) as jml from tm_siswa where posisi ='".$row->id."'")->row();
			 echo $this->Reff->formatuang2($d->jml);
			?>
				<small>Peserta</small>
			</h3>
			</div>
			<div class="card-footer">
			
			</div>
		</div>
		</div>
<?php 
   }
?>


		<div class="col-lg-5 col-md-5 col-sm-5">
		<div class="card card-stats">
			<div class="card-header card-header-warning card-header-icon">
			<div class="card-icon">
				<i class="fa fa-sign-in"></i>
			</div>
			<p class="card-category" style="font-weight:bold;color:black;font-size:15px">Sudah Login CBT     </p>
			<h3 class="card-title" style="font-weight:bold;color:black;font-size:16px">
			 <?php 
			$where ="";
			
		

			
			 $d = $this->db->query("select count(id) as jml from tm_siswa where login !=0 $where ")->row();
			 echo $this->Reff->formatuang2($d->jml);
			?>
				<small>Peserta</small>
			</h3>
			</div>
			<div class="card-footer">
			
			</div>
		</div>
		</div>

		



</div>

<div class="alert alert-danger">
Simulasi Ujian Computer Based Test 
</div>


<div class="row">	
<?php 
   foreach($this->db->get("tm_kategori")->result() as $row){
?>
        <div class="col-lg-4 col-md-4 col-sm-4">
		<div class="card card-stats">
			<div class="card-header card-header-info card-header-icon">
			<div class="card-icon">
				<i class="fa fa-retweet"></i>
			</div>
			<p class="card-category" style="font-weight:bold;color:black;font-size:15px"><b><?php echo $row->nama; ?></b>    </p>
			<h3 class="card-title" style="font-weight:bold;color:black;font-size:16px">
			 <?php 
			 $meng  = $this->db->query("select count(id) as jml from h_ujian  where status='Y' AND kategori='0' and tmsiswa_id IN(select id from tm_siswa where posisi ='".$row->id."')")->row();
			 $sel   = $this->db->query("select count(id) as jml from h_ujian  where status='N' AND kategori='0' and tmsiswa_id IN(select id from tm_siswa where posisi ='".$row->id."')")->row();
			
			?>
				<small><b><?php echo  $this->Reff->formatuang2($meng->jml); ?></b> sedang mengerjakan</small> <br>
				<small><b><?php echo  $this->Reff->formatuang2($sel->jml); ?></b> selesai </small>
			</h3>
			</div>
			<div class="card-footer">
			
			</div>
		</div>
		</div>
<?php 
   }

   $ikut  = $this->db->query("select count(id) as jml from h_ujian  where kategori='0' ")->row();
?>

<div class="col-lg-5 col-md-5 col-sm-5">
		<div class="card card-stats">
			<div class="card-header card-header-info card-header-icon">
			<div class="card-icon">
				<i class="fa fa-retweet"></i>
			</div>
			<p class="card-category" style="font-weight:bold;color:black;font-size:15px"><b><?php echo $ikut->jml; ?> Ikut Simulasi </b>    </p>
			<h3 class="card-title" style="font-weight:bold;color:black;font-size:16px">
			 <?php 
			 $meng  = $this->db->query("select count(id) as jml from h_ujian  where status='Y' AND kategori='0' ")->row();
			 $sel   = $this->db->query("select count(id) as jml from h_ujian  where status='N' AND kategori='0' ")->row();
			 $d = $this->db->query("select count(id) as jml from tm_siswa  ")->row();
			
			?>
				<!-- <small><?php //echo  number_format(($ikut->jml/$d->jml) * 100,2);  ?> Tingkat Partisipasi </small>  --> 
				<small><b><?php echo  $this->Reff->formatuang2($meng->jml); ?></b> sedang mengerjakan</small> <br>
				<small><b><?php echo  $this->Reff->formatuang2($sel->jml); ?></b> selesai </small>
			</h3>
			</div>
			<div class="card-footer">
			
			</div>
		</div>
		</div>

</div>
<div class="alert alert-danger">
Pelaksanaan Jumlah Peserta Computer Based Test  Evaluasi Diri
</div>

<div class="row">	
<?php 
   foreach($this->db->get("tm_kategori")->result() as $row){
?>
        <div class="col-lg-4 col-md-4 col-sm-4">
		<div class="card card-stats">
			<div class="card-header card-header-danger card-header-icon">
			<div class="card-icon">
				<i class="fa fa-file"></i>
			</div>
			<p class="card-category" style="font-weight:bold;color:black;font-size:15px"><b><?php echo $row->nama; ?></b>    </p>
			<h3 class="card-title" style="font-weight:bold;color:black;font-size:16px">
			 <?php 
			 $pes   = $this->db->query("select count(id) as jml from tm_siswa where posisi ='".$row->id."'")->row();
			 $meng  = $this->db->query("select count(id) as jml from h_ujian  where status='Y' AND kategori NOT IN(0) and tmsiswa_id IN(select id from tm_siswa where posisi ='".$row->id."') ")->row();
			 $sel   = $this->db->query("select count(id) as jml from h_ujian  where status='N' AND kategori NOT IN(0) and tmsiswa_id IN(select id from tm_siswa where posisi ='".$row->id."') ")->row();
			
			?>
				<small><b><?php echo  $this->Reff->formatuang2($pes->jml); ?></b> Peserta Tes </small> <br>
				<small><b><?php echo  $this->Reff->formatuang2($meng->jml); ?></b> sedang mengerjakan</small> <br>
				<small><b><?php echo  $this->Reff->formatuang2($sel->jml); ?></b> selesai </small> <br>
				<small><b><?php echo  number_format((($sel->jml/$pes->jml)*100),2); ?></b> % Tingkat Partisipasi </small>
			</h3>
			</div>
			<div class="card-footer">
			
			</div>
		</div>
		</div>
<?php 
   }
?>

<div class="col-lg-6 col-md-6 col-sm-6">
		<div class="card card-stats">
			<div class="card-header card-header-danger card-header-icon">
			<div class="card-icon">
				<i class="fa fa-file"></i>
			</div>
			<p class="card-category" style="font-weight:bold;color:black;font-size:15px"><b>Total Ujian </b>    </p>
			<h3 class="card-title" style="font-weight:bold;color:black;font-size:16px">
			 <?php 
			 $pes   = $this->db->query("select count(id) as jml from tm_siswa")->row();
			 $meng  = $this->db->query("select count(id) as jml from h_ujian  where status='Y' AND kategori NOT IN(0) limit 1")->row();
			 $sel   = $this->db->query("select count(id) as jml from h_ujian  where status='N' AND kategori NOT IN(0) limit 1")->row();
			
			?>
				<small><b><?php echo  $this->Reff->formatuang2($pes->jml); ?></b> Peserta Tes</small> <br>
				<small><b><?php echo  $this->Reff->formatuang2($meng->jml); ?></b> sedang mengerjakan</small> <br>
				<small><b><?php echo  $this->Reff->formatuang2($sel->jml); ?></b> selesai </small> <br>
				<small><b><?php echo  number_format((($sel->jml/$pes->jml)*100),2); ?></b> % Tingkat Partisipasi </small>
			</h3>
			</div>
			<div class="card-footer">
			
			</div>
		</div>
		</div>

</div>
