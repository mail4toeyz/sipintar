<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Schoolmadrasah extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		 if(!$this->session->userdata("status")){
			    
				echo $this->Reff->sessionhabis();
				exit();
		  }
		  $this->load->model('M_madrasah','m');
		
	  }
	  
   function _template($data)
	{
		if($_SESSION['status'] =="madrasah"){
			$this->load->view('madrasah/page_header',$data);	
		}else{
	      $this->load->view('admin/page_header',$data);	
		}	
	}
		
	public function index()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		 $data['data']	  = $this->Reff->get_where("tm_madrasah",array("id"=>$_SESSION['madrasah_id']));
		 $data['title']   = "Profile Madrasah ";
	     if(!empty($ajax)){
					    
			 $this->load->view('page',$data);
		
		 }else{
			 
		     $data['konten'] = "page";
			 
			 $this->_template($data);
		 }
	

	}
	
	
	
	
	public function save(){
     
        $this->form_validation->set_message('required', '{field} Wajib diisi. ');
         $this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , silahkan masukan NUPTK lain.');
				 
		 
			
				$config = array(
				    
				   
					array('field' => 'f[nama]', 'label' => 'Nama Madrasah  ', 'rules' => 'trim|required'),
					array('field' => 'f[password]', 'label' => 'Password   ', 'rules' => 'trim|required|min_length[6]'),
				   
	
					
				);
				
				$this->form_validation->set_rules($config);	
        
        if ($this->form_validation->run() == true) {
			  
			   
			    $f  = xssArray($this->input->get_post("f",true));
				
			
				$this->db->where("id",$_SESSION['madrasah_id']);
				$this->db->update("tm_madrasah",$f);
				echo "sukses";
		
		
							
			     
			    
												 
	     } else {
	         
           
                header('Content-Type: application/json');
                echo json_encode(array('error' => true, 'message' => validation_errors()));
            
        }

		
	
	
	}
	
	
	public function setting()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		 $data['data']	  = $this->Reff->get_where("tm_madrasah",array("id"=>$_SESSION['tmmadrasah_id']));
		 $data['title']   = "Pengaturan Tahun Pelajaran ";
	     if(!empty($ajax)){
					    
			 $this->load->view('page_setting',$data);
		
		 }else{
			 
		     $data['konten'] = "page_setting";
			 
			 $this->_template($data);
		 }
	

	}
	
	public function saves(){
		
	        
			    $f  = xssArray($this->input->get_post("f",true));
				
				
				$this->db->where("id",$_SESSION['tmmadrasah_id']);
				$this->db->update("tm_madrasah",$f);
				redirect(base_url()."schoolmadrasah/setting");
		
	}
	
	public function akun(){  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		 $data['data']	  = $this->Reff->get_where("tm_madrasah",array("id"=>$_SESSION['tmmadrasah_id']));
		 $data['title']   = "Pengaturan Akun Anda ";
	     if(!empty($ajax)){
					    
			 $this->load->view('page_akun',$data);
		
		 }else{
			 
		     $data['konten'] = "page_akun";
			 
			 $this->_template($data);
		 }
	

	}
	
	
	public function saveakun(){
     
        $this->form_validation->set_message('required', '{field} Wajib diisi. ');
         $this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , silahkan masukan NUPTK lain.');
		 $this->form_validation->set_message('min_length', '%s: Minimal  %s digit');
		 $this->form_validation->set_message('matches', '{field} Tidak Sama dengan yang dikolom password .');
			
				$config = array(
				    
				    array('field' => 'password', 'label' => 'Password Baru  ', 'rules' => 'trim|required|min_length[6]'),
				    array('field' => 'passwordc', 'label' => 'Konfirmasi Password  ', 'rules' => 'trim|required|min_length[6]|matches[password]'),
				   
				   
				   
	
					
				);
				
				$this->form_validation->set_rules($config);	
        
        if ($this->form_validation->run() == true) {
			  
			   
				if($_SESSION['status']=="madrasah"){
					$this->db->set("password",enkrip($_POST['password']));
					$this->db->where("id",$_SESSION['tmmadrasah_id']);
					$this->db->update("tm_madrasah",$f);
					echo "sukses";
				}else{
					$this->db->set("password",enkrip($_POST['password']));
					$this->db->where("id",$_SESSION['tmeksekutif_id']);
					$this->db->update("tm_eksekutif",$f);
					echo "sukses";
					
					
				}
		
		
							
			     
			    
												 
	     } else {
	         
           
                header('Content-Type: application/json');
                echo json_encode(array('error' => true, 'message' => validation_errors()));
            
        }

		
	
	
	}
	
	public function statistik(){  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		 $data['data']	  = $this->Reff->get_where("tm_madrasah",array("id"=>$_SESSION['tmmadrasah_id']));
		 $data['title']   = "Statistik dan Grafik ";
	     if(!empty($ajax)){
					    
			 $this->load->view('page_statistik',$data);
		
		 }else{
			 
		     $data['konten'] = "page_statistik";
			 
			 $this->_template($data);
		 }
	

	}
	
	public function aktifitas(){  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
	
		 $data['title']   = "Aktifitas Users";
	     if(!empty($ajax)){
					    
			 $this->load->view('page_aktifitas',$data);
		
		 }else{
			 
		     $data['konten'] = "page_aktifitas";
			 
			 $this->_template($data);
		 }
	

	}
	
	
	// Singkronisasi 
	
	
	public function singkronisasi()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		 $data['data']	  = $this->Reff->get_where("tm_madrasah",array("id"=>$_SESSION['tmmadrasah_id']));
		 $data['title']   = "Singkronisasi Data  ";
	     if(!empty($ajax)){
					    
			 $this->load->view('singkronisasi',$data);
		
		 }else{
			 
		     $data['konten'] = "singkronisasi";
			 
			 $this->_template($data);
		 }
	

	}
	
	 public function singkron(){
	   
	   $nsm  = $this->Reff->get_kondisi(array("id"=>$_SESSION['tmmadrasah_id']),"tm_madrasah","nsm");
	
	   
	   
	   $ajaran   = $this->Reff->ajaran();
	   $semester = $this->Reff->semester();
	   
	   $guru   = $this->db->query("select count(id) as jml from tm_guru where tmmadrasah_id='".$_SESSION['tmmadrasah_id']."'")->row();
	   
	   $siswa  = $this->db->query("select count(id) as jml from tm_siswa where tmmadrasah_id='".$_SESSION['tmmadrasah_id']."' and ajaran='".$ajaran."'")->row();
	   
	   $kelas  = $this->db->query("select count(id) as jml from tr_kelas where tmmadrasah_id='".$_SESSION['tmmadrasah_id']."' and ajaran='".$ajaran."' and semester='".$semester."'")->row();
	   
	   $aktifitas   = $this->db->query("select count(id) as jml from notifikasi ")->row();
	   
	   
	   
	   $jml_guru       = $guru->jml;
	   $jml_siswa      = $siswa->jml;
	   $jml_kelas      = $kelas->jml;
	   $jml_aktifitas  = $aktifitas->jml;
	   
	   $data = array("nsm"=>$nsm,
					 "ajaran"=>$ajaran,
					 "semester"=>$semester,
					 "jml_guru"=>$jml_guru,
					 "jml_siswa"=>$jml_siswa,
					 "jml_kelas"=>$jml_kelas,
					 "jml_aktifitas"=>$jml_aktifitas);
	   
	   echo $this->m->post('https://elearning.kemenag.go.id/web/sync', $data);
	   
	   
   }
   
    public function singkronprofile(){
	   
	   $nsm  = $this->Reff->get_kondisi(array("id"=>$_SESSION['tmmadrasah_id']),"tm_madrasah","nsm");
	
	   
	  
	   $data  = array("nsm"=>$nsm);
	   
	   $result =  ($this->m->post('https://elearning.kemenag.go.id/web/sync_profile', $data));
	   
	   $data = json_decode($result,true); 
	  
	   $this->db->where("nsm",$nsm);
	   $this->db->update("tm_madrasah",$data);
	   echo $result;
	   
   }
   
     public function cekversi(){
	   
	   $nsm  = $this->Reff->get_kondisi(array("id"=>$_SESSION['tmmadrasah_id']),"tm_madrasah","nsm");
	
	   
	  
	   $data  = array("nsm"=>$nsm);
	   
	   echo $result =  ($this->m->post('https://elearning.kemenag.go.id/web/cekversi', $data));
	   
	   
	   
   }
   
 
   public function simpan_profile_s(){
	   
	   $this->db->set("nama",$_POST['a']['nama']);
	   $this->db->set("provinsi_id",$_POST['a']['provinsi_id']);
	   $this->db->set("kota_id",$_POST['a']['kota_id']);
	   $this->db->set("kecamatan_id",$_POST['a']['kecamatan_id']);
	   $this->db->where("id",$_SESSION['tmmadrasah_id']);
	   $this->db->update("tm_madrasah");
	   echo "yeay";
	   
	   
   }
   
   public function hapusguru(){
	   
	   $this->db->query("delete from tm_guru");
	   $this->db->query("delete from tr_kelas");
	   $this->db->query("delete from tr_post");
	   echo "sukses";
	   
	   
   }
   
   public function hapusdatasiswa(){
	   $kelas = $this->input->get_post("kelas");
	   $rombel = $this->input->get_post("rombel");
	   
	   $this->db->where("kelas",$kelas);
	   $this->db->where("rombel",$rombel);
	   $this->db->delete("tm_siswa");
	   
	   echo "sukses";
	   
   }
   public function notifikasireal(){
	   $datanya = $this->input->get_post("datanya");
	   
	   $this->db->set("verifikasi",$datanya);
	   $this->db->where("id",$_SESSION['tmmadrasah_id']);
	   $this->db->update("tm_madrasah");
	   
	   echo "sukses";
   }
   
   public function zonasiwaktu(){
	   
	   $datanya = $this->input->get_post("datanya");
	   
	   $this->db->set("sk",$datanya);
	   $this->db->where("id",$_SESSION['tmmadrasah_id']);
	   $this->db->update("tm_madrasah");
	   
	   echo "sukses";
	   
	   
   }
   
   public function resetlogin(){
	   
	    $this->db->query("update tm_siswa set pekerjaan_wali='0'");
	    $this->db->query("update tm_guru set status='0'");
		echo "sukses";
   }
   
   // Migrasi 
   
   public function migrasi()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		 $data['data']	  = $this->Reff->get_where("tm_madrasah",array("id"=>$_SESSION['tmmadrasah_id']));
		 $data['title']   = "Version Specific Migration ";
	     if(!empty($ajax)){
					    
			 $this->load->view('page_migrasi',$data);
		
		 }else{
			 
		     $data['konten'] = "page_migrasi";
			 
			 $this->_template($data);
		 }
	

	}
	
	public function lakukanmigrasi(){
		   $this->load->dbforge();
		$jenis = $this->input->get_post("jenis");
		
		if($jenis==1){
		    $ajaran = array(
                         'ajaran' => array(
                                                 'type' => 'char',
                                                 'constraint' => 4
                                          )
                        
                        );
			
			if (!$this->db->field_exists('ajaran', 'tr_ruangkelas'))
               {
	
                 $this->dbforge->add_column('tr_ruangkelas', $ajaran);
				 
				   $this->db->query("update tr_ruangkelas set ajaran='2019'");
			   }else{
				   
				   $this->db->query("update tr_ruangkelas set ajaran='2019'");
			   }
			   
			   
		}else if($jenis==2){
			  $alumni = array(
                         'id' => array(
                                                 'type' => 'INT',
                                                 'constraint' => 11,
                                                 'unsigned' => TRUE,
                                                 'auto_increment' => TRUE
                                          ),
                        'tmsiswa_id' => array(
                                                 'type' => 'INT',
                                                 'constraint' => 11
                                          ),
										  
						'nisn' => array(
                                                 'type' => 'VARCHAR',
                                                 'constraint' => 50,
												  'null' => TRUE
                                          ),
					    'nama' => array(
                                                 'type' => 'VARCHAR',
                                                 'constraint' => 300
                                          ),	

						'tempat' => array(
                                                 'type' => 'VARCHAR',
                                                 'constraint' => 100,
												  'null' => TRUE
                                          ),

						'gender' => array(
                                                 'type' => 'VARCHAR',
                                                 'constraint' => 100,
												  'null' => TRUE
                                          ),										  

						'tgl_lahir' => array(
                                                 'type' => 'date',
												  'null' => TRUE
                                          ),

						'foto' => array(
                                                 'type' => 'VARCHAR',
                                                 'constraint' => 100,
												 'null' => TRUE
                                          ),

                        'folder' => array(
                                                 'type' => 'VARCHAR',
                                                 'constraint' => 100,
												  'null' => TRUE
                                          ),


                        'ajaran' => array(
                                                 'type' => 'VARCHAR',
                                                 'constraint' => 10
                                          ),										  
                        
                        
                        );
						
	   
			   if (!$this->db->table_exists("tr_alumni") )
				   {
						   $this->dbforge->add_key('id', TRUE);
						   $this->dbforge->add_field($alumni);
						   $this->dbforge->create_table('tr_alumni');
				   }
				   
		}else if($jenis==3){
			
			
			 $this->db->query("update tr_ruangkelas set ajaran='2019'");
			
		}else if($jenis==4){
			
			$vicon = array(
                         'id' => array(
                                                 'type' => 'INT',
                                                 'constraint' => 11,
                                                 'unsigned' => TRUE,
                                                 'auto_increment' => TRUE
                                          ),
										  
						'jenis' => array(
                                                 'type' => 'INT',
                                                 'constraint' => 11
                                          ),
										  
					    'status' => array(
                                                 'type' => 'INT',
                                                 'constraint' => 11
                                        ),
                        'trkelas_id' => array(
                                                 'type' => 'INT',
                                                 'constraint' => 11
                                          ),
										  
						'kode' => array(
                                                 'type' => 'VARCHAR',
                                                 'constraint' => 50,
												  'null' => TRUE
                                          ),
					    'nama' => array(
                                                 'type' => 'VARCHAR',
                                                 'constraint' => 300
                                          ),	

						'partisipan' => array(
                                                 'type' => 'text',
												  'null' => TRUE
                                          ),
										  
						 'partisipan_mulai' => array(
                                                  'type' => 'text',
												  'null' => TRUE
                           ),

						'partisipan_selesai' => array(
                                                  'type' => 'text',
												  'null' => TRUE
                           ),									  

						'tgl_mulai' => array(
                                                 'type' => 'DATETIME',
												  'null' => TRUE
                                          ),

						'tgl_selesai' => array(
                                                 'type' => 'DATETIME',
                                                 
												 'null' => TRUE
                                          ),
									  
                        
                        
                        );
						
	   
			   if (!$this->db->table_exists("tr_vicon") )
				   {
						   $this->dbforge->add_key('id', TRUE);
						   $this->dbforge->add_field($vicon);
						   $this->dbforge->create_table('tr_vicon');
				   }
			
			
		}
		
	}
	
	
	public function grid(){
		
		  $iTotalRecords = $this->m->grid(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->grid(true)->result_array();
		  
		  
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
			    
			   
				$no = $i++;
				$records["data"][] = array(
					$no,
			
              	
					
					
					$val['keterangan'],
					$this->Reff->timeAgo($val['tanggal']),
					
		
				   
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
	public function gridnotif(){
		
		  $iTotalRecords = $this->m->gridnotif(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->gridnotif(true)->result_array();
		  
		  
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
			    
			   
				$no = $i++;
				$records["data"][] = array(
					$no,
			
              	
					$val['keterangan'],
					$this->Reff->timeAgo($val['tanggal']),
					
		
				   
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	 
}
