  <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-success">
                  <h4 class="card-title">Aktifitas </h4>
                  <p class="card-category">Aktifitas Aplikasi CBT </p>
                </div>
                <div class="card-body">
                    <div class="row">
					
					
					
					
					
					
					<div class="col-lg-12 col-md-12">
              <div class="card">
                <div class="card-header card-header-tabs card-header-primary">
                  <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                      <span class="nav-tabs-title">Users :</span>
                      <ul class="nav nav-tabs" data-tabs="tabs">
                        <li class="nav-item">
                          <a class="nav-link active" href="#eksekutif" data-toggle="tab">
                              Administrator
                            <div class="ripple-container"></div>
                          </a>
                        </li>
                       
                        <li class="nav-item">
                          <a class="nav-link" href="#siswa" data-toggle="tab">
                           Peserta 
                            <div class="ripple-container"></div>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="card-body">
                  <div class="tab-content">
				  
				  
                    <div class="tab-pane active" id="eksekutif">
                      
					<div class="table-responsive">
                                <table class="table table-bordered table-striped  " id="eksekutiftable" width="99%">
                                    <thead class="bg-blue">
                                        <tr>
                                            <th width="2px">NO</th>
                                            
                                            
                                            <th>KETERANGAN </th>
                                            <th>WAKTU </th>
                                            
                                            
                                        </tr>
                                    </thead>
                                   
                                    <tbody>
                                        
                                        
                                    </tbody>
                                </table>
                            </div>
                    </div>
					
					
					 <div class="tab-pane " id="guru">
                     <div class="table-responsive">
                                <table class="table table-bordered table-striped  " id="gurutabel" width="99%">
                                    <thead class="bg-blue">
                                        <tr>
                                            <th width="2px">NO</th>                                           
                                            
                                           
                                           
                                            <th>AKTIFITAS </th>
                                          
                                            <th>WAKTU </th>
                                            
                                            
                                        </tr>
                                    </thead>
                                   
                                    <tbody>
                                        
                                        
                                    </tbody>
                                </table>
                            </div>
							
							
                    </div>
					
					 <div class="tab-pane " id="siswa">
                      <div class="table-responsive">
                                <table class="table table-bordered table-striped  " id="siswatabel" width="99%">
                                    <thead class="bg-blue">
                                        <tr>
                                            <th width="2px">NO</th>                                           
                                            
                                           
                                           
                                            <th>AKTIFITAS </th>
                                          
                                            <th>WAKTU </th>
                                            
                                            
                                        </tr>
                                    </thead>
                                   
                                    <tbody>
                                        
                                        
                                    </tbody>
                                </table>
                            </div>
                    </div>
			


     			</div>
                </div>
              </div>
            </div>
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
            
          
                    </div>
                </div>
              </div>
            </div>
        


<script type="text/javascript">
  var dataTable = $('#eksekutiftable').DataTable( {
						"processing": true,
						"language": {
						"processing": '<div class="preloader pl-size-l"><div class="spinner-layer pl-red-grey"><div class="circle-clipper left"> <div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data kosong",
						   "sLengthMenu": "&nbsp;&nbsp; Menampilkan   _MENU_ Data"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": false,
					"lengthMenu": [[10,25, 50,100,200,300,500,1000, 800000000], [10,25, 50,100,200,300,500,1000,"All"]],
					 
					 "sPaginationType": "full_numbers",
					 "dom": 'Blfrtip',
					"buttons": [
					
                         	
						    {
							extend: 'excelHtml5',
							exportOptions: {
							  columns: [ 0,1,2]
							},
							text:'<font color:"black">Cetak Excel</font>',
							
							}
					],
					
					"ajax":{
						url :"<?php echo site_url("schoolmadrasah/grid?id=1"); ?>", 
						type: "post", 
						"data": function ( data ) {
						
						
					
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				
				
			
var dataTable = $('#gurutabel').DataTable( {
						"processing": true,
						"language": {
						"processing": '<div class="preloader pl-size-l"><div class="spinner-layer pl-red-grey"><div class="circle-clipper left"> <div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data kosong",
						   "sLengthMenu": "&nbsp;&nbsp; Menampilkan   _MENU_ Data"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": false,
					"lengthMenu": [[10,25, 50,100,200,300,500,1000, 800000000], [10,25, 50,100,200,300,500,1000,"All"]],
					 
					 "sPaginationType": "full_numbers",
					 "dom": 'Blfrtip',
					"buttons": [
					
                         	
						    {
							extend: 'excelHtml5',
							
							text:'<font color:"black">Cetak Excel</font>',
							
							}
					],
					
					"ajax":{
						url :"<?php echo site_url("schoolmadrasah/grid?id=2"); ?>", 
						type: "post", 
						"data": function ( data ) {
						
						
					
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				
				

		
var dataTable = $('#siswatabel').DataTable( {
						"processing": true,
						"language": {
						"processing": '<div class="preloader pl-size-l"><div class="spinner-layer pl-red-grey"><div class="circle-clipper left"> <div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data kosong",
						   "sLengthMenu": "&nbsp;&nbsp; Menampilkan   _MENU_ Data"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": false,
					"lengthMenu": [[10,25, 50,100,200,300,500,1000, 800000000], [10,25, 50,100,200,300,500,1000,"All"]],
					 
					 "sPaginationType": "full_numbers",
					 "dom": 'Blfrtip',
					"buttons": [
					
                         	
						    {
							extend: 'excelHtml5',
							
							text:'<font color:"black">Cetak Excel</font>',
							
							}
					],
					
					"ajax":{
						url :"<?php echo site_url("schoolmadrasah/grid?id=3"); ?>", 
						type: "post", 
						"data": function ( data ) {
						
						
					
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				
						
	


</script>		