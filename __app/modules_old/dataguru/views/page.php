

	<div class="row">	  

		<div class="col-lg-6 col-md-6 col-sm-6">
		<div class="card card-stats">
			<div class="card-header card-header-warning card-header-icon">
			<div class="card-icon">
				<i class="fa fa-line-chart"></i>
			</div>
			<p class="card-category">Sedang Mengerjakan </p>
			<h3 class="card-title">
			 <?php 
			 $d = $this->db->query("select id as jml from h_ujian where tgl_akhir IS  NULL group by tmsiswa_id")->num_rows();
			 echo $d;
			?>
				<small>Peserta</small>
			</h3>
			</div>
			<div class="card-footer">
			
			</div>
		</div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6">
		<div class="card card-stats">
			<div class="card-header card-header-danger card-header-icon">
			<div class="card-icon">
				<i class="fa fa-line-chart"></i>
			</div>
			<p class="card-category"> Selesai Mengerjakan </p>
			<h3 class="card-title">  <?php 
			$d = $this->db->query("select id as jml from h_ujian where tgl_akhir IS NOT  NULL group by tmsiswa_id")->num_rows();
			 echo $d;
			?>
				<small>Peserta</small>
			</h3>
			</div>
			<div class="card-footer">
			
			</div>
		</div>
		</div>



</div>

<div class="col-md-12">
		
				       <div class="card">
						<div class="card-header card-header-success">
						  <h4 class="card-title">Data Guru</h4>
						  <p class="card-category">Data Guru Pengawas dan Proktor  </p>
						</div>
					  
						 <div class="card-body">
						 <?php 
						   if($_SESSION['status'] !="madrasah"){
							   ?>
								<button type="button" target="#loadform" url="<?php echo site_url("dataguru/form_excel"); ?>"  data-toggle="modal" data-target="#defaultModal" data-color="cyan" class="btn btn-success btn-sm addmodal"><span class="fa fa-file-excel-o"></span> IMPORT EXCEL </button>
							<?php 
						   }
						   ?>
					   <div class="row" >
						 
					      

						        		
								<div class="col-md-5">
								
									<div class="input-group">
									<input type="text" class="form-control" id="keyword"  placeholder="Cari  Nama disini..">
									<div class="input-group-append">
										<button class="btn btn-success btn-sm" type="button">CARI</button>
									</div>
									</div>
								</div>
					   </div>
					   
					   <br>


							<div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover " id="datatableTable" width="99%">
                                    <thead class="bg-blue">
                                        <tr>
                                            <th width="2px">NO</th>
                                            <th>NAMA </th>
                                            
                                            <th>JABATAN </th>
                                            <th>ASAL  </th>
                                            <th>KABUPATEN </th>
											<th>PROVINSI </th> 
                                            
										    <th>NO AKSI</th>
                                            <th>PASSWORD</th>											
                                            <th>LITERASI 1</th>											
                                            <th>LITERASI 2</th>											
                                           
                                            
                                        </tr>
                                    </thead>
                                   
                                    <tbody>
                                        
                                        
                                    </tbody>
                                </table>
                         </div>
                         </div>
                         </div>
                 
 </div> 

 <style>
 .modal-lg {
    max-width: 80%;
}
</style>
      <div id="defaultModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
	     Monitor Jawaban 
        <button type="button" class="close" data-dismiss="modal">&times;</button>
       
      </div>
      <div class="modal-body" id="loadform">
        <p>Loading...</p>
      </div>
      
    </div>

  </div>
</div>

	
<script type="text/javascript">
  var dataTable = $('#datatableTable').DataTable( {
						"processing": true,
						"language": {
						"processing": '<div class="preloader pl-size-l"><div class="spinner-layer pl-red-grey"><div class="circle-clipper left"> <div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data kosong",
						   "sLengthMenu": "&nbsp;&nbsp; Menampilkan   _MENU_ Data"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": false,
					"lengthMenu": [[10,25, 50,100,200,300,500,1000, 800000000], [10,25, 50,100,200,300,500,1000,"All"]],
					 "dom": 'Blfrtip',
					 "sPaginationType": "full_numbers",
					"buttons": [
					
                         	
						    {
							extend: 'excelHtml5',
							exportOptions: {
							  columns: [ 0,1,2,3,4,5,6,7]
							},
							text:'Cetak Excel',
						
							},
							
							
							
							
						
							{
							extend: 'colvis',
							
							text:' Pengaturan Kolom ',
							
						}
					],
					
					"ajax":{
						url :"<?php echo site_url("dataguru/grid"); ?>", 
						type: "post", 
						"data": function ( data ) {
						
							data.keyword = $("#keyword").val();
							data.tmmadrasah_id = $("#tmmadrasah_id").val();
							data.kelas = $("#kelas").val();
					
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				
				
				
	
				$(document).on("input","#keyword",function(){
	  
					dataTable.ajax.reload(null,false);	
					
				})	

				$(document).on("change","#tmmadrasah_id,#kelas",function(){
						
						dataTable.ajax.reload(null,false);	
						
					})	

</script>
				