

<div class="acc-setting">
										<h3 id="titleujian"> <i class="fa fa-file"></i> Bank Soal  </h3>
										
										<div class="cp-field ">
										<div class="alert alert-warning" style="background-color:#252b87;color:white;">
													<?php 
													  $ujiannya = $this->db->query("select count(id) as jml from tm_ujian")->row();
													  $soalnya = $this->db->query("select count(id) as jml from tr_soal")->row();
													  
													  
													  ?>
													  
													  <input type="text" id="tmujian_id" value="<?php echo $ujian->id; ?>">
													  Terdapat <u><?php echo ($soalnya->jml); ?> Soal</u> pada Bank Soal  Anda   dari pelaksanaan <u><?php echo $ujiannya->jml; ?></u> Ujian Berbasis CBT  
													</div>
										
										
														<br>
														
													     <div class="table-responsive">
														   <table style="font-size:12px" id="datatablebank" width="100%" class="table table-hover table-striped table-bordered">
														    <thead class="bg-blue">
															  <tr>
															    <th width="5px"> NO </th>
																<th> NAMA UJIAN </th>
																
															    
															    <th> JUMLAH SOAL </th>
															    <th> TARIK SOAL </th>
																
															   
																
															   
															   
															   
															
															  </tr>
															</thead>
														   
														    <tbody>
															
															  
															  
															</tbody>
														   
														   
														   
														   </table>
														 
														 
														 
														  <br>
														   
														 
														 </div>
													
													
												
										
										</div>
</div>


<div id="MyModalAgenda" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" style="width:100%; height: 100%;padding:0;margin:0">

    <!-- Modal content-->
    <div class="modal-content ">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="color:white">Detail Bank Soal Madrasah </h4>
      </div>
      <div class="modal-body" id="loadinghasil">
        <p>Sedang memuat, mohon tunggu...</p>
      </div>
      <div class="modal-footer">
        
      </div>
    </div>

  </div>
</div>



<script type="text/javascript">
$(function() {
var datatablebank = $('#datatablebank').DataTable( {
						"processing": true,
						"language": {
						"processing": '<div class="preloader pl-size-l"><div class="spinner-layer pl-red-grey"><div class="circle-clipper left"> <div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Belum ada Rencana Pembelajaran",
						   "sLengthMenu": "&nbsp;&nbsp; Menampilkan   _MENU_ Data"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": false,
					"lengthMenu": [[5,25, 50,100,200,300,500,1000, 800000000], [5,25, 50,100,200,300,500,1000,"All"]],
					 
					 "sPaginationType": "full_numbers",
					 
					 
					
					"ajax":{
						url :"<?php echo site_url("teacherujian/grid_bank"); ?>", 
						type: "post", 
						"data": function ( data ) {
						
						 data.tmujian_id = $("#tmujian_id").val();
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
	

				
			
		} );	  
</script>