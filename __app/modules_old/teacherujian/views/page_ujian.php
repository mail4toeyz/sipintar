<link href="<?php echo base_url(); ?>__statics/js/datatable/button.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>__statics/js/datatable/datatables.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url(); ?>__statics/js/datatable/table.css" rel="stylesheet" type="text/css"/>


<script src="<?php echo base_url(); ?>__statics/js/datatable/datatables.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>__statics/js/datatable/button.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>__statics/js/datatable/jszip.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>__statics/js/datatable/html5.js" type="text/javascript"></script>
  

	<div class="table-responsive">
														   <table  id="datatablesisbanksoal" width="10%" style="font-size:12px" class="table table-hover table-striped table-bordered">
														    <thead class="bg-blue">
															  <tr>
															    <th> NO </th>
															    
															    <th> KELAS </th>
															    <th> UJIAN </th>

															    <th> WAKTU </th>
															    <th> JENIS </th>
															   
															    <th> PILIH </th>
															   
															
															  </tr>
															</thead>
														   
														    <tbody>
															
															  <?php 
															  $siswa = $this->db->select("*")->order_by("trkelas_id,nama","asc")->where("id !='".$tmujian_id."' and nama !='' and tmguru_id='".$_SESSION['tmguru_id']."'")->get("tm_ujian")->result();
															   $no=1;
															   foreach($siswa as $rs){
																    $kelas = $this->db->get_where("tr_kelas",array("id"=>$rs->trkelas_id))->row();
																	
																	$jml_soal = $this->db->query("select count(id) as jml from tr_soal where tmujian_id='".$rs->id."'")->row();
											             	  ?>
															    <tr>
																  <td><?php echo $no++; ?></td>
																   <td> <?php echo $kelas->nama; ?> --  <?php echo $this->Reff->get_kondisi(array("id"=>$kelas->kelas),"tm_kelas","nama"); ?> --  <?php echo $this->Reff->get_kondisi(array("id"=>$kelas->rombel),"tr_ruangkelas","nama"); ?>  -- Semester <?php echo semester($rs->semester); ?></td>
																  
																  <td> <?php echo $rs->nama; ?> </td>
																 
																
																  <td> <?php echo $rs->waktu; ?> Menit </td>
																  <td> <?php echo $jml_soal->jml; ?>  Soal <?php echo ucwords($rs->jenis); ?> </td>
																
																  <td><a class="btn btn-primary btn-sm  " href="<?php echo site_url("teacherujian/loadsoalujian/".base64_encode($tmujian_id)."/".base64_encode("dsd")."/".base64_encode($rs->trkelas_id)."/".base64_encode($rs->id).""); ?>""    style="color:white">Load Semua Soal</a> </td>
																
																
																
																</tr>
																<?php 
															   }
															   ?>
															  
															</tbody>
														   
														   
														   
														   </table>
		</div>
		
		
<script type="text/javascript">
$(function() {
    $('#datatablesisbanksoal').DataTable({
	   responsive: false,
	   	"language": {
							  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Belum ada siswa",
						   "sLengthMenu": "&nbsp;&nbsp; Menampilkan   _MENU_ Data"
				    }
							
							
						
	
	
	});
});

</script>