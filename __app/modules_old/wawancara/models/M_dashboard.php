<?php

class M_dashboard extends CI_Model {
 
    public function __construct() {
        parent::__construct();
    }

    
	
    public function grid($paging){
       
	    $key           = $this->input->get_post("keyword");
	    $tmmadrasah_id = $this->input->get_post("tmmadrasah_id");
	    $kategori         = $this->input->get_post("kategori");
		$sesi          = $this->input->get_post("sesi");
		$provinsi          = $this->input->get_post("provinsi");
		$kota          = $this->input->get_post("kota");
		$literasi      = $this->input->get_post("literasi");
		$keputusan          = $this->input->get_post("keputusan");
		
		$this->db->select("*");
        $this->db->from('tm_siswa');
		$this->db->where("pewawancara",$_SESSION['users_id']); 
		$this->db->where("nilai IS NOT NULL"); 
		


	
	    if(!empty($key)){  $this->db->where("(UPPER(nama) LIKE '%".strtoupper($key)."%' or (no_test) LIKE '%".strtoupper($key)."%')");    }
	  
	    if(!empty($kategori)){  $this->db->where("kategori",$kategori);    }
	    if(!empty($provinsi)){  $this->db->where("provinsi",$provinsi);    }
	    if(!empty($kota)){  $this->db->where("kota",$kota);    }
	    if(!empty($literasi)){  $this->db->where("literasi",$literasi);    }
	    if(!empty($keputusan)){  $this->db->where("keputusan",$keputusan);    }
	   
	
		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 $this->db->order_by("jenjang","ASC");
					 $this->db->order_by("literasi","ASC");
					 $this->db->order_by("nilai","DESC");
					 $this->db->order_by("pedadogis","DESC");
					 $this->db->order_by("profesional","DESC");
					 $this->db->order_by("no_test","ASC");
					
					
		
			 }
      
		
		
		return $this->db->get();
		
    }
    

	

    public function getUjianById($id)
    {
        $this->db->select('*');
        $this->db->from('tm_ujian_wawancara a');
      
        $this->db->where('a.id', $id);
        return $this->db->get()->row();
    }
	
	 public function getSoal($id)
    {
        $ujian = $this->getUjianById($id);
        //$order = $ujian->jenis==="acak" ? 'rand()' : 'urutan';

        $this->db->select('id as id_soal, soal,opsi_a, opsi_b, opsi_c, opsi_d, opsi_e, jawaban,jenis');
        $this->db->from('tm_wawancara');
  
        $this->db->where('tmujian_id', $id);
        $this->db->order_by("urutan","asc");
        
        return $this->db->get()->result();
    }
	
	 public function HslUjian($id, $tmsiswa_id)
    {
        $this->db->select('*, UNIX_TIMESTAMP(tgl_selesai) as waktu_habis');
        $this->db->from('h_ujian_w');
        $this->db->where('tmujian_id', $id);
        $this->db->where('tmsiswa_id', $tmsiswa_id);
        return $this->db->get();
    }
	
	
	  public function ambilSoal($pc_urut_soal1, $pc_urut_soal_arr)
    {
        $this->db->select("*,id as id_soal, {$pc_urut_soal1} AS jawaban");
        $this->db->from('tm_wawancara');
        $this->db->where('id', $pc_urut_soal_arr);
        return $this->db->get()->row();
    }
	
	  public function getJawaban($id_tes)
    {
        $this->db->select('list_jawaban');
        $this->db->from('h_ujian_w');
        $this->db->where('id', $id_tes);
        return $this->db->get()->row()->list_jawaban;
    }
	
	 public function getSoalById($id)
    {
        return $this->db->get_where('tm_wawancara', ['id' => $id])->row();
    }
	
	
	
}
