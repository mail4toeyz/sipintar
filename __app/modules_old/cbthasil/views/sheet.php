


<style>
/* Add some margin to the page and set a default font and colour */

body {
 
  line-height: 1.8em;

}

/* Give headings their own font */

h1, h2, h3, h4 {
  font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
}

/* Main content area */

.contentdragda {
 
  text-align: center;
  -moz-user-select: none;
  -webkit-user-select: none;
  user-select: none;
}

/* Header/footer boxes */



.wideBox h1 {
  font-weight: bold;
  margin: 20px;
  color: #666;
  font-size: 1.5em;
}

/* Slots for final card positions */


/* The initial pile of unsorted cards */

.cardPile {
  margin: 0 auto;
  background: #ffd;
}

.cardSlots, .cardPile {
  width: 100%;
  height: 100%;
  padding: 20px;
  border: none;
  
}

/* Individual cards and slots */

.cardSlots div, .cardPile div {
  float: left;
  width:100%;
    height: 100%;
  padding: 10px;
  padding-top: 40px;
  padding-bottom: 0;
  border: none;
  -moz-border-radius: 10px;
  -webkit-border-radius: 10px;
  border-radius: 10px;
  margin: 0 0 0 10px;
  background: #fff;
}

.cardSlots div:first-child, .cardPile div:first-child {
  margin-left: 0;
}

.cardSlots div.hovered {
  background: #aaa;
}

.cardSlots div {
  border-style: dashed;
}

.cardPile div {
  background: #6ea8d1;
  color:white;
 
  font-size: 15px;
  border:1px;
}

.cardPile div.ui-draggable-dragging {
  -moz-box-shadow: 0 0 .5em rgba(0, 0, 0, .8);
  -webkit-box-shadow: 0 0 .5em rgba(0, 0, 0, .8);
  box-shadow: 0 0 .5em rgba(0, 0, 0, .8);
}

/* Individually coloured cards */

#card1.correct { background: red; }
#card2.correct { background: brown; }
#card3.correct { background: orange; }
#card4.correct { background: yellow; }
#card5.correct { background: green; }
#card6.correct { background: cyan; }
#card7.correct { background: blue; }
#card8.correct { background: indigo; }
#card9.correct { background: purple; }
#card10.correct { background: violet; }




</style>

   
    <div class="col-sm-9">
        <?php echo form_open('', array('id'=>'ujian'), array('id'=> $id_tes)); ?>
		<?Php  
		 $jmsoal = $this->db->query("select count(id) as jmsoal from tr_soal where tmujian_id='".$ujian->id."'")->row(); 
		
		?>
         <div class="box box-primary navbarcbt">
            <div class="box-header with-border" style="background-color:#2bb142;color:white;font-weight:bold">
                <h3 class="box-title"><i class="fa fa-book"></i>
				<span class="hidden-xs"> AKSI 2020 - LITERASI <?php echo strtoupper($ujian->nama); ?>  PAKET  <?php echo strtoupper($ujian->paket); ?></span>
				
				
				</h3>
				
                <div class="box-tools pull-right">
                    
                    <div class="btn-actions-pane-right actions-icon-btn">
			           
                                                <div role="group" class="btn-group-sm btn-group">
                                                    <div class="btn-group dropdown">
                                                        <button type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" class="btn-pill pl-3 dropdown-toggle btn btn-danger btn-sm">Ukuran Font <i class="fa fa-cog" aria-hidden="true"></i>
</button>
                                                        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu-rounded dropdown-menu-right dropdown-menu"><h6 tabindex="-1" class="dropdown-header">Setting Ukuran Font</h6>
                                                            <button type="button" tabindex="0" class="dropdown-item fontgede" data="12px" style="color:black">12 Pixel</button>
                                                            <button type="button" tabindex="0" class="dropdown-item fontgede" data="14px" style="color:black">14 Pixel</button>
															 <button type="button" tabindex="0" class="dropdown-item fontgede" data="16px" style="color:black">16 Pixel</button>
															 <button type="button" tabindex="0" class="dropdown-item fontgede" data="18px" style="color:black">18 Pixel</button>
															 
															  <button type="button" tabindex="0" class="dropdown-item fontgede" data="20px" style="color:black">20 Pixel</button>
															   <button type="button" tabindex="0" class="dropdown-item fontgede" data="22px" style="color:black">22 Pixel</button>
															   
															   <button type="button" tabindex="0" class="dropdown-item fontgede" data="24px" style="color:black">24 Pixel</button>
															    <button type="button" tabindex="0" class="dropdown-item fontgede" data="30px" style="color:black">30 Pixel</button>
															  
                                                           
                                                            
                                                        </div>
                                                    </div>
                                                   
                                                </div>
                                            </div>
                </div>
				
				
				
            </div>
             <div class="box-body" style="min-height:450px">
			 <span class="badge bg-red">Soal No. <span id="soalke"></span>  Dari <span id="darisoal"><?php echo $jmsoal->jmsoal; ?> Soal </span> </span>
                <?php echo $html; ?>
            </div>
            <div class="box-footer text-center">
                <a class="action back btn btn-info" rel="0"  onclick="return back();"><i class="glyphicon glyphicon-chevron-left"></i> Sebelumnya</a>
				
                <a class="ragu_ragu btn btn-warning" rel="1" onclick="return tidak_jawab();" style="display:none">Ragu-ragu</a>
				
                <a class="action next btn btn-info" rel="2"  onclick="return next();"><i class="glyphicon glyphicon-chevron-right"></i> Selanjutnya </a>
                
                <input type="hidden" name="jml_soal" id="jml_soal" value="<?=$no; ?>">
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>
	
	 <div class="col-sm-3">

     

     <div class="box box-primary navbarcbt">
            <div class="box-header with-border" style="background-color:#2bb142;color:white;font-weight:bold">
                <h3 class="box-title"><i class="fa fa-calendar" aria-hidden="true"></i>
					Analisis Hasil Kompetensi  </h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body ">
            <?php 

              $h_ujian = $this->db->query("select * from h_ujian where tmsiswa_id='".$data->id."' and literasi='".$literasi."'")->row();

                if($h_ujian->status=="Y"){

                    $status = '<span class="badge badge-red"> Sedang mengerjakan </span>';
                }else{

                    $status = '<span class="badge badge-green"> Selesai  mengerjakan </span>';
                }

            ?>

            Nama Siswa  : <?php echo $data->nama; ?> <br>
            Nama Madrasah  : <?php echo $data->madrasah; ?>  <br>
            Literasi : Literasi <?php echo $literasi; ?> (<?php echo $ujian->nama; ?>) <br>
            Status :  <?php echo $status; ?> <br>
            Mulai Mengerjakan  :  <?php echo $this->Reff->formattimestamp($h_ujian->tgl_mulai); ?>  <br>
            Selesai Mengerjakan  :  <?php echo $this->Reff->formattimestamp($h_ujian->tgl_akhir); ?>  <br>
           <!-- Skor    :  <?php echo ($h_ujian->nilai); ?>  <br> -->

           <a href="<?php echo site_url("cbthasil/hasilcebt?tmsiswa_id=".$data->id.""); ?>"> H</a>
         
           



            </div>
        </div>
      
        <div class="box box-primary navbarcbt">
            <div class="box-header with-border" style="background-color:#2bb142;color:white;font-weight:bold">
                <h3 class="box-title" ><i class="fa fa-folder-open" aria-hidden="true"></i>

             NAVIGASI SOAL</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body text-center" id="tampil_jawaban">
            </div>
            <span class="col-md-2 col-sm-2 col-xs-2" style="border:1px solid black;height:20px;width:20px;margin-left:40px;background-color:#e1dedd"></span> &nbsp; <b style="font-size:11px">Belum dijawab<b> <br>
					 
                        <span class="col-md-2 col-sm-2 col-xs-2" style="border:1px solid black;height:20px;width:20px;margin-left:40px;background-color:#1dc672"></span> &nbsp; <b style="font-size:11px">Sudah Dijawab <b> <br>
               
			
             <br>
             <center>
             <a class="action back btn btn-info" rel="0" onclick="return back();"><i class="glyphicon glyphicon-chevron-left"></i> Sebelumnya</a>
              
              <a class="action next btn btn-info" rel="2" onclick="return next();"><i class="glyphicon glyphicon-chevron-right"></i> Selanjutnya </a>
              </center>
              <hr>
               <center><a href="<?Php echo site_url('pelaksanaan/analisispeserta'); ?>" class="btn btn-danger btn-sm " ><i class="glyphicon glyphicon-stop"></i>
					Kembali ke Analisis 
				 </a> </center>
                 <br>
        </div>

    
       

      
        
		
		
		
		
		
		
    </div>
</div>

<?php 





?>

<script type="text/javascript">
    var base_url        = "<?php echo base_url(); ?>";
    var id_tes          = "<?php echo $id_tes; ?>";
    var widget          = $(".step");
    var total_widget    = widget.length;
	
	
	
	
	
$(document).ready(function() {
    $('.fontgede').click(function() {
		var data = $(this).attr("data");
		
        $('.papers').css("font-size", function() {
            return $(this).css('font-size',data);
        });
    });
});


var elem = document.getElementById("cbtdendi");
function openFullscreen() {
  if (elem.requestFullscreen) {
    elem.requestFullscreen();
  } else if (elem.mozRequestFullScreen) { /* Firefox */
    elem.mozRequestFullScreen();
  } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari & Opera */
    elem.webkitRequestFullscreen();
  } else if (elem.msRequestFullscreen) { /* IE/Edge */
    elem.msRequestFullscreen();
  }
}



</script>

<script type="text/javascript">

$(document).ready(function () {
    var t = $('.sisawaktu');
    if (t.length) {
        sisawaktu(t.data('time'));
    }

    buka(1);
    simpan_sementara();

    widget = $(".step");
    btnnext = $(".next");
    btnback = $(".back");
    btnsubmit = $(".submit");

    $(".step, .back, .selesai").hide();
    $("#widget_1").show();
});

function getFormData($form) {
    var unindexed_array = $form.serializeArray();
    var indexed_array = {};
    $.map(unindexed_array, function (n, i) {
        indexed_array[n['name']] = n['value'];
    });
    return indexed_array;
}

function buka(id_widget) {
    $(".next").attr('rel', (id_widget + 1));
    $(".back").attr('rel', (id_widget - 1));
    $(".ragu_ragu").attr('rel', (id_widget));
    cek_status_ragu(id_widget);
    cek_terakhir(id_widget);

    $("#soalke").html(id_widget);

    $(".step").hide();
    $("#widget_" + id_widget).show();

    simpan();
}

function next() {
    var berikutnya = $(".next").attr('rel');
    berikutnya = parseInt(berikutnya);
    berikutnya = berikutnya > total_widget ? total_widget : berikutnya;

    $("#soalke").html(berikutnya);

    $(".next").attr('rel', (berikutnya + 1));
    $(".back").attr('rel', (berikutnya - 1));
    $(".ragu_ragu").attr('rel', (berikutnya));
    cek_status_ragu(berikutnya);
    cek_terakhir(berikutnya);

    var sudah_akhir = berikutnya == total_widget ? 1 : 0;

    $(".step").hide();
    $("#widget_" + berikutnya).show();

    if (sudah_akhir == 1) {
        $(".back").show();
        $(".next").hide();
    } else if (sudah_akhir == 0) {
        $(".next").show();
        $(".back").show();
    }

    simpan();
}

function back() {
    var back = $(".back").attr('rel');
    back = parseInt(back);
    back = back < 1 ? 1 : back;

    $("#soalke").html(back);

    $(".back").attr('rel', (back - 1));
    $(".next").attr('rel', (back + 1));
    $(".ragu_ragu").attr('rel', (back));
    cek_status_ragu(back);
    cek_terakhir(back);

    $(".step").hide();
    $("#widget_" + back).show();

    var sudah_awal = back == 1 ? 1 : 0;

    $(".step").hide();
    $("#widget_" + back).show();

    if (sudah_awal == 1) {
        $(".back").hide();
        $(".next").show();
    } else if (sudah_awal == 0) {
        $(".next").show();
        $(".back").show();
    }

    simpan();
}

function tidak_jawab() {
    var id_step = $(".ragu_ragu").attr('rel');
    var status_ragu = $("#rg_" + id_step).val();

    if (status_ragu == "N") {
        $("#rg_" + id_step).val('Y');
        $("#btn_soal_" + id_step).removeClass('btn-success');
        $("#btn_soal_" + id_step).addClass('btn-warning');

    } else {
        $("#rg_" + id_step).val('N');
        $("#btn_soal_" + id_step).removeClass('btn-warning');
        $("#btn_soal_" + id_step).addClass('btn-success');
    }

    cek_status_ragu(id_step);

    simpan();
}

function cek_status_ragu(id_soal) {
    var status_ragu = $("#rg_" + id_soal).val();

    if (status_ragu == "N") {
        $(".ragu_ragu").html('Ragu');
    } else {
        $(".ragu_ragu").html('Tidak Ragu');
    }
}

function cek_terakhir(id_soal) {
    var jml_soal = $("#jml_soal").val();
    jml_soal = (parseInt(jml_soal) - 1);

    if (jml_soal === id_soal) {
        $('.next').hide();
        $(".selesai, .back").show();
    } else {
        $('.next').show();
        $(".selesai, .back").hide();
    }
}

function simpan_sementara() {
    var f_asal = $("#ujian");
    var form = getFormData(f_asal);
    //form = JSON.stringify(form);
    var jml_soal = form.jml_soal;
    jml_soal = parseInt(jml_soal);

    var hasil_jawaban = "";
    var nomor=0;
    for (var i = 1; i < jml_soal; i++) {
		nomor++;
		  if(nomor <10){
			  nomor = "0"+nomor;
		  }
        var idx = 'jawaban_' + i;
        var jwbn = 'jwbn' + i;
        var idx2 = 'rg_' + i;
        
        var jawab = form[jwbn];
       
          
       
        var ragu = form[idx2];
          if(i % 5==1){
			  
			  hasil_jawaban +='<br>';
		  }
        if (jawab != undefined) {
            
             
           
                if (jawab =="") {
                    hasil_jawaban += '<a id="btn_soal_' + (i) + '"  class="btn btn-default btn_soal btn-sm" onclick="return buka(' + (i) + ');">' + (nomor) + "</a>";
                
                } else {
                    hasil_jawaban += '<a id="btn_soal_' + (i) + '" class="btn btn-success btn_soal btn-sm" onclick="return buka(' + (i) + ');">' + (nomor) + "</a>";
                }

                
        
        } else {
			
			
            hasil_jawaban += '<a id="btn_soal_' + (i) + '" class="btn btn-default btn_soal btn-sm" onclick="return buka(' + (i) + ');">' + (nomor) + ". -</a>";
        }
    }
    $("#tampil_jawaban").html('<div id="yes"></div>' + hasil_jawaban);
}

 $(document).off("click",".ayolah").on("click",".ayolah",function(){
    
     var nomor = $(this).attr("nomor");

       $("#btn_soal_"+nomor).addClass('btn-success').removeClass('btn-default');

 })


function simpan() {
    
  
}

function selesai() {
  
}

function waktuHabis() {
	 
				  
   
    
}

function simpan_akhir() {
  

	
    
}

	function sisawaktu(t) {
		var time = new Date(t);
		var n = new Date();
		var x = setInterval(function() {
			var now = new Date().getTime();
			var dis = time.getTime() - now;
			var h = Math.floor((dis % (1000 * 60 * 60 * 60)) / (1000 * 60 * 60));
			var m = Math.floor((dis % (1000 * 60 * 60)) / (1000 * 60));
			var s = Math.floor((dis % (1000 * 60)) / (1000));
			h = ("0" + h).slice(-2);
			m = ("0" + m).slice(-2);
			s = ("0" + s).slice(-2);
			var cd = h + ":" + m + ":" + s;
			$('.sisawaktu').html(cd);
		}, 100);
		setTimeout(function() {
			
			
				
				waktuHabis();
			
			
		}, (time.getTime() - n.getTime()));
	}

</script>

