<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Analisis extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		 if(!$this->session->userdata("status")){
			    
				echo $this->Reff->sessionhabis();
				exit();
		  }
		  $this->load->model('M_siswa','m');
		
	  }
	  
   function _template($data)
	{
		if($_SESSION['status'] =="madrasah"){
			$this->load->view('school/page_header',$data);	
		}else{
	      $this->load->view('admin/page_header',$data);	
		}
	}
		
	public function index()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		
		 $data['title']   = "Analisis Ujian  ";
	     if(!empty($ajax)){
					    
			 $this->load->view('page',$data);
		
		 }else{
			 
		     $data['konten'] = "page";
			 
			 $this->_template($data);
		 }
	

	}
	
	 public function grid(){
		
		  $iTotalRecords = $this->m->grid(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->grid(true)->result_array();
		   
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
			 
			   
				$no = $i++;
				$records["data"][] = array(
					$no,
					
					$val['madrasah'],
					$val['nisn']."",
					$val['no_aksi']."",
					$val['nama'],
					$val['jml_benar'],
					$val['nilai'],
              	  
					
				    ' 
				
					<button type="button" class="btn btn-success btn-sm ubahmodal" datanya="'.$val['id'].'" urlnya="'.site_url("datasiswa/form").'" target="#loadform" data-toggle="modal" data-target="#defaultModal">
                                    <i class="fa fa-retweet"></i>
                      </button>
					
					  '
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
	public function form(){
		
		$id = $this->input->get_post("id");
		$data = array();
		   if(!empty($id)){
			   
			  $data['data']  = $this->Reff->get_where("tm_siswa",array("id"=>$id));
			   
		   }
		$this->load->view("form",$data);
		
	}
	
	
	public function save(){
     
        $this->form_validation->set_message('required', '{field} Wajib diisi. ');
         $this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , silahkan masukan NISN lain.');
			    $this->form_validation->set_message('min_length', '%s: Minimal  %s digit');
         $this->form_validation->set_message('max_length', '%s: Maksimal  %s digit');
    
		 
			
				$config = array(
				    
				   
				    array('field' => 'f[nama]', 'label' => 'Nama Guru  ', 'rules' => 'trim|required'),
				    array('field' => 'f[kelas]', 'label' => 'Kelas  ', 'rules' => 'trim|required'),
					 array('field' => 'password', 'label' => 'Password ', 'rules' => 'trim|required|min_length[3]'),
					
					  array('field' => 'f[tempat]', 'label' => 'Tempat Lahir  ', 'rules' => 'trim|required'),
				   
			
				   
				  
				   
				   
	
					
				);
				
				$this->form_validation->set_rules($config);	
        
        if ($this->form_validation->run() == true) {
			  
			    $id = $this->input->get_post("id",true);
			    $f  = xssArray($this->input->get_post("f",true));
				
				
				 
				
							 if(empty($id)){
								
								 
								   $this->m->insert();
								   echo "Data Berhasil disimpan";	
								 
								
								
							 }else{
								$this->m->update($id); 
								echo "Data Berhasil disimpan";	
								 
							 }
							 
						
							
						
		
		
							
			     
			    
												 
	     } else {
	         
           
                header('Content-Type: application/json');
                echo json_encode(array('error' => true, 'message' => validation_errors()));
            
        }

		
	
	
	}
	

	
	public function form_excel(){
		
		$id = $this->input->get_post("id");
		$data = array();
		   if(!empty($id)){
			   
			  $data['data']  = $this->Reff->get_where("tm_siswa",array("id"=>$id));
			   
		   }
		$this->load->view("form_excel",$data);
		
	}
	
	public function import_excel()
		{
	  
		 	$this->load->library('PHPExcel/IOFactory');
       // error_reporting(0);
	

	    $sheetup   = 0;
	    $rowup     = 3;
		
		$tmmadrasah_id = $this->input->get_post("tmmadrasah_id");
		$kelas         = $this->input->get_post("kelas");
	
		$fileName = time().".xlsx";
         
        $config['upload_path'] = './__statics/excel/tmp/'; //buat folder dengan nama assets di root folder
        $config['file_name'] = $fileName;
        $config['allowed_types'] = '*';
        $config['max_size'] = 100000;
         
        $this->load->library('upload');
        $this->upload->initialize($config);
         
        if(! $this->upload->do_upload('file') ){
			$jupload=0;
              echo $this->upload->display_errors();

			  
		}else{
             
        $media = $this->upload->data('file');
		
        $inputFileName = './__statics/excel/tmp/'.$fileName;
         
        try {
                $inputFileType = IOFactory::identify($inputFileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }
 
            $sheet = $objPHPExcel->getSheet($sheetup);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
        
             
			 $jupload=0;
            for ($row = $rowup; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE);
				
       
			 	
			 	$nisn        =      (trim($rowData[0][1]));
			 	$nama         =      (trim($rowData[0][2]));			 
			    $tempat       =      (trim($rowData[0][3])); 
			     $tgl_lahir       =      (trim($rowData[0][4]));  
			 
			 	
			 
				$error  ="";
				
				 if(!empty($nisn)){
					
					$nisn = strtoupper($this->Reff->randomangka(6));
				 }
					 
				
					
				
					$jupload++;
                 $data = array(
                    "nisn"=> $nisn ,                 
                    
					"nama"=> $nama,                   
					"kelas"=> $kelas,                                 
					"tempat"=> $tempat,                   
				                 
                    "tgl_lahir"=> $tgl_lahir                 
                           
				  );
				  
				 
				  
				  $passwordgenerate = strtoupper($this->Reff->get_id(3));
				  $no_aksi          = strtoupper($this->Reff->randomangka(6));
                   
                   $this->db->set("tmmadrasah_id",$tmmadrasah_id);               
                   $this->db->set("no_aksi",$no_aksi);               
                   $this->db->set("password",enkrip($passwordgenerate));
                   $this->db->insert("tm_siswa",$data);
				
				   
				   
				
           
		   
			}
			
			
		
			echo "<br><br><center>Data yg di upload : ". $jupload ." Data <br> <a href='".site_url("datasiswa")."' > Kembali ke Aplikasi </a>";
        
        }
   }
	
	
	public function hapus(){
		
		$id = $this->input->get_post("id",true);
		
		$this->db->delete("tm_siswa",array("id"=>$id));
		echo "sukses";
	}
	
	
	 
}
