<div class="col-md-12">
		
				       <div class="card">
						<div class="card-header card-header-success">
						  <h4 class="card-title">Data Peserta CBT </h4>
						  <p class="card-category">Data dibawah ini adalah data peserta yang akan mengikuti Computer Based Test (CBT), Jika ada Peserta yang belum masuk kedalam data dibawah ini silahkan menghubungi Administrator  </p>
						</div>
					  
						 <div class="card-body">
						<!-- 
						        <button type="button" target="#loadform" url="<?php echo site_url("datasiswa/form"); ?>"  data-toggle="modal" data-target="#defaultModal" data-color="cyan" class="btn btn-success btn-sm addmodal"><span class="fa fa-plus"></span> TAMBAH DATA </button>
								 <button type="button" target="#loadform" url="<?php echo site_url("datasiswa/form_excel"); ?>"  data-toggle="modal" data-target="#defaultModal" data-color="cyan" class="btn btn-success btn-sm addmodal"><span class="fa fa-file-excel-o"></span> IMPORT EXCEL </button>
								 <button type="button"  class="btn btn-success btn-sm " id="generate"><span class="fa fa-codepen"></span> GENERATE NOMOR TES </button>
								
                    -->
						
					   <div class="row" >
						 
					        

					   		<?php 
						   if($_SESSION['status'] !='madrasah'){
							?>
							<div class="col-md-3">
								
							                <select class="form-control "    id="kategori" >
																 <option value=""> Filter Kategori </option>
																 <?php 
															  $kelas = $this->db->get("tm_kategori")->result();
																 foreach($kelas as $i=>$r){
																	 
																	?><option value="<?php echo $r->id; ?>" > <?php echo ($r->nama); ?></option><?php  
																	 
																 }
															?>
											</select>
							</div>
						<?php 
						   }
						?>
						


						        		
								<div class="col-md-5">
								
									<div class="input-group">
									<input type="text" class="form-control" id="keyword"  placeholder="Cari  Nama disini..">
									<div class="input-group-append">
										<button class="btn btn-success btn-sm" type="button">CARI</button>
									</div>
									</div>
								</div>
					   </div>
					   
					   <br>


							<div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover " id="datatableTable" width="99%">
                                    <thead class="bg-blue">
                                        <tr>
                                            <th width="2px">NO</th>
                                            
                                            <th>FOTO </th>
                                            <th>NIK</th>
											<th>NAMA </th>
                                            <th>NO TEST</th>
                                            <th>TGL LAHIR </th>                                          
                                            
                                            <th>KATEGORI </th>
                                                                                   
                                            <th>SESI  </th>
                                            <th>STATUS LOGIN  </th>
                                           
										  							
                                          	<th width="5%">AKSI </th>
                                        </tr>
                                    </thead>
                                   
                                    <tbody>
                                        
                                        
                                    </tbody>
                                </table>
                         </div>
                         </div>
                         </div>
                 
 </div> 
      <div id="defaultModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
	     FORM SISWA
        <button type="button" class="close" data-dismiss="modal">&times;</button>
       
      </div>
      <div class="modal-body" id="loadform">
        <p>Loading...</p>
      </div>
      
    </div>

  </div>
</div>


<div id="pelaksanaanmodal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        
      </div>
      <div class="modal-body" id="loadbody">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
	
<script type="text/javascript">


$(document).off("click",".camera").on("click",".camera",function(){
	            
				var tmujian_id = $(this).attr("tmujian_id");
			 
				  $.post("<?php echo site_url('datasiswa/camera'); ?>",{tmujian_id:tmujian_id},function(data){
	   
				   $("#loadbody").html(data);
  
				  })
				   
				 
			   });
			   
			   $(document).off("click",".camerates").on("click",".camerates",function(){
				  
				var tmsiswa_id = $(this).attr("tmsiswa_id");
			 
				  $.post("<?php echo site_url('datasiswa/camerates'); ?>",{tmsiswa_id:tmsiswa_id},function(data){
	   
				   $("#loadbody").html(data);
  
				  })
				   
				 
			   });

			   
$(document).off("click",".resetlogin").on("click",".resetlogin",function(){
	            
				var tmsiswa_id = $(this).attr("tmsiswa_id");
			 
				  $.post("<?php echo site_url('datasiswa/resetlogin'); ?>",{tmsiswa_id:tmsiswa_id},function(){
	   
					alertify.success("Login berhasil direset");
					location.reload();
				  })
				   
				 
			   });
  var dataTable = $('#datatableTable').DataTable( {
						"processing": true,
						"language": {
						"processing": '<div class="preloader pl-size-l"><div class="spinner-layer pl-red-grey"><div class="circle-clipper left"> <div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data kosong",
						   "sLengthMenu": "&nbsp;&nbsp; Menampilkan   _MENU_ Data"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": false,
					"lengthMenu": [[10,25, 50,100,200,300,500,1000, 800000000], [10,25, 50,100,200,300,500,1000,"All"]],
					 "dom": 'Blfrtip',
					 "sPaginationType": "full_numbers",
					"buttons": [
					
                         	
						    {
							extend: 'excelHtml5',
							
							text:'Cetak Excel',
						
							},
							
							
							
							
						
							{
							extend: 'colvis',
							
							text:' Pengaturan Kolom ',
							
						}
					],
					
					"ajax":{
						url :"<?php echo site_url("datasiswa/grid"); ?>", 
						type: "post", 
						"data": function ( data ) {
						
							data.keyword = $("#keyword").val();
						
							data.kategori = $("#kategori").val();
							data.pewawancara = $("#pewawancara").val();
					
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				
				
				
	
				$(document).on("input","#keyword",function(){
	  
					dataTable.ajax.reload(null,false);	
					
				});

				$(document).on("change","#tmmadrasah_id,#kategori,#pewawancara",function(){
						
						dataTable.ajax.reload(null,false);	
						
					});

					$(document).off("click","#generate").on("click","#generate",function(){
	  
	                
					  alertify.confirm("Generate Nomor Tes akan dilakukan, apakah Anda yakin ?  ",function(){


						  $.post("<?php echo site_url("datasiswa/generate_no"); ?>",function(data){

							dataTable.ajax.reload(null,false);	
							alertify.alert(data);



						  })
					  });
  })

</script>
				