<div class="col-md-12">
		
				       <div class="card">
						<div class="card-header card-header-success">
						  <h4 class="card-title">Data Madrasah</h4>
						  <p class="card-category">Data Madrasah Peserta Ujian Madrasah </p>
						</div>
					  
						 <div class="card-body">
						      <select class="form-control" id="jenjang">
									<option value="">- Cari Jenjang - </option>
								<?php 
								$jenjang = $this->db->get("tm_kategori")->result();
									foreach($jenjang as $rp){
										?><option value="<?php echo $rp->id; ?>" ><?php echo $rp->nama; ?></option> <?php 


									}
									?>


								</select>
						 <button type="button" target="#loadform" url="<?php echo site_url("datamadrasah/form_excel"); ?>"  data-toggle="modal" data-target="#defaultModal" data-color="cyan" class="btn btn-success btn-sm addmodal"><span class="fa fa-file-excel-o"></span> IMPORT EXCEL </button>
                            <br><br>
							<div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover " id="datatableTable" width="99%">
                                    <thead class="bg-blue">
									 <tr>
                                            <th width="2px" rowspan="2">NO</th>
                                                                                       
                                            <th rowspan="2">JENJANG </th>
                                            <th rowspan="2">NAMA MADRASAH</th>
											<th rowspan="2">NSM  </th> 
                                            <th rowspan="2">PASSWORD </th> 
                                            <th rowspan="2">ALAMAT </th> 
                                            <th colspan="4"> <center> SISWA </center> </th>
                                            <th colspan="2"> <center> DEVICE </center> </th>                                           
                                                                                    
                                            
                                        </tr>
										

										<tr>

                                            <td>Jumlah</td>                                            
                                            <td>Mengerjakan</td>
                                            <td>Belum </td>
                                            <td>Prosentase</td>

											<td><center><i class="fa fa-android" style="color:green"></i></center></td>
											<td><center><i class="fa fa-laptop" style="color:blue"></i></center></td>
										
                                           

											

										</tr>


                                       
                                    </thead>
                                   
                                    <tbody>
                                        
                                        
                                    </tbody>
                                </table>
                            </div>
                         </div>
                         </div>
                 
 </div>
              
        
	<div id="defaultModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
       
      </div>
      <div class="modal-body" id="loadform">
        <p>Loading...</p>
      </div>
      
    </div>

  </div>
</div>
	
<script type="text/javascript">
  var dataTable = $('#datatableTable').DataTable( {
						"processing": true,
						"language": {
						"processing": '<div class="preloader pl-size-l"><div class="spinner-layer pl-red-grey"><div class="circle-clipper left"> <div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data kosong",
						   "sLengthMenu": "&nbsp;&nbsp; Menampilkan   _MENU_ Data"
				    },
					
					"serverSide": true,
					"searching": true,
					"responsive": false,
					"lengthMenu": [[10,25, 50,100,200,300,500,1000, 800000000], [10,25, 50,100,200,300,500,1000,"All"]],
					 
					 "sPaginationType": "full_numbers",
					 "dom": 'Blfrtip',
					"buttons": [
					
                         	
						    {
							extend: 'excelHtml5',
							exportOptions: {
							  columns: [ 0,2,3,4,5,6,7,8,9,10]
							},
							text:'<font color:"black">Cetak Excel</font>',
							
							},
							
							
							
							
						
							{
							extend: 'colvis',
							
							text:' Pengaturan Kolom ',
							
						}
					],
					
					"ajax":{
						url :"<?php echo site_url("datamadrasah/grid"); ?>", 
						type: "post", 
						"data": function ( data ) {
						
						data.jenjang = $("#jenjang").val();
					
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );

				$(document).on("change","#jenjang",function(){

					dataTable.ajax.reload(null,false);	
				})
				
				
				
	


</script>
				