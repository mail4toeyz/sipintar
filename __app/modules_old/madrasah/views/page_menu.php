
</style>
 <?php 
   $uri = $this->uri->segment(1);
   $uri2 = $this->uri->segment(2);
   $uri4 = ($this->uri->segment(4));
  
   
     
  ?>

 <div class="sidebar-wrapper" style="color:black">
        <ul class="nav">
          <li class="nav-item <?php echo ($uri=="madrasah") ? "active" :""; ?> ">
            <a class="nav-link menuajax" style="color:black" href="<?php echo site_url("madrasah"); ?>">
              <i class="fa fa-dashboard"></i>
              <p>Dashboard  </p>
            </a>
          </li>

		  
   
         <li class="nav-item <?php echo ($uri=="schoolmadrasah" && $uri2=="") ? "active":""; ?> ">
            <a class="nav-link menuajax" style="color:black" href="<?php echo site_url("schoolmadrasah"); ?>">
            <i class="fa fa-bank" aria-hidden="true"></i>
              <p>Profil  Madrasah </p>
            </a>
          </li>      


          <li class="nav-item <?php echo ($uri=="datasiswa" && $uri2=="") ? "active":""; ?> ">
            <a class="nav-link menuajax" style="color:black" href="<?php echo site_url("datasiswa"); ?>">
            <i class="fa fa-users" aria-hidden="true"></i>
              <p>Data Peserta Tes </p>
            </a>
          </li>                   
		  	   


        
		   
          <li class="nav-item <?php echo ($uri=="datasiswa" && $uri2=="sesi") ? "active":""; ?> ">
            <a class="nav-link menuajax" style="color:black" href="<?php echo site_url("datasiswa/sesi?id=0"); ?>" title="Pengawasan ">
            <i class="fa fa-retweet" aria-hidden="true"></i>


              <p>Pengawasan Simulasi    </p>
            </a>
          </li>

          <li class="nav-item <?php echo ($uri=="datasiswa" && $uri2=="hasil") ? "active":""; ?> ">
                        <a href="javascript:void(0);" style="color:black" class="menu-toggle nav-link">
                            <i class="fa fa-retweet"></i>
                            <span> Pengawasan Ujian     </span> 
                            
                        </a>
						
							<ul class="ml-menu" style="display:none">
               <?php 
                 $ujian = $this->db->get("tm_jadwal")->result();

                  foreach($ujian as $rj){
                  ?>
                  <li>
                 <a href="<?php echo site_url("datasiswa/sesi?id=".$rj->id.""); ?>" style="color:black" class="nav-link menuajax" title="Simulasi"><i class="fa fa-angle-right"></i> <?php echo $rj->matapelajaran; ?> </a>
                 </li>
                 <?php 

                  }
                ?>
							  
							
								
							  </ul>
                            
         </li>

          <li class="nav-item <?php echo ($uri=="datasiswa" && $uri2=="hasil") ? "active":""; ?> ">
                        <a href="javascript:void(0);" style="color:black" class="menu-toggle nav-link">
                            <i class="fa fa-info"></i>
                            <span> Hasil Ujian Peserta    </span> 
                            
                        </a>
						
							<ul class="ml-menu" style="display:none">
							   <li>
                 <a href="<?php echo site_url("datasiswa/hasil"); ?>" style="color:black" class="nav-link menuajax" title="Simulasi"><i class="fa fa-angle-right"></i> Nilai Ujian  </a>
                 </li>
							 
                 <li>
                      <a href="<?php echo site_url("pelaksanaan/hasilujian?kategori=0"); ?>" style="color:black"  class="nav-link menuajax " title="Hasil Jawaban "><i class="fa fa-angle-right"></i>  Hasil Simulasi   </a>
                 </li>
                
                 <li>
                      <a href="<?php echo site_url("pelaksanaan/statistik"); ?>" style="color:black"  class="nav-link menuajax " title="Statistik Ujian "><i class="fa fa-angle-right"></i>  Statistik Ujian  </a>
                 </li>
								
							  </ul>
                            
         </li>



         
		   
		    
		  
             <li class="nav-item <?php echo ($uri=="schoolmadrasah" && $uri2=="aktifitas") ? "active":""; ?> ">
              <a class="nav-link menuajax" style="color:black" href="<?php echo site_url("schoolmadrasah/aktifitas"); ?>">
              <i class="fa fa-clock-o"></i>
              <p>Monitoring Aktifitas </p>
            </a>
           </li> 


           
		  
          <li class="nav-item ">
            <a class="nav-link" style="color:black" href="javascript:void(0)" id="keluar">
              <i class="fa fa-sign-out"></i>
              <p>Logout </p>
            </a>
          </li>
         
          
         
        </ul>
      </div>
	  
