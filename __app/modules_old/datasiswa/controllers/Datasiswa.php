<?php
defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(0);
class Datasiswa extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		 if(!$this->session->userdata("status")){
			    
				echo $this->Reff->sessionhabis();
				exit();

				
		  }

		  if($this->session->userdata("status") =="Pewawancara"){
			    
			echo $this->Reff->sessionhabis();
			exit();
	    }


		  $this->load->model('M_siswa','m');
		  $this->load->helper('exportpdf_helper');  
		
	  }
	  
   function _template($data)
	{
		
		if($_SESSION['status'] =="madrasah"){
			$this->load->view('madrasah/page_header',$data);	
		}else{
	      $this->load->view('admin/page_header',$data);	
		}	
		
	}
		
	public function index()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		
		 $data['title']   = "Data Siswa ";
	     if(!empty($ajax)){
					    
			 $this->load->view('page',$data);
		
		 }else{
			 
		     $data['konten'] = "page";
			 
			 $this->_template($data);
		 }
	

	}
	
	 public function grid(){
		
		  $iTotalRecords = $this->m->grid(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->grid(true)->result_array();
		   
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
			

			$login ="<span class='badge bagde-blue'> Belum Login </span>";
			if($val['login']==1){
			 $login ="<a href='#' class='btn btn-danger btn-sm resetlogin' tmsiswa_id='".$val['id']."'> Reset  </a>";

			}else if($val['login']==2){

			 $login ="<a href='#' class='btn btn-success btn-sm resetlogin' tmsiswa_id='".$val['id']."'> Selesai </a>";
			}
			
		  

			
			
			$fotoawal ="";
			 if($val['foto_awal'] !=''){
				
			   $fotoawal ="<a href='#' class='btn btn-success btn-sm camerates' tmsiswa_id='".$val['id']."' data-toggle='modal' data-target='#pelaksanaanmodal'><span class='fa fa-camera'></span> </a>"; 
			}

			$foto = base_url()."__statics/img/not.png";
			 if($val['foto'] !=''){

				$foto = 'https://appmadrasah.kemenag.go.id/seleksiptk/file_upload/peserta/'.$val['nik'].'/'.$val['foto'].'';
			 }



				$no = $i++;
				$records["data"][] = array(
					$no,
					'<img class="img-circle " src="'.$foto.'" width="70px"> ',
					$val['nik'],
					$val['nama'],
					$val['no_test'],
					$val['tgl_lahir'],	
					
					$this->Reff->get_kondisi(array("id"=>$val['kategori']),"tm_kategori","nama"),
					
								
					$val['sesi'],
					$login.$fotoawal,
					
					
				    ' 
				
					<button type="button" class="btn btn-success btn-sm ubahmodal" datanya="'.$val['id'].'" urlnya="'.site_url("datasiswa/form").'" target="#loadform" data-toggle="modal" data-target="#defaultModal">
                                    <i class="fa fa-pencil"></i>
                      </button>
					  <button type="button"  class="btn btn-success btn-sm hapus" datanya="'.$val['id'].'" urlnya="'.site_url("datasiswa/hapus").'">
                                    <i class="fa fa-trash"></i>
					  </button>
					 
					  '
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	

	public function setkuota_prov(){

       $this->db->set($_POST['kolom'],$_POST['kuota']);
	   $this->db->where("id",$_POST['provinsi_id']);
	   $this->db->update("provinsi");
	   echo "sukses";
	}

	public function setkuota_kabko(){

		$this->db->set($_POST['kolom'],$_POST['kuota']);
		$this->db->where("id",$_POST['provinsi_id']);
		$this->db->update("kota");
		echo "sukses";
	 }
			public function kartutes()
			{
				
			

			$data['breadcumb']  ="Kartu Tes";
				
			$data['siswa']      = $this->db->get_where("tm_siswa",array("id"=>$_GET['id']))->row();
				
			$pdf_filename = 'Kartutes'.str_replace(array("'",",","-"),"",$data['siswa']->nama).'.pdf';	 
			$data_header = array('title' => 'Kartu Tes',);


			$user_info = $this->load->view('kartutes', $data, true);

			$output = $user_info;

			
			generate_pdf($output, $pdf_filename,TRUE);
			

			
			}


	public function hasil()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		
		 $data['title']   = "Data Siswa ";
	     if(!empty($ajax)){
					    
			 $this->load->view('page_hasil',$data);
		
		 }else{
			 
		     $data['konten'] = "page_hasil";
			 
			 $this->_template($data);
		 }
	

	}
	
	 public function grid_hasil(){
		
		  $iTotalRecords = $this->m->grid(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->grid(true)->result_array();
		   
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
			
				$simulasi = $this->db->query("select nilai from h_ujian where kategori=0 and tmsiswa_id='".$val['id']."'")->row();
				

				$no = $i++;
				$records["data"][] = array(
					$no,
					
					$val['no_test'],
					
					$val['nama'],
					$simulasi->nilai
					
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	


	public function resetujian(){
		
		$id = $this->input->get_post("id");

		$this->db->query("update tm_siswa set login='0' where id='".$id."'");
		$this->db->query("delete from h_ujian where tmsiswa_id='".$id."'");
		echo "sukses";
		
		
	}

	public function form(){
		
		$id = $this->input->get_post("id");
		$data = array();
		   if(!empty($id)){
			   
			  $data['data']  = $this->Reff->get_where("tm_siswa",array("id"=>$id));
			   
		   }
		$this->load->view("form",$data);
		
	}
	
	
	
	public function save(){
     
        $this->form_validation->set_message('required', '{field} Wajib diisi. ');
         $this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , silahkan masukan NISN lain.');
			    $this->form_validation->set_message('min_length', '%s: Minimal  %s digit');
         $this->form_validation->set_message('max_length', '%s: Maksimal  %s digit');
    
		 
			
				$config = array(
				    
				   
				    array('field' => 'f[nama]', 'label' => 'Nama Guru  ', 'rules' => 'trim|required'),
				  
				   
			
				   
				  
				   
				   
	
					
				);
				
				$this->form_validation->set_rules($config);	
        
        if ($this->form_validation->run() == true) {
			  
			    $id = $this->input->get_post("id",true);
			    $f  = xssArray($this->input->get_post("f",true));
				
								
				$config['upload_path']      = './__statics/upload/';
				$folder   					= '__statics/upload/';
		        $config['allowed_types']    = "jpg|jpeg|png"; 	
				$config['overwrite']        = true; 
                $config['max_size']         = '8200';				
				$filenamepecah			    = explode(".",$_FILES['file']['name']);
				$imagenew				    = "dp".time().".".pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
				$config['file_name'] = $imagenew;
				
				$id = $this->input->get_post("id",true);
				
				   $this->load->library('upload', $config);

						if ( ! $this->upload->do_upload('file'))
						{
							
							
								 
							$this->m->update($id); 
							echo "Data Berhasil disimpan";	
									
							
							
						
						}
						else{
				 
				
							 if(empty($id)){
								
								   $this->db->set("foto",$imagenew);
								   $this->m->insert();
								   echo "Data Berhasil disimpan";	
								 
								
								
							 }else{

								$this->db->set("foto",$imagenew);
								$this->m->update($id); 
								echo "Data Berhasil disimpan";	
								 
							 }


							 
							}
							
						
		
		
							
			     
			    
												 
	     } else {
	         
           
                header('Content-Type: application/json');
                echo json_encode(array('error' => true, 'message' => validation_errors()));
            
        }

		
	
	
	}
	

	
	public function form_excel(){
		
		$id = $this->input->get_post("id");
		$data = array();
		   if(!empty($id)){
			   
			  $data['data']  = $this->Reff->get_where("tm_siswa",array("id"=>$id));
			   
		   }
		$this->load->view("form_excel",$data);
		
	}
	
	public function import_excel()
		{
	  
		 	$this->load->library('PHPExcel/IOFactory');
       // error_reporting(0);
	

	    $sheetup   = 0;
	    $rowup     = 2;
		
		
	
		$fileName = time().".xlsx";
         
        $config['upload_path'] = './__statics/excel/tmp/'; //buat folder dengan nama assets di root folder
        $config['file_name'] = $fileName;
        $config['allowed_types'] = '*';
        $config['max_size'] = 100000;
         
        $this->load->library('upload');
        $this->upload->initialize($config);
         
        if(! $this->upload->do_upload('file') ){
			$jupload=0;
              echo $this->upload->display_errors();

			  
		}else{
             
        $media = $this->upload->data('file');
		
        $inputFileName = './__statics/excel/tmp/'.$fileName;
         
        try {
                $inputFileType = IOFactory::identify($inputFileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }
 
            $sheet = $objPHPExcel->getSheet($sheetup);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
        
             
			 $jupload=0;
            for ($row = $rowup; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE);
				
       
			 	
			 		 
			 	$nama	              =      (trim($rowData[0][1]));			 
			 	$nisn                 =      (trim($rowData[0][2]));			 
			 	$tempat               =      (trim($rowData[0][3]));			 
			 	$tgl_lahir            =      (trim($rowData[0][4]));			 
			 	$gender	              =      (trim($rowData[0][5]));			 
			 		 		 
				$jurusanex            =      (trim($rowData[0][6]));
			    $jurusan         	  =      $this->db->query("select id from tm_kategori where UPPER(nama)='".strtoupper($jurusanex)."'")->row();
				$error  ="";
				
			    
					 
				
					
				
					$jupload++;
						$data = array(							                 
							"nisn"=> $nisn,                   
							"nama"=> $nama,                   
							"gender"=> $gender,                   
							"tempat"=> $tempat,                                 
							"tgl_lahir"=> $tgl_lahir,  
							"sesi"=> 1,  
							                 
							"kategori"=> $jurusan->id,   
							"token"=> $this->Reff->randomangka(6)              
								
						);
				  
				 
				  
				 
                  
                   $this->db->insert("tm_siswa",$data);
				
				   
				   
				
           
		   
			}
			
			
		
			echo "<br><br><center>Data yg di upload : ". $jupload ." Data <br> <a href='".site_url("datasiswa")."' > Kembali ke Aplikasi </a>";
        
        }
   }
	
	
	public function hapus(){
		
		$id = $this->input->get_post("id",true);
		
		$this->db->delete("tm_siswa",array("id"=>$id));
		echo "sukses";
	}
	// sESI 

	public function sesi()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		 $data['tmujian_id']  = $this->input->get_post("id");
		 $data['title']   = "Pemilihan Sesi Siswa ";
	     if(!empty($ajax)){
					    
			 $this->load->view('page_sesi',$data);
		
		 }else{
			 
		     $data['konten'] = "page_sesi";
			 
			 $this->_template($data);
		 }
	

	}

	public function sesi_ujian()
	{  

		
         $ajax            = $this->input->get_post("ajax",true);	
		 $data['tmjadwal_id']  = $this->input->get_post("id");
		 $data['title']   = "Pelaksanaan Ujian  ";
	     if(!empty($ajax)){
					    
			 $this->load->view('page_sesi_ujian',$data);
		
		 }else{
			 
		     $data['konten'] = "page_sesi_ujian";
			 
			 $this->_template($data);
		 }
	

	}
	
	 public function grid_sesi(){
		
		  $iTotalRecords = $this->m->grid(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->grid(true)->result_array();
		   
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
			   $disabled="";
			   if($_SESSION['status'] !="madrasah"){
				$disabled="disabled";
			   }

			$selek ="<select class='form-control sesi' datanya='".$val['id']."' ".$disabled.">";
			$selek .="<option value=''>- Pilih Sesi - </option>";
			  $sesi = $this->db->query("select * from tm_sesi")->result();
			    foreach($sesi as $r){
					 $selected="";
					   if($r->sesi==$val['sesi']){
						  $selected="selected";
					   }
					$selek .="<option value='".$r->sesi."' ".$selected."> Sesi ".$r->sesi." - ".$r->hari." pukul ".$r->pukul."</option>";

				}
				
					
					
		    $selek .="</select>";
			
			 


				$no = $i++;
				$records["data"][] = array(
					$no,
					
					$val['madrasah'],
					$val['no_aksi']."",
					$val['nama'],
					$val['tempat'],
					$val['tgl_lahir'],
					$val['namakelas'],
					$selek
				
                   

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	

	public function pilihsesi(){

	 $sesi = $_POST['sesi'];
	 $id    = $_POST['id'];
	 if($sesi !=""){
	   $cek = $this->db->query("select count(id) as jml from tm_siswa where sesi='".$sesi."'")->row();
	   $jml = $cek->jml;
	 }else{
		$jml = 0;

	 }
	     if($jml >= 3000){

             echo 1;
		 }else{

			$this->db->set("sesi",$sesi);
			$this->db->where("id",$id);
			$this->db->update("tm_siswa");
			echo "sukses";


		 }

	}

	public function infosesi()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		
		 $data['title']   = "Informasi Sesi Siswa ";
	     if(!empty($ajax)){
					    
			 $this->load->view('page_sesi_info',$data);
		
		 }else{
			 
		     $data['konten'] = "page_sesi_info";
			 
			 $this->_template($data);
		 }
	

	}

	public function generate_no(){


		 
		$siswa = $this->db->query("select * from tm_siswa ")->result();
		 $arraysekolah = array("0"=>"1",
		                       "1"=>"2",
							   "2"=>"3",
							   "3"=>"4"); 
							   
			
		 
		   foreach($siswa as $row){
			     $maxid     = $this->db->query("select max(urutan) as pendaftar from tm_siswa ")->row();
				 $pendaftar = $maxid->pendaftar+1;
				 //$jenis = $row->jenis_sekolah;
				 
				
			   
			   
				 if(strlen($pendaftar)==1){
					 $nopen = "0000".$pendaftar;
				 }else if(strlen($pendaftar)==2){
					 
					 $nopen = "000".$pendaftar;
				 }else if(strlen($pendaftar)==3){
					 
					 $nopen = "00".$pendaftar;
				 }else if(strlen($pendaftar)==4){
					 
					 $nopen = "0".$pendaftar;
				 }else if(strlen($pendaftar)==5){
					 
					 $nopen = $pendaftar;
				 }

				 $tmujian_id=0;

				 
				 $paket       = $this->db->query('SELECT id FROM tm_jadwal WHERE JSON_CONTAINS(tmjurusan_id,\'["'.$row->literasi.'"]\') and jenjang LIKE "%'.$row->posisi.'%"')->row();
				 $tmujian_id  = $paket->id; 
				 

				 $kategori    = $this->Reff->get_kondisi(array("id"=>$row->kategori),"tm_kategori","kode");
				
				 $sesi        = strlen($row->sesi==1) ? "0".$row->sesi : $row->sesi; 
				 $no_test     = $sesi.".".$kategori.".".$nopen; 

				 //$no_test  ="10-07-503-".$nopen;
				 
				// $this->db->set("urutan",($pendaftar));
				 //$this->db->set("no_test",($no_test));
				 $this->db->set("tmujian_id",($tmujian_id));
				 //$this->db->set("token",$this->Reff->randomangka(6));
				 $this->db->where("id",$row->id);
				 $this->db->update("tm_siswa");
			   
			    
			   
		   }
		   
		   $jumlahsiswa = count($siswa); 
		   if($jumlahsiswa==0){
			    echo  "Generate Gagal , Seluruh siswa sudah di generate sebelumnya";
		   }else{
		    echo  $jumlahsiswa." Nomor Test Calon siswa berhasil di generate";
		   }


	}

	public function resetlogin(){



		$this->db->set("login",0);
		$this->db->where("id",$_POST['tmsiswa_id']);
		$this->db->update("tm_siswa");
		echo "sukses";
	}


	public function camera(){
		
		$tmujian_id = $this->input->get_post("tmujian_id");
		$data = array();
		  
			  $data['data']  = $this->Reff->get_where("h_ujian",array("id"=>$tmujian_id));
			   
		   
		$this->load->view("camera",$data);
		
	}

	public function ljk(){
		
		$tmujian_id = $this->input->get_post("tmujian_id");

		$data = array();
		$data['tmujian_id']  = $tmujian_id;  
			  
			   
		   
		$this->load->view("ljk",$data);
		
	}
	
	public function camerates(){
		
		$tmsiswa_id = $this->input->get_post("tmsiswa_id");
		$data = array();
		  
			  $data['data']  = $this->Reff->get_where("tm_siswa",array("id"=>$tmsiswa_id));
			   
		   
		$this->load->view("camerates",$data);
		
	}


public function bantuan(){

	  $data['data'] = $this->db->get_where("tm_siswa",array("id"=>$_POST['tmsiswa_id']))->row();
	  $data['pertanyaan'] = $this->db->query("select * from bantuan where tmsiswa_id='".$_POST['tmsiswa_id']."' AND jawaban ='' order by tgl_pertanyaan DESC limit 1")->row();

	  $this->load->view("bantuan",$data);


}

public function bantuan_save(){


   $this->db->set("tgl_jawaban",date("Y-m-d H:i:s"));
   $this->db->set("jawaban",$_POST['pertanyaan']);
   $this->db->where("id",$_POST['id']);
   $this->db->update("bantuan");
   echo "sukses";
}
   
	 // data siswa

	 public function ujian()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
         $id              = $this->input->get_post("id",true);	
         $kategori        = $this->input->get_post("kategori",true);	
		
		 $data['title']   	 = "Data Ujian  ";
		 $data['id']   		 =  $id; 
		 $data['kategori']   =  $kategori;
	     if(!empty($ajax)){
					    
			 $this->load->view('page_ujian',$data);
		
		 }else{
			 
		     $data['konten'] = "page_ujian";
			 
			 $this->_template($data);
		 }
	

	}
	
	 public function grid_ujian(){
		
		  $iTotalRecords = $this->m->grid_ujian(false)->num_rows();
		  $kategori_ujian = $this->input->get_post("kategori_ujian");
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->grid_ujian(true)->result_array();
		   
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
			

         
					
					$nilai    = $val['nilai']; 
					$waktu    = round($val['waktu']/60)." Menit"; 
					$camera ="<a href='#' class='btn btn-success btn-sm camera' tmujian_id='".$val['id_ujian']."' data-toggle='modal' data-target='#pelaksanaanmodal'><span class='fa fa-camera'></span> </a>";
					$ljk    ="<a href='#' class='btn btn-primary btn-sm ljk' tmujian_id='".$val['id_ujian']."' data-toggle='modal' data-target='#defaultModal'><span class='fa fa-file'></span> </a>";
				
				
			
			

				$bantuan   = $this->db->query("select id from bantuan where tmsiswa_id='".$val['id']."' AND jawaban =''")->row();
				$pertanyaan="";

				if(!is_null($bantuan)){
				$pertanyaan ="<a href='#' class='btn btn-warning btn-sm bantuan' tmsiswa_id='".$val['id']."' data-toggle='modal' data-target='#pelaksanaanmodal'><span class='fa fa-reply'></span> </a>";
			

				}
			   
				$status = "";
				  if($val['status']=="Y"){

					$status = "<br><span class='badge badge-pill badge-primary'>Sedang mengerjakan </span>";
					  if($_SESSION['status']=="admin"){

						$status .="<a href='".site_url('datasiswa/selesaikan?id='.$val['id_ujian'].'')."' class='btn btn-warning btn-sm' target='_blank'><span class='fa fa-stop'></span> </a>";
					  }

				  }else if($val['status']=="N"){

					$status = "<br><span class='badge badge-pill badge-danger'>Selesai mengerjakan </span>";

				  }

				  if($val['posisi']==1){
					

				
				  }else{
					$dposisi = array("1"=>"Guru  ","2"=>"Kepala RA","3"=>"Pengawas Madrasah ","4"=>"Dosen ","5"=>"Praktisi Pendidikan ","6"=>"Widyaiswara","7"=>"Kepala Madrasah");
												
				

				  }
			
			  $provinsi = $this->Reff->get_kondisi(array("id"=>$val['provinsi']),"provinsi","nama");
			  $kota     = $this->Reff->get_kondisi(array("id"=>$val['kota']),"kota","nama");
			  $jml_soal = $this->db->query("select count(id) as jml from tr_soal where tmujian_id='".$val['id_bank']."'")->row();
              $url = site_url("datasiswa/cetak?id=".$val['id']."&tmujian_id=".$val['id_bank']."&id_ujian=".$val['id_ujian']."");
				$no = $i++;
				$records["data"][] = array(
					$no,
					$camera.$pertanyaan.$ljk,
					$val['no_test'],
					"<a href='".$url."'>".$val['nama'].'</a>',
					$val['tgl_lahir'],
					$this->Reff->get_kondisi(array("id"=>$val['id_bank']),"tm_ujian","nama"),
					$jml_soal->jml,
					$val['jml_benar'],
					($jml_soal->jml-$val['jml_benar']),
					number_format($nilai,2),
					$waktu
					
						

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
  public function selesaikan(){


	$id_tes = $this->input->get_post('id', true);
		$id_tes = ($id_tes);
		$list_jawaban = $this->m->getJawaban($id_tes);
		
		// Pecah Jawaban
		$pc_jawaban = explode(",", $list_jawaban);
		
		$jumlah_benar 	= 0;
		$jumlah_salah 	= 0;
		$jumlah_ragu  	= 0;
		$nilai_bobot 	= 0;
		$total_bobot	= 0;
		$jumlah_soal	= sizeof($pc_jawaban);

		foreach ($pc_jawaban as $jwb) {
			$pc_dt 		= explode(":", $jwb);
			$id_soal 	= $pc_dt[0];
			$jawaban 	= $pc_dt[1];
			$ragu 		= $pc_dt[2];

			$cek_jwb 	 = $this->m->getSoalById($id_soal);
			
			if($cek_jwb->jenis==1){

				if($cek_jwb->jawaban==$jawaban){
					$jumlah_benar++;
					$nilai_bobot = $nilai_bobot + $cek_jwb->bobot;
					$total_bobot = $total_bobot + $cek_jwb->bobot;
				}else{
					$jumlah_salah++;
				} 

			}else if($cek_jwb->jenis==9){
				$arr_nilai = array("A"=>"1","B"=>"2","C"=>"3","D"=>"4","E"=>"5");
				$nilai_bobot = $nilai_bobot + $arr_nilai[$jawaban];
				$total_bobot = $total_bobot + $cek_jwb->bobot;
				
			}
			
		}

		$nilai 			= ($jumlah_benar / ($jumlah_soal))  * 100;
		$nilai_bobot 	= ($total_bobot / ($jumlah_soal))  * 100;

		$d_update = [
			'jml_benar'		=> $jumlah_benar,
			//'tgl_selesai'	=> date("Y-m-d H:i:s"),
			'nilai'			=> $nilai,
			'nilai_bobot'	=> number_format(floor($nilai_bobot), 0),
			'status'		=> 'N'
		];
          
		$this->db->update('h_ujian', $d_update,array("id"=>$id_tes));
		print_r($d_update);


  }

  public function hasilujian()
  {  
	  
		
		
			  
	   $ajax            = $this->input->get_post("ajax",true);	
	   $id              = $this->input->get_post("id",true);	
	   $kategori        = $this->input->get_post("kategori",true);	
	  
	   $data['title']   	 = "Hasil Ujian  ";
	   $data['id']   		 =  $id; 
	   $data['kategori']   =  $kategori;
	   if(!empty($ajax)){
					  
		   $this->load->view('hasilujian',$data);
	  
	   }else{
		   
		   $data['konten'] = "hasilujian";
		   
		   $this->_template($data);
	   }
  

  }

  public function grid_hasilujian(){

	$iTotalRecords = $this->m->grid_hasilujian(false)->num_rows();
	$kategori_ujian = $this->input->get_post("kategori_ujian");
	$iDisplayLength = intval($_REQUEST['length']);
	$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
	$iDisplayStart = intval($_REQUEST['start']);
	$sEcho = intval($_REQUEST['draw']);
	
	$records = array();
	$records["data"] = array(); 

	$end = $iDisplayStart + $iDisplayLength;
	$end = $end > $iTotalRecords ? $iTotalRecords : $end;
	
	$datagrid = $this->m->grid_hasilujian(true)->result_array();
	 
	$sta = array("1"=>"S1","2"=>"S2","3"=>"S3");



	 $i= ($iDisplayStart +1);
	 $no=0;
	 foreach($datagrid as $val) {
	   $no++;
		 $sesi1 = $this->db->query("select nilai from h_ujian where tmujian_id='20' and tmsiswa_id='".$val['id']."'")->row();
		 $sesi2 = $this->db->query("select nilai from h_ujian where tmujian_id='21' and tmsiswa_id='".$val['id']."'")->row();

		 $nilai = $sesi1->nilai+$sesi2->nilai;
		 
		$this->db->query("update tm_siswa set nilai='".$nilai."' where id='".$val['id']."'");
		  $records["data"][] = array(
			  $no,
			  $val['no_test'],
			  $val['nama'],		
			  $val['tgl_lahir'],	
			  
			  number_format($sesi1->nilai,2),
			  number_format($sesi2->nilai,2),
			  number_format($val['nilai'],2),
			 
			  $no	
			
			  
				  

			);

		//}
	//}
		}
  
	$records["draw"] = $sEcho;
	$records["recordsTotal"] = $iTotalRecords;
	$records["recordsFiltered"] = $iTotalRecords;
	
	echo json_encode($records);
}

public function ljkwawancara(){
		
	$tmujian_id = $this->input->get_post("tmujian_id");

	$data = array();
	$data['tmujian_id']  = $tmujian_id;  
		  
		   
	   
	$this->load->view("ljkwawancara",$data);
	
}

public function rekap2()
{  
	
	  
	  
			
	 $ajax            = $this->input->get_post("ajax",true);	
	 $id              = $this->input->get_post("id",true);	
	 $kategori        = $this->input->get_post("kategori",true);	
	
	 $data['title']   	 = "Hasil Ujian  ";
	 $data['id']   		 =  $id; 
	 $data['kategori']   =  $kategori;
	 if(!empty($ajax)){
					
		 $this->load->view('rekap',$data);
	
	 }else{
		 
		 $data['konten'] = "rekap";
		 
		 $this->_template($data);
	 }


}


public function rekap3()
{  
	
	  
	  
			
	 $ajax            = $this->input->get_post("ajax",true);	
	 $id              = $this->input->get_post("id",true);	
	 $kategori        = $this->input->get_post("kategori",true);	
	
	 $data['title']   	 = "Hasil Ujian  ";
	 $data['id']   		 =  $id; 
	 $data['kategori']   =  $kategori;
	 if(!empty($ajax)){
					
		 $this->load->view('rekap2',$data);
	
	 }else{
		 
		 $data['konten'] = "rekap2";
		 
		 $this->_template($data);
	 }


}




public function rekaplist2()
{  
	
	  
	  
			
	 $ajax            = $this->input->get_post("ajax",true);	
	 $id              = $this->input->get_post("id",true);	
	 $kategori        = $this->input->get_post("kategori",true);	
	
	 $data['title']   	 = "Hasil Ujian  ";
	 $data['id']   		 =  $id; 
	 $data['kategori']   =  $kategori;
	 if(!empty($ajax)){
					
		 $this->load->view('rekaplist2',$data);
	
	 }else{
		 
		 $data['konten'] = "rekaplist2";
		 
		 $this->_template($data);
	 }


}


public function rekaplist3()
{  
	
	  
	  
			
	 $ajax            = $this->input->get_post("ajax",true);	
	 $id              = $this->input->get_post("id",true);	
	 $kategori        = $this->input->get_post("kategori",true);	
	
	 $data['title']   	 = "Hasil Ujian  ";
	 $data['id']   		 =  $id; 
	 $data['kategori']   =  $kategori;
	 if(!empty($ajax)){
					
		 $this->load->view('rekaplist3',$data);
	
	 }else{
		 
		 $data['konten'] = "rekaplist3";
		 
		 $this->_template($data);
	 }


}


public function kelulusan()
{  
	
	  
	  
			
	 $ajax            = $this->input->get_post("ajax",true);	
	 $id              = $this->input->get_post("id",true);	
	 $kategori        = $this->input->get_post("kategori",true);	
	
	 $data['title']   	 = "Hasil Ujian  ";
	 $data['id']   		 =  $id; 
	 $data['kategori']   =  $kategori;
	 if(!empty($ajax)){
					
		 $this->load->view('kelulusan',$data);
	
	 }else{
		 
		 $data['konten'] = "kelulusan";
		 
		 $this->_template($data);
	 }


}

public function grid_kelulusan(){
  $redaksi = "Yth. Bapak/Ibu %0a %0a Dengan hormat, %0a %0a Kami mengkonfirmasi untuk segera masuk aplikasi untuk  melakukan wawancara, petunjuk wawancara ada didalam akun Pendaftaran Anda  %0a %0a Demikian atas partisipasinya kami sampaikan terima kasih. %0a %0a ".$_SESSION['nama_aksi']."";

  $iTotalRecords = $this->m->grid_kelulusan(false)->num_rows();
  $kategori_ujian = $this->input->get_post("kategori_ujian");
  $iDisplayLength = intval($_REQUEST['length']);
  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
  $iDisplayStart = intval($_REQUEST['start']);
  $sEcho = intval($_REQUEST['draw']);
  
  $records = array();
  $records["data"] = array(); 

  $end = $iDisplayStart + $iDisplayLength;
  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
  
  $datagrid = $this->m->grid_kelulusan(true)->result_array();
   
  $sta = array("1"=>"S1","2"=>"S2","3"=>"S3");
  $kepeg = array("1"=>"PNS ","2"=>"NON-PNS","3"=>"CPNS  ");

   $i= ($iDisplayStart +1);
   foreach($datagrid as $val) {
	
	 // $this->db->query("update tm_siswa set literasi = 46 where kelulusan=1 and literasi=0");
 
			$ped  = $this->db->query("select * from h_ujian where kategori=27 AND tmsiswa_id='".$val['id']."'")->row();
			$camera2="-";
			$nilai2 ="-";
			if(!is_null($ped)){
			   
			  $camera2   ="<a href='#' class='btn btn-success btn-sm camera' tmujian_id='".$ped->id."' data-toggle='modal' data-target='#pelaksanaanmodal'><span class='fa fa-camera'></span> Pedagogis </a><br>";
			  $nilai2    ="<a href='#' class='ljk' tmujian_id='".$ped->id."' data-toggle='modal' data-target='#defaultModal'>".$ped->nilai." </a>";
		  
		  


			}


			$prof  = $this->db->query("select * from h_ujian where kategori='".$val['tmujian_id']."' AND tmsiswa_id='".$val['id']."'")->row();
			$camera3="-";
			$nilai3 ="-";
			if(!is_null($prof)){
			   
			  $camera3   ="<a href='#' class='btn btn-success btn-sm camera' tmujian_id='".$prof->id."' data-toggle='modal' data-target='#pelaksanaanmodal'><span class='fa fa-camera'></span> Profesional </a><br>";
			  $nilai3    ="<a href='#' class='ljk' tmujian_id='".$prof->id."' data-toggle='modal' data-target='#defaultModal'>".$prof->nilai." </a>";
		  
		  


			}

		  if($val['pri'] !=1){
			$wawancara  = $this->db->query("select * from h_ujian_w where  tmsiswa_id='".$val['id']."'")->row();

			$nilai4 ="-";
			$opsi   ="<small class='badge badge-danger'>Blm Wawancara </small>";
			if(!is_null($wawancara)){
			   
			  $nilai4    ="<a href='#' class='ljkwawancara' tmujian_id='".$wawancara->id."' data-toggle='modal' data-target='#defaultModal'>".$wawancara->nilai." </a>";
		  
			  if($val['keputusan']==1){
				  $opsi    = "<small class='badge badge-primary'>Direkomendasikan Lulus </small>";
			  }else if($val['keputusan']==2){
				  $opsi    = "<small class='badge badge-danger'>Tidak Direkomendasikan </small>";

			  }
			  
			  

			}
			
	
			$opsi    .= "<br>Pewawancara : <br> ".$this->Reff->get_kondisi(array("id"=>$val['pewawancara']),"admin","nama")."";


		  }else{

			$wawancara  = $this->db->query("select * from h_ujian where  tmsiswa_id='".$val['id']."' and tmujian_id='19'")->row();

			$nilai4 ="-";
			$opsi   ="<small class='badge badge-danger'>Blm Wawancara </small>";
			if(!is_null($wawancara)){
			   
			  $nilai4    ="<a href='#' class='ljk' tmujian_id='".$wawancara->id."' data-toggle='modal' data-target='#defaultModal'>".$val['wawancara']." </a>";
			  $opsi    = "<small class='badge badge-primary'>sudah wawancara</small>";
			  

			}
			
	
		   


		  }
		   
		   

  
	   
  

		  if($val['posisi']==1){
			$dposisi = array("1"=>"Penulis Modul PKB Guru  ","2"=>"Tim pengembang PKB Guru tingkat Nasional","3"=>"Widyaiswara Pusdiklat dan Balai diklat Keagamaan ","4"=>"Tim Review Modul PKB Guru Madrasah ","5"=>"Instruktur Nasional PKB Guru Madrasah hasil piloting
			program PKB Guru. ","6"=>"Dosen/Praktisi pendidikan");

			$unsur = "<br><b>".$dposisi[$val['unsur']]." </b>";
		  }else{
			$dposisi = array("1"=>"Guru  ","2"=>"Kepala RA","3"=>"Pengawas Madrasah ","4"=>"Dosen ","5"=>"Praktisi Pendidikan ","6"=>"Widyaiswara","7"=>"Kepala Madrasah");
										
			$unsur = "<br><b>".$dposisi[$val['unsur']]." </b>";

		  }
	
	  $provinsi = $this->Reff->get_kondisi(array("id"=>$val['provinsi']),"provinsi","nama");
	  $kota     = $this->Reff->get_kondisi(array("id"=>$val['kota']),"kota","nama");
	  $hp       = "+62".substr($val['hp'],1);

	  $sel ="";

	  if($val['posisi']==1){
		   
		   //$literasi =  $this->Reff->get_kondisi(array("jurusan"=>$pen->pendidikan_jurusan),"literasi","id");

		  // $this->db->query("update tm_siswa set literasi='".$literasi."' where id='".$val['id']."'");
       if($val['literasi']==""){

        $literasi ="Lainnya";
	    }else{
		  $jenjang  = $this->Reff->get_kondisi(array("id"=>$val['literasi']),"literasi","jenjang");
		  $literasi = $this->Reff->get_kondisi(array("id"=>$jenjang),"jenjang","nama")." - ".$this->Reff->get_kondisi(array("id"=>$val['literasi']),"literasi","nama");
		}

		$sel ='<select class="literasi" disabled tmsiswa_id="'.$val['id'].'">';

		$literasinya = $this->db->query("select * from literasi where jenjang IN(9,10,11,0)")->result();
		  foreach($literasinya as $rl){
			   $selected ="";
			  if($rl->id==$val['literasi']){
				$selected ="selected";

			  }
           $sel .="<option value='".$rl->id."' ".$selected.">".$this->Reff->get_kondisi(array("id"=>$rl->jenjang),"jenjang","nama")."-".$rl->nama."</option>";

		  }

        $sel .="</select>";
		   
	  }else{
		  $jenjang  = $this->Reff->get_kondisi(array("id"=>$val['literasi']),"literasi","jenjang");
		  $literasi = $this->Reff->get_kondisi(array("id"=>$jenjang),"jenjang","nama")." - ".$this->Reff->get_kondisi(array("id"=>$val['literasi']),"literasi","nama");

 
		  $sel = "#".$val['rank'];

		  $sel .='<select class="kelulusan" disabled tmsiswa_id="'.$val['id'].'">';

		
			   $selected ="";
			   $selected2 ="";
				if($val['kelulusan']==1){
					$selected ="selected";
				}else{
					$selected2 ="selected";
				}

			  
           $sel .="<option value='1' ".$selected."> Lulus </option>";
           $sel .="<option value='2' ".$selected2."> Tidak Lulus </option>";

		  

		$sel .="</select>";
		



		/*$sel .='<select class="literasi" disabled tmsiswa_id="'.$val['id'].'">';
        //if()
		$literasinya = $this->db->query("select * from literasi where jenjang IN(9,10,11,0)")->result();
		  foreach($literasinya as $rl){
			   $selected ="";
			  if($rl->id==$val['literasi']){
				$selected ="selected";

			  }
           $sel .="<option value='".$rl->id."' ".$selected.">".$this->Reff->get_kondisi(array("id"=>$rl->jenjang),"jenjang","nama")."-".$rl->nama."</option>";

		  }

		$sel .="</select>";
		*/
	  }
	  
		  
	   $pri     = ($val['pri']==1) ? "<small class='badge badge-success'>Prioritas</small>" :"";
	  
	 $dapen   = $this->db->query("select * from tr_pendidikan where peserta_id='".$val['id']."' order by tingkat asc ")->result();
	     
		  $rp ="<ol>";
			 foreach($dapen as $rowpen){

			  $rp .="<li>".$sta[$rowpen->tingkat]."-".$this->Reff->get_kondisi(array("id"=>$rowpen->jurusan),"jurusan","nama")."</li>";   
			 }

			$rp .="</ol>";   
	  $pen  = $this->db->query("select * from peserta where id='".$val['id']."' ")->row();
			
	   $lp   = $sta[$pen->pendidikan]."-".$this->Reff->get_kondisi(array("id"=>$pen->pendidikan_jurusan),"jurusan","nama");
	   $statuskepeg = $this->Reff->get_kondisi(array("id"=>$val['id']),"peserta","status_kepegawaian");
        $l = ($val['kelulusan']==1) ? "(*)" :"";
		$no = $i++;
		$records["data"][] = array(
		  $no,  
		    $val['no_test'].$l,
			$val['nama'],
			$this->Reff->get_kondisi(array("id"=>$val['id']),"peserta","jk"),
			$this->Reff->get_kondisi(array("id"=>$val['id']),"peserta","hp"),
			$this->Reff->get_kondisi(array("id"=>$val['id']),"peserta","email"),
			$kepeg[$statuskepeg],
			$unsur,
			$lp,
			$literasi,
			$provinsi,
		    $kota,	
		  
			
			$nilai2,
			$nilai3,
			$val['nilai'],
			$nilai4,
			
			$sel
			
			
				

		  );
	  }

  $records["draw"] = $sEcho;
  $records["recordsTotal"] = $iTotalRecords;
  $records["recordsFiltered"] = $iTotalRecords;
  
  echo json_encode($records);
}

  public function ubah_literasi(){

     $this->db->set("literasi",$_POST['literasi']);
     $this->db->where("id",$_POST['tmsiswa_id']);
	 $this->db->update("tm_siswa");
	 
	 echo "sukses";

  }

  public function ubah_kelulusan(){

	$this->db->set("kelulusan",$_POST['kelulusan']);
	$this->db->where("id",$_POST['tmsiswa_id']);
	$this->db->update("tm_siswa");
	
	echo "sukses";

 }

 public function cetak(){
	    
	$data['peserta']   = $this->db->query("select * from tm_siswa where id='".$_GET['id']."'")->row();
		
	$data['soal']      = $this->db->query("select * from   tm_ujian where id='".$_GET['tmujian_id']."'")->row();
	$data['id_ujian']  = $_GET['id_ujian'];
		
	$pdf_filename = "Jawaban ".str_replace(array(".","'"),"",$data['peserta']->nama).str_replace(",","_",$data['soal']->nama).".pdf";	 

	$user_info = $this->load->view('cetak', $data, true);

	 $output = $user_info;
	
	 //echo $output;

	generate_pdf($output, $pdf_filename,TRUE);
	


}

}
