<div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">Petunjuk Ujian CBT </h3>

                   
                    </div>


                <div class="box-body">
                  <div class="row">
                   
                    <div class="col-md-10">
                    <p>
                    <ol start="1">
                    <li> Berdoalah kepada Allah SWT sebelum Anda  memulai mengerjakan soal. </li>
                    
                    <li> Waktu ujian akan dimulai ketika Anda menekan tombol <b><span class="fa fa-edit"></span> Mulai Ujian </b> </li>
                    
                    </ol>
                    
                    </p>
                    </div>
                    </div>

                    


                </div>

</div>

<div class="info-box">
                        <span class="info-box-icon bg-yellow"><i class="fa fa-file"></i></span>

                        <div class="info-box-content">
                        <span class="info-box-text" style="font-weight:bold">Simulasi Ujian 
                          
                          </span>
                        
                        <?php 

                        $simulasi      = $this->db->query("select * from tm_ujian where simulasi=1")->row();
                        $soal_simulasi = $this->db->query("select count(id) as jml from tr_soal where tmujian_id='".$simulasi->id."'")->row();
                        $h_ujian       = $this->db->query("select * from h_ujian where tmujian_id='".$simulasi->id."' and tmsiswa_id='".$_SESSION['siswaID']."'")->row();
                        $status        ="<span class='text-red'>Belum Mengerjakan</span>";
                        $button        = '<a class="btn btn-success" href="'.site_url('cbt/ujian?kategori='.base64_encode($simulasi->kategori).'&parameter='.base64_encode($simulasi->id).'').'"><i class="fa fa-edit"></i> Mulai  Simulasi </a>';
                        if(!is_null($h_ujian)){

                            if($h_ujian->status=="N"){
                                
                                $status        ="<span class='text-success'>Sudah Mengikuti <br> Jumlah Benar : ".$h_ujian->jml_benar." <br> Nilai Simulasi : ".$h_ujian->nilai."</span>";

                            }else if($h_ujian->status=="Y"){
                                $status        ="<span class='text-yellow'>Belum Menyelesaikan</span>";

                            }

                        }

                        if($data->foto_awal==""){
                            //$status        ="<span class='text-red'>Silahkan rekam foto terlebih dahulu </span>";
                            //$button        = '<span class="text-red"> Belum rekam foto </span>';
                         } 


                        ?>
                        <span class="">
                        Tidak akan berpengaruh terhadap hasil ujian, Anda dapat mengikuti simulasi ujian untuk memahami cara pengisian lembar jawaban
                        </span>
                        </div>
                        <!-- /.info-box-content -->
                        <div class="row">
                             <div class="col-md-2"> Jumlah : <?php echo $soal_simulasi->jml; ?> Soal </div>
                             <div class="col-md-2"> Waktu : <?php echo $simulasi->waktu; ?>  Menit  </div>
                             <div class="col-md-3"> Status  : <?php echo $status; ?>    </div>
                             <div class="col-md-2"> <?php echo $button; ?> </div>
                             
                               
                            </div>
                    </div>

<?php 
  $jadwal = $this->db->query("SELECT * FROM tm_jadwal ")->result();
   foreach($jadwal as $rj){
    $ujian = $this->db->query("select id,nama,status,kategori,waktu from tm_ujian where kategori='".$rj->id."' ")->row();  
?>

<div class="box box-warning">
             

                <div class="box-body">

                        <div class="info-box">
                                <span class="info-box-icon bg-green"><i class="fa fa-file"></i></span>

                                <div class="info-box-content">
                                <span class="info-box-text" style="font-weight:bold"> <?php echo $ujian->nama; ?>  </span>
                                <?php 

                                                             

                                   
                                    $status        ="<span class='text-red'>Ujian Belum Aktif</span>";
                                    $button        = '<span class="text-red"> Ujian ini akan dilaksanakan sesuai jadwal yang sudah ditentukan </span>';

                                   if($ujian->status==1){
                                    $status        ="<span class='text-blue'> Aktif, Silahkan kerjakan <br>(klik tombol mulai ujian) </span>";
                                    $button     = '<a class="btn btn-success" href="'.site_url('cbt/ujian?kategori='.base64_encode($ujian->kategori).'&parameter='.base64_encode($ujian->id).'').'"><i class="fa fa-edit"></i> Mulai Ujian  </a> ';
                                   }

                                   $h_ujian = $this->db->query("select * from h_ujian where tmujian_id='".$ujian->id."' and tmsiswa_id='".$data->id."'")->row();  
                                    if(!is_null($h_ujian)){

                                        if($h_ujian->status=='Y'){
                                            $status        ="<span class='text-red'>Belum Menyelesaikan </span>";
                                            $button        = '<a class="btn btn-success" href="'.site_url('cbt/ujian?kategori='.base64_encode($ujian->kategori).'&parameter='.base64_encode($ujian->id).'').'"><i class="fa fa-edit"></i> Lanjutkan Ujian  </a> ';
                      
        

                                        }else if($h_ujian->status=='N'){   
                                            //$status        ="<span class='text-success'>Sudah Mengikuti <br> Jumlah Benar : ".$h_ujian->jml_benar." <br> Skor  : ".$h_ujian->nilai."</span>";
                                            $status        ="<span class='text-success'>Sudah dikerjakan  </span>";
                                            $button        = '<span class="text-green"> Sudah menyelesaikan ujian CBT ini </span>';
        


                                        }
                                    }


                                    if($data->foto_awal==""){
                                        //  $status        ="<span class='text-red'>Silahkan rekam foto terlebih dahulu </span>";
                                        // $button        = '<span class="text-red"> Belum rekam foto </span>';
                                          }



                                       
                                ?>

                                

                                </div>
                                <div class="row">
                                    <div class="col-md-5">   <?php echo $rj->hari; ?> -  <?php echo $rj->waktu; ?>  </div>
                                    <div class="col-md-1">   <?php echo $ujian->waktu; ?> </div>
                                    <div class="col-md-2">   <?php echo $status; ?> </div>
                                    <div class="col-md-2"> <?php echo $button; ?>  </div>
                                    
                                    
                                </div>
                        </div>


                </div>
</div>

<?php 
   }
   ?>
