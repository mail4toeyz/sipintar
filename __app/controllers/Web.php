<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
defined('BASEPATH') OR exit('No direct script access allowed');

class Web extends CI_Controller {

	function __construct()
	{
		parent::__construct();	
		require APPPATH.'libraries/phpmailer/src/Exception.php';
		require APPPATH.'libraries/phpmailer/src/PHPMailer.php';
		require APPPATH.'libraries/phpmailer/src/SMTP.php';


		$this->load->model("M_web","mf");
		date_default_timezone_set("Asia/Jakarta");
	}



	/*public function tarikSoal(){
   
		 $soal= $this->db->query("SELECT * FROM `tr_soal_lama`")->result();
		   foreach($soal as $rows){

			   $this->db->set("tmujian_id",2);
			   $this->db->set("jenis",1);
			   $this->db->set("soal",$rows->soal);
			   $this->db->set("opsi_a",$rows->opsi_a);
			   $this->db->set("opsi_b",$rows->opsi_b);
			   $this->db->set("opsi_c",$rows->opsi_c);
			   $this->db->set("opsi_d",$rows->opsi_d);
			   $this->db->set("opsi_e",$rows->opsi_e);
			   $this->db->set("jawaban",$rows->jawaban);
			   $this->db->set("bobot",$rows->bobot);
			   $this->db->set("urutan",$rows->urutan);
			   $this->db->set("kategori",$rows->jenis);
			   $this->db->insert("tr_soal");

		   }
		   echo "sukses";


	} */
	public function tarikpeserta(){

		$data = $this->db->query("SELECT * FROM `tm_peserta` where no_test IS NOT NULL and sts=2")->result();
		  foreach($data as $rd){
            $cf = $this->db->query("select nama_file from tm_data_upload where id_admin='".$rd->id."' and UPPER(nama_file) LIKE '%Pas%' limit 1")->row();
			$this->db->set("id",$rd->id);
			$this->db->set("nik",$rd->reg);
			$this->db->set("nama",$rd->nama);
			$this->db->set("gender",$rd->jk);
			$this->db->set("token",$rd->tgl_lahir);
			$this->db->set("tempat",$rd->tempat_lahir);
			$this->db->set("tgl_lahir",$rd->tgl_lahir);
			$this->db->set("kategori",$rd->posisi_peminatan);
			$this->db->set("madrasah_peminatan",$rd->madrasah_peminatan);
			$this->db->set("foto",$cf->nama_file);
			$this->db->set("provinsi",$rd->idprov);
			$this->db->set("kota",$rd->idkab);
			$this->db->set("email",$rd->email);
			$this->db->set("no_test",$rd->no_test);
			$this->db->insert("tm_siswa");
		  }


	 }


	public  function generate_posisi(){

		  $data = $this->db->get("tm_siswa")->result();
		   foreach($data as $rd){

			 $posisi = $this->db->get_where("v_peserta",array("id"=>$rd->id))->row();
			 //$unsur  = ($posisi->unsur_xx_posisi==0) ? $posisi->unsur_xx : $posisi->unsur_xx_posisi;
			 $this->db->set("hp",$posisi->hp);
			/* $this->db->set("kota",$posisi->kota);
			 $this->db->set("unsur",$unsur); */
			 $this->db->where("id",$rd->id);
			 $this->db->update("tm_siswa");
		   }

		   echo "sukses";


	}
	
	public function email_verval(){


		$q = $this->db->query("SELECT * from peserta where  status =0 AND sendmail IS NULL")->result();
		 foreach($q as $r){
		   $email = $r->email;
   
		   
   
   
   

		$keterangan ="Assalamu'alaikum War. Wab. <br><br>";
		$keterangan .="Bapak/Ibu ".$r->nama.", <br> ";
		$keterangan .="Kami ucapkan terimakasih sudah melakukan pendaftaran dalam <br><b>SELEKSI INSTRUKTUR NASIONAL
        FASILITATOR PROVINSI DAN DAERAH (IN, FASPROV, FASDA)
        PKB GURU MI - MTs - MA </b>  pada alamat https://madrasahreform.kemenag.go.id/fasilitatorpkb <br>";
		$keterangan .="Sistem kami mendeteksi bahwa Anda belum menyelesaikan pendaftaran, silahkan untuk segera menyelesaikan pendaftaran sebelum tanggal 28 Maret 2021 <br>";
		$keterangan .="Jika Anda tidak menyelesaikan pendaftaran sampai waktu yang sudah ditetapkan, data Anda tidak dapat kami verifikasi dan akan dianggap gugur <br> ";
		$keterangan .="Saat ini, Data Anda terdaftar sebagai berikut : <br> <br>";
	
		$keterangan .="<b>Nama  Peserta : ".$r->nama." </b><br>";
		$keterangan .="<b>Nomor Induk Kependudukan (NIK) : ".$r->nik."</b> <br>";
	    $keterangan .="<b>Email : ".$r->email."</b> <br><br>";
		
		$keterangan .="Jika Anda lupa password, silahkan klik lupa password pada halaman login <br><br>";
		$keterangan .="Berikut adalah timeline  seleksi  :<br>";
		$keterangan .="(1) Batas akhir menyelesaikan pendaftaran pada tanggal 30 Maret 2021 Pukul 12:00 WIB <br>";
		$keterangan .="(2) Pengumuman Kelulusan Seleksi Administrasi dan Peserta CBT pada tanggal 1 April 2021 (dapat diakses melalui akun seleksi masing-masing pada alamat pendaftaran) <br>";
		$keterangan .="(3) Simulasi Pelaksanaan CBT pada tanggal 2 April 2021 <br>";
		$keterangan .="(4) Pelaksanaan CBT pada tanggal 3-4 April 2021 sesuai sesi masing-masing pada kartu peserta tes <br>";
		$keterangan .="(5) Pelaksanaan wawancara dilaksanakan pada 5 - 10 April 2021 <br>";
		$keterangan .="(6) Pengumuman dan penetapan (SK Dir.GTK) pada 13 April 2021 <br><br>";
		$keterangan .="Wassalaamualaikum War. Wab";


		    $this->send_mail($email,$keterangan);
   

		 }
	   }
	   
   
	   public function send_mail($tujuan,$pesan) {
		   $response = false;
		   $mail = new PHPMailer(); // create a new object
		   $mail->IsSMTP(); // enable SMTP
		   $mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
		   $mail->SMTPAuth = true; // authentication enabled
		   $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
		   $mail->Host = "smtp.gmail.com";
		   $mail->Port = 465; // or 587
		   $mail->IsHTML(true);
		   $mail->Username = "berbagi@madrasah.kemenag.go.id";
		   $mail->Password = "Indonesia45";
		   $mail->SetFrom("berbagi@madrasah.kemenag.go.id");
		   $mail->Subject = "Pengumuman dan Informasi Seleksi IN, FASPROV dan FASDA ";
		   $mail->Body = $pesan;
		   $mail->AddAddress($tujuan);
	
			// Send email
			if(!$mail->send()){
   
			   echo $mail->ErrorInfo;
				
			}else{
   
			    $this->db->query("update peserta set sendmail=1 where email='".$tujuan."'");
			   echo "Berhasil";
			   
			}
	  }

	  


	  public function email_selesai(){


		$q = $this->db->query("SELECT * from peserta where  status !=0 AND sendmail IS NULL")->result();
		 foreach($q as $r){
		   $email = $r->email;
   
		   
   
   
   

		$keterangan ="Assalamu'alaikum War. Wab. <br><br>";
		$keterangan .="Bapak/Ibu ".$r->nama.", <br> ";
		$keterangan .="Kami ucapkan terimakasih sudah melakukan pendaftaran dalam <br><b>SELEKSI INSTRUKTUR NASIONAL
        FASILITATOR PROVINSI DAN DAERAH (IN, FASPROV, FASDA)
        PKB GURU MI - MTs - MA </b>  pada alamat https://madrasahreform.kemenag.go.id/fasilitatorpkb <br>";
		 $keterangan .="Data Anda sudah kami terima dan dalam tahap verifikasi administrasi. <br>";
		//$keterangan .="Jika Anda tidak menyelesaikan pendaftaran sampai waktu yang sudah ditetapkan, data Anda tidak dapat kami verifikasi dan akan dianggap gugur <br> ";
		$keterangan .="Saat ini, Data Anda terdaftar sebagai berikut : <br> <br>";
	
		$keterangan .="<b>Nama  Peserta : ".$r->nama." </b><br>";
		$keterangan .="<b>Nomor Induk Kependudukan (NIK) : ".$r->nik."</b> <br>";
	    $keterangan .="<b>Email : ".$r->email."</b> <br><br>";
		
		$keterangan .="Jika Anda lupa password, silahkan klik lupa password pada halaman login <br><br>";
		$keterangan .="Berikut adalah timeline  seleksi  :<br>";
		$keterangan .="(1) Batas akhir menyelesaikan pendaftaran pada tanggal 28 Maret 2021<br>";
		$keterangan .="(2) Pengumuman Kelulusan Seleksi Administrasi dan Peserta CBT pada tanggal 1 April 2021 (dapat diakses melalui akun seleksi masing-masing pada alamat pendaftaran) <br>";
		$keterangan .="(3) Simulasi Pelaksanaan CBT pada tanggal 2 April 2021 <br>";
		$keterangan .="(4) Pelaksanaan CBT pada tanggal 3-4 April 2021 sesuai sesi masing-masing pada kartu peserta tes <br>";
		$keterangan .="(5) Pelaksanaan wawancara dilaksanakan pada 5 - 10 April 2021 <br>";
		$keterangan .="(6) Pengumuman dan penetapan (SK Dir.GTK) pada 13 April 2021 <br><br>";
		$keterangan .="Wassalaamualaikum War. Wab";


		    $this->send_mail2($email,$keterangan);
   
			
		 }
	   }
	   

	   public function singkron_nilai(){


		$pedadogis   = $this->db->query("select * from h_ujian_w ")->result();
		  foreach($pedadogis as $row){
			  
			  $this->db->set("wawancara",$row->nilai);
			  $this->db->where("id",$row->tmsiswa_id);
			  $this->db->update("tm_siswa");

		  }
		



		  echo "sukses";



     }
   
	   public function send_mail2($tujuan,$pesan) {
		   $response = false;
		   $mail = new PHPMailer(); // create a new object
		   $mail->IsSMTP(); // enable SMTP
		   $mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
		   $mail->SMTPAuth = true; // authentication enabled
		   $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
		   $mail->Host = "smtp.gmail.com";
		   $mail->Port = 465; // or 587
		   $mail->IsHTML(true);
		   $mail->Username = "dendinuraziz@madrasah.kemenag.go.id";
		   $mail->Password = "Bismillah2020";
		   $mail->SetFrom("dendinuraziz@madrasah.kemenag.go.id");
		   $mail->Subject = "Pengumuman dan Informasi Seleksi IN, FASPROV dan FASDA ";
		   $mail->Body = $pesan;
		   $mail->AddAddress($tujuan);
	
			// Send email
			if(!$mail->send()){
   
			   echo $mail->ErrorInfo;
				
			}else{
   
			   $this->db->query("update peserta set sendmail=1 where email='".$tujuan."'");
			   echo "Berhasil";
			   
			}
	  }


	
	public function soal(){

		$soal = $this->db->get("tr_soal")->result();
		  foreach($soal as $rs){

			  $this->db->set("soal",str_replace("https://pipmadrasah.kemenag.go.id/aksi/","https://pipmadrasah.kemenag.go.id/aksi2020/",$rs->soal));
			  $this->db->set("opsi_a",str_replace("https://pipmadrasah.kemenag.go.id/aksi/","https://pipmadrasah.kemenag.go.id/aksi2020/",$rs->opsi_a));
			  $this->db->set("opsi_b",str_replace("https://pipmadrasah.kemenag.go.id/aksi/","https://pipmadrasah.kemenag.go.id/aksi2020/",$rs->opsi_b));
			  $this->db->set("opsi_c",str_replace("https://pipmadrasah.kemenag.go.id/aksi/","https://pipmadrasah.kemenag.go.id/aksi2020/",$rs->opsi_c));
			  $this->db->set("opsi_d",str_replace("https://pipmadrasah.kemenag.go.id/aksi/","https://pipmadrasah.kemenag.go.id/aksi2020/",$rs->opsi_d));
			  $this->db->set("opsi_e",str_replace("https://pipmadrasah.kemenag.go.id/aksi/","https://pipmadrasah.kemenag.go.id/aksi2020/",$rs->opsi_e));
			  $this->db->where("id",$rs->id);
			  $this->db->update("tr_soal");
			  
		  }
		  echo "sukses";
	}


	public function generate_jadwal(){

		$guru = $this->db->query("select * from tm_jadwal  where jenjang=3")->result();
		 // echo (count($guru));
		 $no=0;
		   foreach($guru as $rg){
				
				
				$this->db->set("nama",$rg->matapelajaran);				
				$this->db->set("waktu",90);				
				$this->db->set("jenjang",$rg->jenjang);				
				$this->db->set("publish",1);				
				$this->db->set("kategori",$rg->id);				
				
				$this->db->insert("tm_ujian");

				$no++;

		   }
		   echo $no."-sukses";
	}

	public function literasi2(){

		$guru = $this->db->query("select id from tm_siswa where literasi2=0")->result();
		$no=0;
		   foreach($guru as $rg){
				$dl = $this->db->query("SELECT id FROM `tm_ujian` WHERE literasi !='4'")->result();
				  $literasi="";
				  foreach($dl as $rd){
                   $literasi .= ",".$rd->id;

				  }
				 $literasi = substr($literasi,1);

				 $genreArray = explode(',', $literasi);
                  $genre = $genreArray[mt_rand(0, (count($genreArray)-1))]; 
				
				$this->db->set("literasi2",$genre);				
				$this->db->where("id",$rg->id);
				$this->db->update("tm_siswa");
				$no++;

		   }
		   echo $no."-sukses";
	}

	public function generateujian(){
	  
		 $ujian = $this->db->get("tm_ujian")->result();
		   foreach($ujian as $rj){
			 $token = $this->Reff->get_id(4);
			  $literasi = $this->Reff->get_kondisi(array("id"=>$rj->literasi),"tm_literasi","nama");

			   $this->db->set("token",$token);
			   $this->db->set("nama",$literasi);
			   $this->db->where("id",$rj->id);
			   $this->db->update("tm_ujian");


		   }

	}
	public function index()
	{
		$data = array();
		$browser = $this->agent->browser();
		$this->Reff->pengunjung($browser);
		$this->load->view('page_header',$data);
	}
   public function sambutan(){
		
		$data 			= array();
		$data['title']  = "Sambutan ";
		$data['konten'] = "page_video";
		
		$this->load->view('page_header',$data);
		
	}
	
	public function panduan(){
		
		$data 			= array();
		$data['title']  = "Panduan Pendirian Madrasah ";
		$data['konten'] = "page_panduan";
		
		$this->load->view('page_header',$data);
		
	}
	public function persyaratan(){
		
		$data 			= array();
		$data['title']  = "Persyaratan Pendirian Madrasah ";
		$data['konten'] = "page_persyaratan";
		
		$this->load->view('page_header',$data);
		
	}
	public function juknis(){
		
		$data 			= array();
		$data['title']  = "Petunjuk Teknis Pendirian Madrasah ";
		$data['konten'] = "page_juknis";
		
		$this->load->view('page_header',$data);
		
	}
	
	public function undang(){
		
		$data 			= array();
		$data['title']  = "Regulasi- Undang Undang";
		$data['konten'] = "page/page_undang";
		
		$this->load->view('page_header',$data);
		
	}
	public function pemerintah(){
		
		$data 			= array();
		$data['title']  = "Regulasi- Peraturan Pemerintah";
		$data['konten'] = "page/page_pemerintah";
		
		$this->load->view('page_header',$data);
		
	}
	public function menteri(){
		
		$data 			= array();
		$data['title']  = "Regulasi- Peraturan Menteri";
		$data['konten'] = "page/page_menteri";
		
		$this->load->view('page_header',$data);
		
	}
	
	public function dirjen(){
		
		$data 			= array();
		$data['title']  = "Regulasi- Keputusan Dirjen";
		$data['konten'] = "page/page_dirjen";
		
		$this->load->view('page_header',$data);
		
	}
	public function registrasi(){
		
		$data 			= array();
		$data['title']  = "Registrasi Madrasah";
		$data['konten'] = "page_registrasi";
		
		$this->load->view('page_header',$data);
		
	}
	
	public function login(){
		
		$data 			= array();
		$data['title']  = "Login Madrasah";
		$data['konten'] = "page_login";
		
		$this->load->view('page_header',$data);
		
	}
	public function download(){
		
		$data 			= array();
		$data['title']  = "Download Aplikasi SIBOS PINTAR";
		$data['konten'] = "page_download";
		
		$this->load->view('page_header',$data);
		
	}
	
	public function datapokok($jenjang){
		
		$data 			= array();
		$data['title']  = "Rekapitulasi Data Madrasah";
		$data['konten'] = "page_dapo";
		$data['jenjang']= $jenjang;
		$this->load->view('page_header',$data);
		
	}
	
	public function manual(){
		
		$data 			= array();
		$data['title']  = "User Manual SIBOS PINTAR";
		$data['konten'] = "page_manual";
		
		$this->load->view('page_header',$data);
		
	}
	public function logink(){
		
		$data 			= array();
		$data['title']  = "Login Kanwil";
		$data['konten'] = "page_logink";
		
		$this->load->view('page_header',$data);
		
	}
	
	public function do_login(){
		
		   $data = $this->db->get_where("dmadrasahs",array("email"=>$_POST['username'],"password"=>sha1(md5($this->security->xss_clean($this->input->get_post("password"))))))->row();
		      if(count($data) > 0){
				  
				     $session = array(
																'tmmadrasah_id'        => $data->id,
																'nama'        => $data->nama,
																'status'        => "madrasah",
																'date_login'   => date("Y-m-d H:i:s"));
																
																$this->session->sess_expiration = '1000000';
																$this->session->set_userdata($session);
																
																
			        echo "Proses Login kedalam Aplikasi  berhasil, anda akan di alihkan ke halaman Dashboard";
				}else{
					
					 header('Content-Type: application/json');
                     echo json_encode(array('error' => true, 'message' => "Akun anda tidak ditemukan,mohon periksa Email  dan Password yang anda masukkan"));
					
				}
		
		
	}
	
	
	public function do_logink(){
     
        $this->form_validation->set_message('required', '{field} Tidak Boleh Kosong. ');
        
        $this->form_validation->set_message('min_length', '{field} Minimal 6 Karakter .');
        $this->form_validation->set_message('matches', '{field} Tidak Sama dengan yang dikolom password .');
        $this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , mungkin anda sudah pernah melakukan pendaftaran, silahkan klik menu login .');
				 
		 
			
				$config = array(
				    array('field' => 'username', 'label' => 'Username   ', 'rules' => 'trim|required'),
				  
				    array('field' => 'password', 'label' => 'Password ', 'rules' => 'trim|required|min_length[6]'),
				  
				 
	
					
				);
				
				$this->form_validation->set_rules($config);	
        
        if ($this->form_validation->run() == true) {
			  
			  
			   $data = $this->M_web->loginkomite();
			    if($data !="gagal"){
			        echo $data;
				}else{
					
					 header('Content-Type: application/json');
                     echo json_encode(array('error' => true, 'message' => "Akun anda tidak ditemukan,mohon periksa username dan Password yang anda masukkan"));
					
				}
												 
	     } else {
	         
           
                header('Content-Type: application/json');
                echo json_encode(array('error' => true, 'message' => validation_errors()));
            
        }	
	
	
	}
	public function do_registrasi(){
     
        $this->form_validation->set_message('required', '{field} Tidak Boleh Kosong. ');
        $this->form_validation->set_message('numeric', '{field} Harus terdiri dari angka. ');
        
        $this->form_validation->set_message('min_length', '{field} Minimal terdiri dari 6 digit .');
        $this->form_validation->set_message('matches', '{field} Tidak Sama dengan yang dikolom password .');
        $this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , mungkin anda sudah pernah melakukan pendaftaran, silahkan klik menu login .');
				 
		 
			
				$config = array(
				    array('field' => 'f[nama]', 'label' => 'Nama Calon Madrasah ', 'rules' => 'trim|required'),
				    array('field' => 'f[level_id]', 'label' => 'Jenjang Pendidikan ', 'rules' => 'trim|required'),
					 array('field' =>'f[pendiri]', 'label' => 'Nama Pendiri ', 'rules' => 'trim|required'),
					 array('field' =>'f[email]', 'label' => 'Email  ', 'rules' => 'trim|required'),
				    array('field' => 'password', 'label' => 'Password  ', 'rules' => 'trim|required|min_length[6]'),
				    array('field' => 'passwordc', 'label' => 'Ulangi Password ', 'rules' => 'trim|required|matches[password]'),
				    array('field' => 'f[propinsi_id]', 'label' => 'Provinsi ', 'rules' => 'trim|required'),
				    array('field' => 'f[kota_id]', 'label' => 'Kota', 'rules' => 'trim|required'),
				    array('field' => 'f[kecamatan_id]', 'label' => 'Kecamatan ', 'rules' => 'trim|required'),
				    array('field' => 'f[desa_id]', 'label' => 'Desa ', 'rules' => 'trim|required'),
				    
				 
	
					
				);
				
				$this->form_validation->set_rules($config);	
        
        if ($this->form_validation->run() == true) {
			  
			  
			   $this->M_web->registrasi();
			   echo "Proses membuat akun Anda berhasil, anda akan di alihkan ke halaman Dashboard";
												 
	     } else {
	         
           
                header('Content-Type: application/json');
                echo json_encode(array('error' => true, 'message' => validation_errors()));
            
        }	
	
	
	}
	
	
	
	public function kontak(){
		
		$data 			= array();
		$data['title']  = "Kontak Kami";
		$data['konten'] = "page/page_kontak";
		
		$this->load->view('page_header',$data);
		
	}
	public function logout(){
		     $this->session->sess_destroy();
			redirect(base_url());
		 
	 }
	 
	 
	 
	  public function tracking(){
		 
		 $id = base64_decode($this->input->get_post('data'));
		
		 $data['madrasah'] = $this->db->query("select * from tm_yayasan where id='".$id."'")->row();
		 
		 $this->load->view("tracking",$data);
		 
		 
		 
		 
	  }
	 

	  public function generate_lulus_fasprov(){

			$data = $this->db->query("select * from provinsi where status=1 and id='64'")->result();

			  foreach($data as $row){
				 
				   $literasi = $this->db->query("select * from literasi where jenjang IN(1,2,3,0)")->result();
					echo $row->nama; echo "<br>";
				       foreach($literasi as $rl){

                            $kolom ="s".$rl->id;
					     	$peserta   = $this->db->query("select * from tm_siswa where provinsi='".$row->id."' and posisi=2 and literasi='".$rl->id."' and keputusan=1 order by nilai DESC,pedadogis DESC,profesional DESC, wawancara DESC")->result();
								$rank =0;
								echo $rl->nama; echo "<br>";
								   foreach($peserta as $pes){
                               
									 $rank++;

									 
									 echo $rank."-".$pes->nama;
									 $kuota = $row->$kolom;
									 $this->db->query("update tm_siswa set kuota='".$kuota."', rank='".$rank."',jml_peserta='".count($peserta)."' where id='".$pes->id."'");
									 if($rank <= $row->$kolom){
										echo "-L";
										$this->db->query("update tm_siswa set kelulusan='1' where id='".$pes->id."'");
									  }
									  echo "<br>";

								   }  
								   
								   echo "<hr>";

					   }
				  


			  }


	  }


	  public function generate_lulus_fasda(){

		$data = $this->db->query("select * from kota where provinsi_id IN(select id from provinsi where id=64)")->result();

		  foreach($data as $row){
			 
			   $literasi = $this->db->query("select * from literasi where jenjang IN(5,6,7,0)")->result();
				echo $row->nama; echo "<br>";
				   foreach($literasi as $rl){

						$kolom ="s".$rl->id;
						 $peserta   = $this->db->query("select * from tm_siswa where kota='".$row->id."' and posisi=3 and literasi='".$rl->id."' and keputusan=1 order by nilai DESC,pedadogis DESC,profesional DESC, wawancara DESC")->result();
							$rank =0;
							echo $rl->nama; echo "<br>";
							   foreach($peserta as $pes){
						   
								 $rank++;
                                  
								 
								 echo $rank."-".$pes->nama;
								

								 $this->db->query("update tm_siswa set rank='".$rank."',jml_peserta='".count($peserta)."' where id='".$pes->id."'");
								 if($rank <= $row->$kolom){
									echo "-L";
									

										$this->db->query("update tm_siswa set kelulusan='1' where id='".$pes->id."'");
									
								  }
								  echo "<br>";

							   }  
							   
							   echo "<hr>";

				   }
			  


		  }


  }

	public function cek_pengawas(){


		
	}
	 
	
}
