<?php

class M_siswa extends CI_Model {
 
    public function __construct() {
        parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
    }

    
	public function grid($paging){
       
	    $key = $this->input->get_post("keyword");
	    $tmmadrasah_id = $this->input->get_post("tmmadrasah_id");
		
		$sesi       = $this->input->get_post("sesi");
		$this->db->select("*");
        $this->db->from('v_siswa');
	  
		if($_SESSION['status'] =="madrasah"){
			$this->db->where("tmmadrasah_id",$_SESSION['tmmadrasah_id']);
		}
		if(!empty($sesi)){  $this->db->where("sesi",$sesi);    }
	    if(!empty($key)){  $this->db->where("(no_aksi LIKE '%".$key."%' or UPPER(nama) LIKE '%".strtoupper($key)."%')");    }
	    if(!empty($tmmadrasah_id)){  $this->db->where("tmmadrasah_id",$tmmadrasah_id);    }
	    if(!empty($kelas)){  $this->db->where("kelas",$kelas);    }
	
		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 $this->db->order_by("sesi","ASC");
					 $this->db->order_by("tmmadrasah_id","ASC");
					 $this->db->order_by("kelas","ASC");
					 $this->db->order_by("nama","ASC");
					
		
			 }
      
		
		
		return $this->db->get();
		
	}
	
	public function grid_hasil($paging){
       
	    $key = $this->input->get_post("keyword");
	    $tmmadrasah_id = $this->input->get_post("tmmadrasah_id");
		$literasi1  = $this->input->get_post("literasi1");
		$literasi2  = $this->input->get_post("literasi2");
		$sesi       = $this->input->get_post("sesi");


		if(!empty($literasi1)){

			  if($literasi1==1){

				$this->db->where("id in (select tmsiswa_id  from h_ujian where status='Y' and literasi='1') ");


			  }else if($literasi1==2){

				$this->db->where("id in (select tmsiswa_id  from h_ujian where status='N' and literasi='1') ");


			  }


		}
		

		if(!empty($literasi2)){

			if($literasi2==1){

			  $this->db->where("id in (select tmsiswa_id  from h_ujian where status='Y' and literasi='2') ");


			}else if($literasi2==2){

			  $this->db->where("id in (select tmsiswa_id  from h_ujian where status='N' and literasi='2') ");


			}


	  }
	 
	  


		$this->db->select("*");
        $this->db->from('v_siswa');
        $this->db->where('login',1);
	  
		if($_SESSION['status'] =="madrasah"){
			$this->db->where("tmmadrasah_id",$_SESSION['tmmadrasah_id']);
		}
	
		if(!empty($key)){  $this->db->where("(no_aksi LIKE '%".$key."%' or UPPER(nama) LIKE '%".strtoupper($key)."%')");    }
	    if(!empty($tmmadrasah_id)){  $this->db->where("tmmadrasah_id",$tmmadrasah_id);    }
	    if(!empty($kelas)){  $this->db->where("kelas",$kelas);    }
	    if(!empty($sesi)){  $this->db->where("sesi",$sesi);    }
	
		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 $this->db->order_by("tmmadrasah_id","ASC");
					 $this->db->order_by("kelas","ASC");
					 $this->db->order_by("nama","ASC");
					
		
			 }
      
		
		
		return $this->db->get();
		
	}
	


	
	public function insert(){
		
		$form  			  = $this->security->xss_clean($this->input->get_post("f"));
		$no_aksi          = strtoupper($this->Reff->randomangka(6));
	    
		$this->db->set("password",enkrip($_POST['password']));
		$this->db->set("no_aksi",$no_aksi);
		
		$this->db->insert("tm_siswa",$form);
		                                                
		
		
		     if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
					 return false;
				   
				} else {
						
					$this->db->trans_commit();
					 return true;
					
				}
		
		
		
	}
	
	public function update($id){
		
		$form  			  = $this->security->xss_clean($this->input->get_post("f"));
		  
							
		
		$this->db->set("password",enkrip($_POST['password']));
	
		$this->db->where("id",$id);
		$this->db->update("tm_siswa",$form);
		                                                
		
		
		     if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
					 return false;
				   
				} else {
						
					$this->db->trans_commit();
					 return true;
					
				}
		
		
		
	}
	
}
