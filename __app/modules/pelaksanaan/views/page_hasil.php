<div class="col-md-12">
		
				       <div class="card">
						<div class="card-header card-header-success">
						  <h4 class="card-title">Analisis Hasil  Peserta AKSI 2020 </h4>
						  <p class="card-category">Peserta yang sudah login dan sedang mengerjakan CBT akan tampil pada halaman ini  </p>
						</div>
					  
						 <div class="card-body">
						
					   <div class="row" >
						 
					        <div class="col-md-3">
								
							<select class="form-control"    id="tmmadrasah_id">
																 <option value=""> Pilih Madrasah </option>
																 <?php 
																  $madrasah = $this->db->query(" select * from tm_madrasah order by nama asc")->result();
																 if($_SESSION['status']=="madrasah"){
																	$madrasah = $this->db->query(" select * from tm_madrasah where id='".$_SESSION['tmmadrasah_id']."'")->result();
																 }
															
																 foreach($madrasah as $i=>$r){
																	 
																	?><option value="<?php echo $r->id; ?>" > <?php echo ($r->nama); ?></option><?php  
																	 
																 }
															?>
											</select>
							</div>

							<div class="col-md-2">
								
							<select class="form-control"    id="sesi">
																 <option value=""> Filter  Sesi </option>
																 <?php 
																  $madrasah = $this->db->query(" select * from tm_sesi")->result();
																
																 foreach($madrasah as $i=>$r){
																	 
																	?><option value="<?php echo $r->sesi; ?>" > Sesi <?php echo ($r->sesi); ?></option><?php  
																	 
																 }
															?>
											</select>
							</div>



							<div class="col-md-2">
								
							              <select class="form-control"   id="literasi1" >
											  <option value=""> Filter Literasi I  </option>
											  <option value="1"> Sedang Mengerjakan Literasi I  </option>
											  <option value="2"> Selesai Mengerjakan Literasi I  </option>
																
											</select>
							</div>


							<div class="col-md-2">
								
							              <select class="form-control"   id="literasi2" >
											  <option value=""> Filter Literasi II  </option>
											  <option value="1"> Sedang Mengerjakan Literasi II  </option>
											  <option value="2"> Selesai Mengerjakan Literasi II  </option>
																
											</select>
							</div>



						        		
								<div class="col-md-3">
								
									<div class="input-group">
									<input type="text" class="form-control" id="keyword"  placeholder="Cari  Nomor atau Nama..">
									<div class="input-group-append">
										<button class="btn btn-success btn-sm" type="button">CARI</button>
									</div>
									</div>
								</div>
					   </div>
					   
					   <br>


							<div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover " id="datatableTable" width="99%">
                                    <thead class="bg-blue">
                                        <tr>
                                            <th width="2px">NO</th>
                                            <th>MADRASAH</th>                                            
                                            <th>NO AKSI </th>                              									
                                            <th>NAMA SISWA </th> 
                                            <th>DEVICE  </th> 
                                            <th>ANALISIS LITERASI I</th>											
                                            <th>ANALISIS LITERASI II</th>
                                           
                                            <th> RESET LITERASI I </th>
                                            <th> RESET LITERASI II </th>
											<th> RESET SEMUA  </th>
                                        </tr>
                                    </thead>
                                   
                                    <tbody>
                                        
                                        
                                    </tbody>
                                </table>
                         </div>
                         </div>
                         </div>
                 
 </div> 
      <div id="defaultModal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
	     FORM SISWA
        <button type="button" class="close" data-dismiss="modal">&times;</button>
       
      </div>
      <div class="modal-body" id="loadform">
        <p>Loading...</p>
      </div>
      
    </div>

  </div>
</div>
	
<script type="text/javascript">
  var dataTable = $('#datatableTable').DataTable( {
						"processing": true,
						"language": {
						"processing": '<div class="preloader pl-size-l"><div class="spinner-layer pl-red-grey"><div class="circle-clipper left"> <div class="circle"></div></div><div class="circle-clipper right"><div class="circle"></div></div></div></div>',
						  "oPaginate": {
							"sFirst": "Halaman Pertama",
							"sLast": "Halaman Terakhir",
							 "sNext": "Selanjutnya",
							 "sPrevious": "Sebelumnya"
							 },
						"sInfo": "Total Data :  _TOTAL_ dan ini (_START_ - _END_)",
						 "sInfoEmpty": "Tidak ada data yang di tampilkan",
						   "sZeroRecords": "Data kosong",
						   "sLengthMenu": "&nbsp;&nbsp; Menampilkan   _MENU_ Data"
				    },
					
					"serverSide": true,
					"searching": false,
					"responsive": false,
					"lengthMenu": [[10,25, 50,100,200,300,500,1000, 800000000], [10,25, 50,100,200,300,500,1000,"All"]],
					 "dom": 'Blfrtip',
					 "sPaginationType": "full_numbers",
					"buttons": [
					
                         	
						    {
							extend: 'excelHtml5',
							
							text:'Cetak Excel',
						
							},
							
							
							
							
						
							{
							extend: 'colvis',
							
							text:' Pengaturan Kolom ',
							
						}
					],
					
					"ajax":{
						url :"<?php echo site_url("pelaksanaan/grid_hasil"); ?>", 
						type: "post", 
						"data": function ( data ) {
						
							data.keyword = $("#keyword").val();
							data.tmmadrasah_id = $("#tmmadrasah_id").val();
							data.literasi1 = $("#literasi1").val();
							data.literasi2 = $("#literasi2").val();
							data.sesi      = $("#sesi").val();
					
				
                    }
						
					},
					"rowCallback": function( row, data ) {
						
						
					}
				} );
				
				
				
	
				$(document).on("input","#keyword",function(){
	  
					dataTable.ajax.reload(null,false);	
					
				})	

				$(document).on("change","#tmmadrasah_id,#literasi1,#literasi2,#sesi",function(){
						
						dataTable.ajax.reload(null,false);	
						
					})	

					$(document).off("click",".resetujian").on("click",".resetujian",function(){
	  
	                 var id = $(this).attr("datanya");
	                 var urlnya = $(this).attr("urlnya");
					  alertify.confirm("Ketika Anda mereset ujian,Jawaban siswa akan terhapus dan  Siswa harus melakukan ujian ulang, apakah Anda yakin ?  ",function(){


						  $.post(urlnya,{id:id},function(){

							dataTable.ajax.reload(null,false);	
							alertify.alert("Perubahan berhasil dilakukan");



						  })
					  })
  })

</script>
				