<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>SIPINTAR - SILAHKAN MASUK </title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="<?php echo base_url(); ?>__statics/img/logo.png" rel="icon">
  
  <link href="https://fonts.gstatic.com" rel="preconnect">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>__statics/fa/css/all.min.css">
  <link href="<?php echo base_url(); ?>__statics/admin/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>__statics/admin/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>__statics/admin/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>__statics/admin/assets/vendor/quill/quill.snow.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>__statics/admin/assets/vendor/quill/quill.bubble.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>__statics/admin/assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>__statics/admin/assets/vendor/simple-datatables/style.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>__statics/js/alert/alert.css">
  <!-- Template Main CSS File -->
  <link href="<?php echo base_url(); ?>__statics/admin/assets/css/style.css" rel="stylesheet">
  <script src="<?php echo base_url(); ?>__statics/js/jquery.min.js"></script>
 <script src="<?php echo base_url(); ?>__statics/js/alert/alert.js"></script>

</head>

<body>

  <main>
    <div class="container">

      <section class="section register  flex-column align-items-center justify-content-center py-4">
        <div class="container">
          <div class="row justify-content-center">
            <div class="col-lg-4 col-md-6 d-flex flex-column align-items-center justify-content-center">

            

              <div class="card mb-3">

                <div class="card-body">

                  <div class="pt-4 pb-2">
                    <center><img src="<?php echo base_url(); ?>__statics/img/sipintar.png" alt=""></center>                
                  
                  </div>

                
				  <form class="row g-3 needs-validation" id="loginmadrasah" action="javascript:void(0)" url="<?php echo site_url("login/do_login"); ?>" method="post">


                    <div class="col-12">
                      <label for="yourUsername" class="form-label">Username</label>
                      <div class="input-group has-validation">
                        <span class="input-group-text" id="inputGroupPrepend"><i class="fa fa-user"></i></span>
                        <input type="text" name="username" class="form-control" id="username" required>
                        <div class="invalid-feedback">Please enter your username.</div>
                      </div>
                    </div>  
					<div class="col-12">
                      <label for="yourUsername" class="form-label">Password</label>
                      <div class="input-group has-validation">
                        <span class="input-group-text" id="inputGroupPrepend"><i class="fa fa-lock"></i></span>
                        <input type="password" name="password" class="form-control"  required>
                        <div class="invalid-feedback">Please enter your username.</div>
                      </div>
                    </div>



                    <div class="col-12">
                      <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="remember" value="true" id="rememberMe">
                        <label class="form-check-label" for="rememberMe">Remember me</label>
                      </div>
                    </div>
                    <div class="col-12">
                      <button class="btn btn-primary w-100" type="submit" id="action">Masuk</button>
					  <button  style="display:none" disabled class="btn btn-primary w-100"  type="button" id="loading"><i class="fas fa-spinner fa-spin"></i> Mohon Tunggu .. </button>
                    </div>
                    <div class="col-12">
                      <p class="small mb-0">Butuh Bantuan ? <a href="pages-register.html">Lihat Petunjuk Masuk</a></p>
                    </div>
                  </form>

                </div>
              </div>

              <div class="credits">
               
                Copyright by <a href="https://kemenag.go.id">Inspektorat Jenderal Kementerian Agama</a>
              </div>

            </div>
          </div>
        </div>

      </section>

    </div>
  </main><!-- End #main -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- Vendor JS Files -->
  <script src="<?php echo base_url(); ?>__statics/admin/assets/vendor/apexcharts/apexcharts.min.js"></script>
  <script src="<?php echo base_url(); ?>__statics/admin/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?php echo base_url(); ?>__statics/admin/assets/vendor/chart.js/chart.min.js"></script>
  <script src="<?php echo base_url(); ?>__statics/admin/assets/vendor/echarts/echarts.min.js"></script>
  <script src="<?php echo base_url(); ?>__statics/admin/assets/vendor/quill/quill.min.js"></script>
  <script src="<?php echo base_url(); ?>__statics/admin/assets/vendor/simple-datatables/simple-datatables.js"></script>
  <script src="<?php echo base_url(); ?>__statics/admin/assets/vendor/tinymce/tinymce.min.js"></script>
  <script src="<?php echo base_url(); ?>__statics/admin/assets/vendor/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="<?php echo base_url(); ?>__statics/admin/assets/js/main.js"></script>

</body>


<script type="text/javascript">
var base_url="<?php echo base_url(); ?>";


function sukses(param){
	   let timerInterval;
	   Swal.fire({
		 type: 'success',
		 title: 'Proses Authentication Berhasil ',
		 showConfirmButton: false,
		  html: 'Dalam <strong></strong> detik<br>Anda akan dialihkan kedalam aplikasi',

		 timer: 1000,
		 onBeforeOpen: () => {
		   Swal.showLoading();
		   timerInterval = setInterval(() => {
			 Swal.getContent().querySelector('strong')
			   .textContent = Swal.getTimerLeft()
		   }, 100)
		 },
		 onClose: () => {
		   clearInterval(timerInterval)
		 }
	   }).then((result) => {
		 if (
		   /* Read more about handling dismissals below */
		   result.dismiss === Swal.DismissReason.timer
		   
		 ) {
		
		  location.href = base_url+param;
		 }
	   })
}

function gagal(param){
	   let timerInterval;
	   Swal.fire({
		 type: 'warning',
		 title: 'Proses Authentication Gagal ',
		 showConfirmButton: true,
		  html: param

	   })
}


   $(document).on('submit', 'form#loginmadrasah', function (event, messages) {
	event.preventDefault();
	  var form   = $(this);
	  var urlnya = $(this).attr("url");
   
	 
	
			 $("#loading").show();
			 $("#action").hide();
			  $.ajax({
				   type: "POST",
				   url: urlnya,
				   data: form.serialize(),
				   success: function (response, status, xhr) {
					var ct = xhr.getResponseHeader("content-type") || "";
					   if (ct == "application/json") {
					   
						gagal(response.message);
						$("#loading").hide();
						$("#action").show();
						   
						   
					   } else {
						 
						  
						  sukses(response);
						  
						   
						   
							
						   
						 
					   }
					   
					  
					   
				   }
			   });
			 
	   return false;
   });
   
   
   
</script>
</html>