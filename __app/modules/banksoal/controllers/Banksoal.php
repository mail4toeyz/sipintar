<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banksoal extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		 if(!$this->session->userdata("aksi_id")){
			    
				echo $this->Reff->sessionhabis();
				exit();
		  }
		  $this->load->model('M_soal','m');
		  $this->load->helper('exportpdf_helper');  
	  }
	  
   function _template($data)
	{
	  $this->load->view('admin/page_header',$data);	
	}
		
	public function buatsoal()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		
		 $data['title']   = "Data  Soal ";
	     if(!empty($ajax)){
					    
			 $this->load->view('page',$data);
		
		 }else{
			 
		     $data['konten'] = "page";
			 
			 $this->_template($data);
		 }
	

	}

	public function analisissoal()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
         $id            = $this->input->get_post("id",true);	
		
		 $data['title']   = "Analisis Butir Soal ";
		 $data['id']	  = $id;
		 if($id !=5){
	     if(!empty($ajax)){
					    
			 $this->load->view('analisis',$data);
		
		 }else{
			 
		     $data['konten'] = "analisis";
			 
			 $this->_template($data);
		 }
		}else{

			if(!empty($ajax)){
					    
				$this->load->view('analisis_moderasi',$data);
		   
			}else{
				
				$data['konten'] = "analisis_moderasi";
				
				$this->_template($data);
			}
		}
	

	}

	public function cetak(){
	    
			
		$data['paket']      = $this->db->query("select * from   ref_ujian where id='".$_GET['id']."'")->row();
		
			
		$pdf_filename = "Soal ".str_replace(",","_",$data['paket']->nama).".pdf";	 
	
		$user_info = $this->load->view('cetak', $data, true);
	
		 $output = $user_info;
		
	  $output;
	
		generate_pdf($output, $pdf_filename,TRUE);
		
	
	
	}
	
	 public function grid(){
		
		  $iTotalRecords = $this->m->grid(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->grid(true)->result_array();
		   
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
				
			 $jml_soal = $this->db->query("select count(id) as jml from tr_soal where tmujian_id='".$val['id']."'")->row();
			 $jadwal   = $this->db->get_where("tm_jadwal",array("id"=>$val['kategori']))->row();

			 $waktu   = $jadwal->hari."<br><span class='badge badge-sm bg-gradient-success'>". $jadwal->waktu."</span>";

			
			 $checked ="";
			if($val['status']==1){

				$checked ="checked";
			}
				$no = $i++;
				$records["data"][] = array(
					$no,
					$waktu,
					$val['nama'],
					$jml_soal->jml,
					$val['waktu']." Menit",
					'<input type="checkbox" tmujian_id="'.$val['id'].'" class="aktivasi" '.$checked.'>',
					
					'
					<div class="btn-group">
					<a href="#"  class="btn btn-primary btn-sm buatujian"  tmujian_id="'.$val['id'].'"><i class="fa fa-pencil"></i> Buat Soal </a>
					
					<a class="btn btn-success btn-sm"   href="'.site_url("teachercbt/simulasi?start=".base64_encode($val['id'])."").'" style="color:white" title="Simulasi Ujian"><i class="fa fa-retweet"></i> Simulasi </a>
					
					 
					  <button type="button" class="btn btn-warning btn-sm ubahmodal"  datanya="'.$val['id'].'" urlnya="'.site_url("banksoal/form").'" target="#loadform" data-toggle="modal" data-target="#defaultModal">
					  <i class="fa fa-pencil"></i> Edit
					  </button>
					  <button type="button" class="btn btn-warning btn-sm hapus"  datanya="'.$val['id'].'" urlnya="'.site_url("banksoal/truncate").'" >
					  <i class="fa fa-trash"></i>  Truncate
		              </button>
					  <button type="button" class="btn btn-danger btn-sm hapus"  datanya="'.$val['id'].'" urlnya="'.site_url("banksoal/hapus").'" >
					  <i class="fa fa-trash"></i>  
		              </button>
					  </div>
					'
					
				  
				    

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}


	public function aktivasi(){

		$this->db->where("id",$_POST['tmujian_id']);
		$this->db->set("status",$_POST['status']);
		$this->db->update("tm_ujian");

		echo "sukses";
	}

	public function statussoal(){
		$this->db->where("id",$_POST['id']);
		$this->db->set("status",$_POST['status']);
		$this->db->update("tm_ujian");

		echo "sukses";


	}

	public function hapus(){

		$this->db->where("id",$_POST['id']);
		$this->db->delete("tm_ujian");

		$this->db->where("tmujian_id",$_POST['id']);
		$this->db->delete("tr_soal");
		echo "sukses";
   

	}
	
	public function truncate(){

           $this->db->where("tmujian_id",$_POST['id']);
		   $this->db->delete("tr_soal");
		   echo "sukses";

	}

	public function analisis(){
		$data['id'] = $_POST['id'];
		 
		$this->load->view("analisis",$data); 



	}

	public function form(){
		
		$id = $this->input->get_post("id");
		$data = array();
		   if(!empty($id)){
			   
			  $data['data']  = $this->Reff->get_where("tm_ujian",array("id"=>$id));
			   
		   }
		$this->load->view("form",$data);
		
	}
	
	
	
	public function save(){
     
        $this->form_validation->set_message('required', '{field} Wajib diisi. ');
         $this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , silahkan masukan NISN lain.');
			    $this->form_validation->set_message('min_length', '%s: Minimal  %s digit');
         $this->form_validation->set_message('max_length', '%s: Maksimal  %s digit');
    
		 
			
				$config = array(
				    
				   
				    array('field' => 'f[kategori]', 'label' => 'kategori  ', 'rules' => 'trim|required'),
				   
				   
					
				);
				
				$this->form_validation->set_rules($config);	
        
        if ($this->form_validation->run() == true) {
			  
			    $id = $this->input->get_post("id",true);
			    $f  = ($this->input->get_post("f",true));
				
				
				 
				
							 if(empty($id)){
								  
								   $pengaturan_id  = $this->Reff->set();
								   
								   $this->db->set("pengaturan_id",$pengaturan_id);
								   $this->db->insert("tm_ujian",$f);
								   echo "Data Berhasil disimpan";	
								 
								
								
							 }else{
								
								$this->db->where("id",$id);
								$this->db->update("tm_ujian",$f);
								echo "Data Berhasil disimpan";	
								 
							 }
							 
						
							
						
		
		
							
			     
			    
												 
	     } else {
	         
           
                header('Content-Type: application/json');
                echo json_encode(array('error' => true, 'message' => validation_errors()));
            
        }

		
	
	
	}


	 
}
