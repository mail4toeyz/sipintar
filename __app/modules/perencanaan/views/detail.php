<main id="main" class="main">

<div class="pagetitle">
  <h1><?php echo $title; ?></h1>
  <nav>
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Approval</a></li>
      
      <li class="breadcrumb-item active"><?php echo $title; ?></li>
    </ol>
  </nav>
</div><!-- End Page Title -->

<section class="section">
  <div class="row">
    <div class="col-lg-12">

      <div class="card">
        <div class="card-body">
          <h5 class="card-title">Approval <?php echo $title; ?></h5>

          <!-- Default Tabs -->
          <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item" role="presentation">
              <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">Kegiatan  </button>
             
            </li>
              <li class="nav-item" role="presentation">
                  <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="false">Approval  </button>
                </li>
              
          
          </ul>
          <div class="tab-content pt-2" id="myTabContent">
            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
               <table class="table">
                   <tr>
                       <td> Jenis Kegiatan </td>
                       <td> <?php echo  $this->Reff->get_kondisi(array("id"=>$kegiatan->jenis),"tm_jenis","nama"); ?> </td>

                   </tr>

                   <tr>
                       <td> Judul Kegiatan </td>
                       <td> <?php echo  $kegiatan->judul; ?> </td>

                   </tr>

                   <tr>
                       <td> Provinsi </td>
                       <td> <?php echo  $this->Reff->get_kondisi(array("id"=>$kegiatan->provinsi_id),"provinsi","nama"); ?> </td>

                   </tr>

                   <tr>
                       <td> Kabupaten/Kota </td>
                       <td> <?php echo  $this->Reff->get_kondisi(array("id"=>$kegiatan->kota_id),"kota","nama"); ?> </td>

                   </tr>
                   <tr>
                       <td> Satuan Kerja  </td>
                       <td> <?php echo  $this->Reff->get_kondisi(array("id"=>$kegiatan->satker_id),"satker","nama"); ?> </td>

                   </tr>
                   <tr>
                       <td> Tanggal Mulai Kegiatan  </td>
                       <td> <?php echo  $this->Reff->formattanggalstring($kegiatan->tgl_mulai); ?> </td>

                   </tr>
                   <tr>
                       <td> Tanggal Selesai Kegiatan  </td>
                       <td> <?php echo  $this->Reff->formattanggalstring($kegiatan->tgl_selesai); ?> </td>

                   </tr>
               </table>
            </div>



                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">

                <table class="table">
                   <thead>
                      <tr>
                       <th> Nama </th>
                       <th> NIP </th>
                       <th> Jabatan </th>
                       <th> Jumlah Hari </th>
                     </tr>

                   </thead>
                   <tbody>
                     <tr>
                       <td> <?php echo $this->Reff->get_kondisi(array("id"=>$kegiatan->penanggung_jawab),"sekertariat","nama"); ?></td>
                       <td> <?php echo $this->Reff->get_kondisi(array("id"=>$kegiatan->penanggung_jawab),"sekertariat","nip"); ?> </td>
                       <td> Penanggung Jawab </td>
                       <td>
                        <?php echo $this->Reff->get_kondisi(array("kegiatan_id"=>$kegiatan->id,"jabatan"=>1,"pejabat_id"=>$kegiatan->penanggung_jawab),"tr_kegiatanJabatan","waktu");
                        ?> Hari 
                         
                       
                    
                    </td>
                     </tr>

                     <tr>
                     <td> <?php echo $this->Reff->get_kondisi(array("id"=>$kegiatan->pengendali_mutu),"sekertariat","nama"); ?></td>
                       <td> <?php echo $this->Reff->get_kondisi(array("id"=>$kegiatan->pengendali_mutu),"sekertariat","nip"); ?> </td>
                       <td> Pengendali Mutu </td>
                       <td>

                       <?php echo $this->Reff->get_kondisi(array("kegiatan_id"=>$kegiatan->id,"jabatan"=>2,"pejabat_id"=>$kegiatan->pengendali_mutu),"tr_kegiatanJabatan","waktu");
                        ?> Hari 
                       </td>
                     </tr>

                     <tr>
                     <td> <?php echo $this->Reff->get_kondisi(array("id"=>$kegiatan->pengendali_teknis),"sekertariat","nama"); ?></td>
                       <td> <?php echo $this->Reff->get_kondisi(array("id"=>$kegiatan->pengendali_teknis),"sekertariat","nip"); ?> </td>
                       <td> Pengendali Teknis </td>
                       <td>

                       <?php echo $this->Reff->get_kondisi(array("kegiatan_id"=>$kegiatan->id,"jabatan"=>3,"pejabat_id"=>$kegiatan->pengendali_teknis),"tr_kegiatanJabatan","waktu");
                        ?> Hari 

                       </td>
                     </tr>

                     <tr>
                     <td> <?php echo $this->Reff->get_kondisi(array("id"=>$kegiatan->ketua),"auditor","nama"); ?></td>
                       <td> <?php echo $this->Reff->get_kondisi(array("id"=>$kegiatan->ketua),"auditor","nip"); ?> </td>
                       <td> Ketua TIM </td>
                       <td>

                         
                       <?php echo $this->Reff->get_kondisi(array("kegiatan_id"=>$kegiatan->id,"jabatan"=>4,"pejabat_id"=>$kegiatan->ketua),"tr_kegiatanJabatan","waktu");
                        ?> Hari 

                       </td>
                     </tr>
                     <?php 
                       $anggota = json_decode($kegiatan->anggota,true);
                        foreach($anggota as $r){
                          ?>
                          <tr>
                            <td><?php echo $this->Reff->get_kondisi(array("id"=>$r),"auditor","nama"); ?> </td>
                            <td><?php echo $this->Reff->get_kondisi(array("id"=>$r),"auditor","nip"); ?></td>
                            <td> Anggota </td>
                            <td>


                            <?php echo $this->Reff->get_kondisi(array("kegiatan_id"=>$kegiatan->id,"jabatan"=>5,"pejabat_id"=>$r),"tr_kegiatanJabatan","waktu");
                        ?> Hari 


                            </td>
                          </tr>
                     <?php 


                        }
                        ?>
                     
                   </tbody>
                 </table>

                 <h5><b>Berdasarkan kegiatan diatas, Anda dapat menyetujui atau menolak kegiatan tersebut </b></h5>

                 <select class="verifikasi form-control" kegiatan_id="<?php echo $kegiatan->id; ?>">
                  <option value="">- Lakukan Approval -</option> 
                  <option value="1"> Setujui </option> 
                  <option value="2"> Tolak</option> 
                 </select>
                 <hr>
                 <center><a href="<?php echo site_url('perencanaan'); ?>" class="btn btn-primary btn-sm"> Simpan Hasil Approval </a></center>
                 
                </div>



           
          </div><!-- End Default Tabs -->

        </div>
      </div>

    

    </div>

  </div>
</section>

</main><!-- End #main -->

<script>
 $(document).off("change",".verifikasi").on("change",".verifikasi",function(){
        var kegiatan_id  = $(this).attr("kegiatan_id");
        var status  = $(this).val();
          $.post("<?php echo site_url("perencanaan/verifikasi"); ?>",{kegiatan_id:kegiatan_id,status:status},function(data){

          
          })
      

      });
</script>
