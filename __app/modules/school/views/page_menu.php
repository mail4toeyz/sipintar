
</style>
 <?php 
   $uri = $this->uri->segment(1);
   $uri2 = $this->uri->segment(2);
   $uri4 = ($this->uri->segment(4));
  
   
     
  ?>

 <div class="sidebar-wrapper" style="color:black">
        <ul class="nav">
          <li class="nav-item <?php echo ($uri=="admin") ? "active" :""; ?> ">
            <a class="nav-link menuajax" style="color:black" href="<?php echo site_url("school"); ?>" title="Dashboard Madrasah ">
              <i class="fa fa-dashboard"></i>
              <p>Dashboard  </p>
            </a>
          </li>
		  
		  
		  
		
		
		     <li class="nav-item <?php echo ($uri=="schoolmadrasah" && $uri2=="") ? "active":""; ?> ">
            <a class="nav-link menuajax" style="color:black" href="<?php echo site_url("schoolmadrasah"); ?>" title="Profile Madrasah ">
               <i class="fa fa-bank"></i>
              <p>Profile Madrasah</p>
            </a>
          </li>
		  
		
		  
		 

          <li class="nav-item <?php echo ($uri=="datasiswa" && $uri2=="") ? "active":""; ?> ">
            <a class="nav-link menuajax" style="color:black" href="<?php echo site_url("datasiswa"); ?>" title="Data Siswa ">
            <i class="fa fa-users" aria-hidden="true"></i>
              <p>Data Siswa </p>
            </a>
          </li>    

          <li class="nav-item <?php echo ($uri=="datasiswa" && $uri2=="sesi") ? "active":""; ?> ">
            <a class="nav-link menuajax" style="color:black" href="<?php echo site_url("datasiswa/sesi"); ?>" title="Pemilihan Sesi ">
            <i class="fa fa-check-square-o" aria-hidden="true"></i>


              <p>Pemilihan Sesi  </p>
            </a>
          </li>                 
		  
          <li class="nav-item <?php echo ($uri=="datasiswa" && $uri2=="infosesi") ? "active":""; ?> " title="Informasi Sesi ">
            <a class="nav-link menuajax" style="color:black" href="<?php echo site_url("datasiswa/infosesi"); ?>">
            <i class="fa fa-info" aria-hidden="true"></i>

              <p> Informasi Sesi  dan Kuota </p>
            </a>
          </li>
		   
		    <li class="nav-item <?php echo ($uri=="pelaksanaan") ? "active":""; ?> ">
                        <a href="javascript:void(0);" style="color:black" class="menu-toggle nav-link">
                            <i class="fa fa-desktop"></i>
                            <span>Monitor Pelaksanaan     </span> 
                            
                        </a>
						
							<ul class="ml-menu" style="<?php echo ($uri=="pelaksanaan" or $uri=="statistik") ? "":"display:none"; ?>">
							   <li>
                      <a href="<?php echo site_url("pelaksanaan/statistik"); ?>" style="color:black" class="nav-link menuajax" title="Statistik Pelaksanaan"><i class="fa fa-angle-right"></i>Statistik Pelaksanaan  </a>
                 </li>
							 
                 <li>
                      <a href="<?php echo site_url("pelaksanaan/datapeserta"); ?>" style="color:black" class="nav-link menuajax" title="Data Peserta"><i class="fa fa-angle-right"></i>Data Peserta  </a>
                 </li>
							  
                 <li>
                      <a href="<?php echo site_url("pelaksanaan/analisispeserta"); ?>" style="color:black" class="nav-link menuajax" title="Analisis Peserta"><i class="fa fa-angle-right"></i> Analisis Hasil Peserta  </a>
                 </li>
							  

								
							  </ul>
                            
         </li>
   


		  
		  
             <li class="nav-item <?php echo ($uri=="schoolmadrasah" && $uri2=="aktifitas") ? "active":""; ?> ">
              <a class="nav-link menuajax" style="color:black" href="<?php echo site_url("schoolmadrasah/aktifitas"); ?>">
              <i class="fa fa-clock-o"></i>
              <p>Monitoring Aktifitas </p>
            </a>
           </li> 
		
		  
          <li class="nav-item ">
            <a class="nav-link" style="color:black" href="javascript:void(0)" id="keluar">
              <i class="fa fa-sign-out"></i>
              <p>Logout </p>
            </a>
          </li>
         
          
         
        </ul>
      </div>
	  
