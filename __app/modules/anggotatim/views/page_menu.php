<aside id="sidebar" class="sidebar">

    <ul class="sidebar-nav" id="sidebar-nav">

      <li class="nav-item">
        <a class="nav-link " href="<?php echo site_url('auditor'); ?>">
          <i class="bi bi-grid"></i>
          <span>Dashboard</span>
        </a>
      </li><!-- End Dashboard Nav -->

      <li class="nav-heading">Jadwal Kegiatan</li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="<?php echo site_url("dokumen"); ?>">
          <i class="bi bi-plus-circle"></i>
          <span> Permintaan Dokumen </span>
        </a>
      </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="<?php echo site_url("vicon"); ?>">
        <i class="bi bi-menu-button-wide"></i>
          <span> Video Conference</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link collapsed" href="<?php echo site_url("vicon"); ?>">
        <i class="bi bi-file-check"></i>
          <span> Kertas Kerja </span>
        </a>
      </li>

      <li class="nav-heading">Laporan </li>

      <li class="nav-item">
        <a class="nav-link collapsed" href="<?php echo site_url("auditor/laporan"); ?>">
        <i class="fa fa-file"></i>
          <span> Laporan Hasil Audit</span>
        </a>
      </li>


      <li class="nav-heading">e-Palap  </li>
      <li class="nav-item">
      <a class="nav-link collapsed" href="<?php echo site_url("auditor/laporan"); ?>">
      <i class="bi bi-send-plus-fill"></i>
        <span> Penugasan </span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link collapsed" href="<?php echo site_url("auditor/laporan"); ?>">
      <i class="bi bi-car-front"></i>
        <span> Pengeluaran ril </span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link collapsed" href="<?php echo site_url("auditor/laporan"); ?>">
      <i class="bi bi-cash-coin"></i>
        <span>Saldo  </span>
      </a>
    </li>

      

      <li class="nav-heading">Pengaturan </li>
      <li class="nav-item">
        <a class="nav-link collapsed" href="#">
          <i class="bi bi-dash-circle"></i>
          <span>Profile Anda</span>
        </a>
      </li><!-- End Error 404 Page Nav -->

      <li class="nav-item">
        <a class="nav-link collapsed" href="<?php echo site_url("login"); ?>">
          <i class="bi bi-file-earmark"></i>
          <span>Logout</span>
        </a>
      </li><!-- End Blank Page Nav -->

    </ul>

  </aside><!-- End Sidebar-->