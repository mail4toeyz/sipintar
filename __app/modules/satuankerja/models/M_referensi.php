<?php

class M_referensi extends CI_Model {
 
    public function __construct() {
        parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
    }

    
	public function grid($paging){
      
		
	    $keyword      = $this->input->post("keyword",true);
	    $kategori = $this->input->post("kategori",true);
	 
	 
		$this->db->select("*");
        $this->db->from('satker');
		$this->db->where('kategori',$kategori);
        
	    if(!empty($kategori)){ $this->db->where('kategori',$kategori);      }
	    if(!empty($keyword)){  $this->db->where("UPPER(nama) LIKE '%".strtoupper($keyword)."%'");    }
		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 
					 $this->db->order_by("nama","ASC");
		
			 }
      
		
		
		return $this->db->get();
		
	}
	
	
}
