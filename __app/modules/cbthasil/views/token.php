 <?php 
   $literasi1  = $this->db->query("select tgl_akhir from h_ujian where tmsiswa_id='".$_SESSION['siswaID']."' and tmujian_id='".$data->literasi1."' and status='N'")->row();
   $literasi2  = $this->db->query("select tgl_akhir from h_ujian where tmsiswa_id='".$_SESSION['siswaID']."' and tmujian_id='".$data->literasi2."' and status='N'")->row();
   
  

   ?>
<div class="callout callout-danger">
    <h4>Petunjuk !</h4>
	<p>
	 <ol start="1">
	   <li> Dalam Asesmen Kompetensi Siswa 2020, ada 2 literasi yang harus Anda kerjakan</li>
	   <li> Silahkan kerjakan terlebih dahulu literasi pertama</li>
	   <li> Setelah literasi pertama selesai dikerjakan, Anda akan dialihkan untuk mengerjakan literasi kedua </li>
	   <li> Mohon untuk mengisi semua pertanyaan dengan bersungguh-sungguh </li>
	 </ol>
	
	</p>
           
   </div>
<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title">DETAIL PESERTA ASESMEN KOMPETENSI SISWA INDONESIA 2020  </h3>
    </div>
    <div class="box-body">
        
        <div class="row">
            <div class="col-sm-4">
                <table class="table table-bordered">
                    <tr>
                        <th>Nama Siswa </th>
                        <td><?php echo $data->nama; ?></td>
                    </tr>
					
					<tr>
                        <th>Tanggal Lahir  </th>
						<td><?php echo $data->tgl_lahir; ?></td>
                    </tr>

				
					
				
                </table>

               <center> <video autoplay="true" id="video-webcam" style="width: 50%;height:auto">
                Browsermu tidak mendukung, upgrade yaa !
            </video> </center>
            </div>

            
            <div class="col-sm-8">
                <div class="box box-solid">
                    <div class="box-body pb-0">

					  <div class="callout callout-success" style="font-size:18px">
					   <h4> Selamat datang didalam Asesmen Kompetensi Siswa Indonesia 2020</h4> <br>
						Dalam AKSI 2020, Anda  harus mengerjakan 2 literasi yang dirandom,
						Anda dapat menyelesaikan literasi pertama dengan klik tombol dibawah ini,
						setelah Anda selesai dengan Literasi pertama, Silahkan selesaikan literasi ke dua,
						literasi kedua hanya dapat dikerjakan setelah Anda menyelesaikan Literasi pertama 

					  </div>
					  <input autocomplete="off" id="token" placeholder="Token" value="membaca" type="hidden" class="input-sm form-control">
                      
                     
                       <?php 
                         if(is_null($literasi1)){
                             ?>

                            <center>
                                <a  href='<?php echo base_url("cbt/literasi?start=".base64_encode($data->literasi1)."&hash".base64_encode($data->literasi1)."&literasi=1"); ?>' class="btn btn-success btn-lg mb-4">
                                                        <i class="fa fa-edit"></i> MULAI KERJAKAN  LITERASI PERTAMA
                                </a>
                               
                               
                            </center>
                            
                            <?php 

                         }else{

                            if(is_null($literasi2)){
                            

                            ?>
                            <div class="callout callout-warning">
					         <b style="font-size:16px">Anda sudah menyelesaikan  literasi pertama pada <?php echo $this->Reff->formattimestamp($literasi1->tgl_akhir); ?>, <br>
                             silahkan klik tombol dibawah ini untuk mengerjakan  literasi kedua </b>  <br> 
                             <hr>
                            

					       </div> 

                           <center>  <a  href='<?php echo base_url("cbt/literasi?start=".base64_encode($data->literasi2)."&hash".base64_encode($data->literasi2)."&literasi=2"); ?>' class="btn btn-success btn-lg mb-4">
                                       <i class="fa fa-edit"></i>  MULAI KERJAKAN  LITERASI KEDUA
                                </a>
                                </center>

                           <center>
                               
                            </center>

                        <?php 
                         }else{

                            ?>
                            <div class="callout callout-warning">
                            <h4>Terimakasih !</h4> <br>
					         <b style="font-size:16px"> 
                             Anda sudah menyelesaikan  literasi pertama pada <?php echo $this->Reff->formattimestamp($literasi1->tgl_akhir); ?> <br>
                             dan menyelesaikan  literasi kedua pada <?php echo $this->Reff->formattimestamp($literasi2->tgl_akhir); ?> <br>
                             silahkan logout aplikasi  </b>  <br> 
                             <hr>
                            

					       </div> 

                          
                                <br>
                                <a href='<?php echo site_url(); ?>' class="btn btn-success btn-lg mb-4">
                                     <i class="fa fa-power-off"></i> SILAHKAN LOGOUT APLIKASI 
                                </a>
                            
                             

                           <?php 




                         }
                         ?>


                      <?php 



                         }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    // seleksi elemen video
    var video = document.querySelector("#video-webcam");

    // minta izin user
    navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia || navigator.oGetUserMedia;

    // jika user memberikan izin
    if (navigator.getUserMedia) {
        // jalankan fungsi handleVideo, dan videoError jika izin ditolak
        navigator.getUserMedia({ video: true }, handleVideo, videoError);
    }

    // fungsi ini akan dieksekusi jika  izin telah diberikan
    function handleVideo(stream) {
        video.srcObject = stream;
    }

    // fungsi ini akan dieksekusi kalau user menolak izin
    function videoError(e) {
        // do something
        alert("Izinkan mengakses kamera aksi 2020 !")
    }
</script>

<script type="text/javascript">
var base_url = "<?php echo base_url(); ?>";
$(document).ready(function () {
    ajaxcsrf();

    $('#btncek').on('click', function () {
        var token = $('#token').val();
		var literasi = "";
		  
        var idUjian = $(this).data('id');
        var id_ujian = $(this).attr('id_ujian');
        var key = id_ujian;
            var hash = id_ujian;
        location.href = base_url + 'cbtguru/literasi?start=' + key +'&hash='+hash;

       
    });

    var time = $('.countdown');
    if (time.length) {
        countdown(time.data('time'));
    }
});

</script>