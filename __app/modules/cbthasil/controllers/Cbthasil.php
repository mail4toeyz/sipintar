<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cbthasil extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		  
		  $this->load->helper('my');
		  $this->load->model('M_cbt');
		  $this->load->helper('exportpdf_helper');  
		$this->Reff->zona_waktu();
	  }

	
	

	public function literasi()
	{
		
		$literasi = $this->input->get('literasi', true);
		$key = $this->input->get('start', true);
		$hash = $this->input->get('hash', true);
		$tmsiswa_id = $this->input->get('tmsiswa_id', true);
	     $id  = base64_decode($key); 
		   
		
		$ujian 		= $this->M_cbt->getUjianById($id);
		$soal 		= $this->M_cbt->getSoal($id);
		
		

		$siswa     = $this->db->query("select id,nama,madrasah from  v_siswa where id='".$tmsiswa_id."'")->row();
		
		

		$h_ujian 	= $this->M_cbt->HslUjian($id,$siswa->id);
	
		$cek_sudah_ikut = $h_ujian->num_rows(); 

		if ($cek_sudah_ikut < 1) {
			$soal_urut_ok 	= array();
			$i = 0;
			foreach ($soal as $s) {
				$soal_per = new stdClass();
				$soal_per->id_soal 		= $s->id_soal;
				$soal_per->soal 		= $s->soal;
				$soal_per->file 		= $s->file;				
				$soal_per->opsi_a 		= $s->opsi_a;
				$soal_per->opsi_b 		= $s->opsi_b;
				$soal_per->opsi_c 		= $s->opsi_c;
				$soal_per->opsi_d 		= $s->opsi_d;
				$soal_per->opsi_e 		= $s->opsi_e;
				$soal_per->jawaban 		= $s->jawaban;
				$soal_urut_ok[$i] 		= $soal_per;
				$i++;
			}
			$soal_urut_ok 	= $soal_urut_ok;
			$list_id_soal	= "";
			$list_jw_soal 	= "";
			if (!empty($soal)) {
				foreach ($soal as $d) {
					$list_id_soal .= $d->id_soal.",";
					$list_jw_soal .= $d->id_soal."::,";
				}
			}
			$list_id_soal 	= substr($list_id_soal, 0, -1);
			$list_jw_soal 	= substr($list_jw_soal, 0, -1);
			$waktu_selesai 	= date('Y-m-d H:i:s', strtotime("+{$ujian->waktu} minute"));
			$time_mulai		= date('Y-m-d H:i:s');

			//print_r($list_jw_soal); exit();

			$input = [
				'tmujian_id' 		=> $id,
				'literasi'  	=> $literasi,
				'tmsiswa_id'	=> $siswa->id,
				'list_soal'		=> $list_id_soal,
				'list_jawaban' 	=> $list_jw_soal,
				'jml_benar'		=> 0,
				'nilai'			=> 0,
				'nilai_bobot'	=> 0,
				'tgl_mulai'		=> $time_mulai,
				'tgl_selesai'	=> $waktu_selesai,
				'status'		=> 'Y'
			];
			
			
			$this->db->insert('h_ujian', $input);

			redirect('cbt/literasi?start='.base64_encode($id)."&literasi=".$literasi."&hash=".$hash, 'location', 301);
		}
		
		$q_soal = $h_ujian->row();
		
		$urut_soal 		= explode(",", $q_soal->list_jawaban);
		$soal_urut_ok	= array();
		for ($i = 0; $i < sizeof($urut_soal); $i++) {
			$pc_urut_soal	= explode(":",$urut_soal[$i]);
			$pc_urut_soal1 	= empty($pc_urut_soal[1]) ? "''" : "'{$pc_urut_soal[1]}'";
			$ambil_soal 	= $this->M_cbt->ambilSoal($pc_urut_soal1, $pc_urut_soal[0]);
			$soal_urut_ok[] = $ambil_soal; 
		}

		$detail_tes = $q_soal;
		$soal_urut_ok = $soal_urut_ok;

		$pc_list_jawaban = explode(",", $detail_tes->list_jawaban);
		$arr_jawab = array();
		
		foreach ($pc_list_jawaban as $v) {
			$pc_v 	= explode(":", $v);
			$idx 	= $pc_v[0];
			$val 	= $pc_v[1];
			$rg 	= $pc_v[2];

			$arr_jawab[$idx] = array("j"=>$val,"r"=>$rg);
		}








		$arr_opsi = array("a","b","c","d","e");
		$html = '';
			
		$no = 1;
		//print_r($soal_urut_ok); exit();
		if (!empty($soal)) {
			foreach ($soal as $s) {
				$path = $s->folder."/";
				$jwbn       = isset($h_jawaban) ? $h_jawaban :"";
				$vrg = "N";
				$jwbn = $arr_jawab[$s->id_soal]["j"] == "" ? "" : $arr_jawab[$s->id_soal]["j"];
				$ket  = $arr_jawab[$s->id_soal]["r"] == "" ? "" : $arr_jawab[$s->id_soal]["r"];
				
				$html .= '<input type="hidden" name="id_soal_'.$no.'" value="'.$s->id_soal.'">';
				$html .= '<input type="hidden" name="jenis_'.$no.'" value="'.$s->jenis.'">';
				$html .= '<input type="hidden" name="jwbn'.$no.'" value="'.$jwbn.'">';
				$html .= '<input type="hidden" name="rg_'.$no.'" id="rg_'.$no.'" value="'.$vrg.'">';
				$html .= '<div class="step" id="widget_'.$no.'">';

				$html .= '<div class="papers"><div class="text-center"></div>'.$s->soal.'</div>';
				
				if($s->jenis==1){
					$html .= '<hr><div class="funkyradio">';
						for ($j = 0; $j < 5; $j++) {
							$opsi 			= "opsi_".$arr_opsi[$j];
							$file 			= "file_".$arr_opsi[$j];
							
							$pilihan_opsi 	= !empty($s->$opsi) ? $s->$opsi : "";
							if($pilihan_opsi !=""){
								$checked 		= "";
							
							 if($jwbn==strtoupper($arr_opsi[$j])){
								$checked = "checked";
							

							 }
							$html .= '<div class="funkyradio-success" >
								<input type="radio" id="opsi_'.strtolower($arr_opsi[$j]).'_'.$s->id_soal.'" disabled  name="jawaban_'.$no.'" class="ayolah" nomor="'.$no.'" value="'.strtoupper($arr_opsi[$j]).'" '.$checked.'> <label for="opsi_'.strtolower($arr_opsi[$j]).'_'.$s->id_soal.'"><div class="huruf_opsi">'.$arr_opsi[$j].'</div> <p>'.$pilihan_opsi.'</p></label></div>';
							}
						}
						
						
						
				}else if($s->jenis==2){
					$html .= '<hr><div class="funkyradio">';
					for ($j = 0; $j < 2; $j++) {
							$opsi 			= "opsi_".$arr_opsi[$j];
							$file 			= "file_".$arr_opsi[$j];
							$checked 		= "";
							$pilihan_opsi 	= !empty($s->$opsi) ? $s->$opsi : "";
							if($jwbn==$arr_opsi[$j]){
								$checked 		= "checked";

							 }
							

					  $html .= '<div class="funkyradio-success" >
								<input type="radio"  class="ayolah" nomor="'.$no.'"  disabled id="opsi_'.strtolower($arr_opsi[$j]).'_'.$s->id_soal.'" name="jawaban_'.$no.'" value="'.strtoupper($arr_opsi[$j]).'" '.$checked.'> <label for="opsi_'.strtolower($arr_opsi[$j]).'_'.$s->id_soal.'"><div class="huruf_opsi">'.$arr_opsi[$j].'</div> <p>'.$pilihan_opsi.'</p></label></div>';
					}
					
					
				}else if($s->jenis==4){

					if($jwbn !=""){
						
						$jawaban44 = explode("#",$jwbn);

					}

					
					$html .= '<hr><div class="row">';
					$opsi_a   = explode("[split]",$s->opsi_a);
					  
					  
					$opsi_b   = explode("[split]",$s->opsi_b);
					$opsi_c   = explode("[split]",$s->opsi_c);
					$opsi_d   = explode("[split]",$s->opsi_d);
					$opsi_e   = explode("[split]",$s->opsi_e);
					$opsi_a_per = isset($opsi_a[0]) ? $opsi_a[0]:"-";
					$opsi_b_per = isset($opsi_b[0]) ? $opsi_b[0]:"-";
					$opsi_c_per = isset($opsi_c[0]) ? $opsi_c[0]:"-";
					$opsi_d_per = isset($opsi_d[0]) ? $opsi_d[0]:"-";
					$opsi_e_per = isset($opsi_e[0]) ? $opsi_e[0]:"-";

					$opsi_a_j = isset($opsi_a[1]) ? $opsi_a[1]:"-";
					$opsi_b_j = isset($opsi_b[1]) ? $opsi_b[1]:"-";
					$opsi_c_j = isset($opsi_c[1]) ? $opsi_c[1]:"-";
					$opsi_d_j = isset($opsi_d[1]) ? $opsi_d[1]:"-";
					$opsi_e_j = isset($opsi_e[1]) ? $opsi_e[1]:"-";


					
					$pertanyaan    = array($opsi_a_per,$opsi_b_per,$opsi_c_per,$opsi_d_per,$opsi_e_per);
					
					$listjawaban  = array($opsi_a_j,$opsi_b_j,$opsi_c_j,$opsi_d_j,$opsi_e_j);
						
				   
					

					  $html .= '<hr>  <div class="table-responsive"> <table class="table-hover table  ">';
				  
						$noempat=1;
						  foreach($pertanyaan as $ig=>$per){

							

							  
  
							  if(!empty($per)){
					  
								$html .= '<tr>';
								$html .= '<td align="left" width="3px">'.$noempat++.'. </td>';
								$html .= '<td align="left">'.$per.'</td>';
								$html .= '<td align="left">'.$listjawaban[$jawaban44[$ig]].'</td>';
							  
							   
								$html .= '<tr>';
							  }
  
  
  
						  }
						  $html .= '</table>
						  
						  <textarea class="form-control" name="keterangan_'.$no.'" disabled placeholder="Masukan alasan Anda ">'.$ket.'</textarea>
						 
						  </div>';
					
					
					
					
					
					
				}else if($s->jenis==3){
					$html .= '<hr><div class="rows">'; 
					$html .= '<div class="form-group"><textarea class="form-control ayolah" disabled nomor="'.$no.'" name="jawaban_'.$no.'"  placeholder="Masukkan Jawaban Anda disini " >'.$jwbn.'</textarea></div>';
					
					
					
			   }else if($s->jenis==5){
					$html .= '<hr><div class="rows">';
					$html .= '<div class="form-group"><textarea class="form-control ayolah" disabled nomor="'.$no.'" name="jawaban_'.$no.'"  placeholder="Masukkan Jawaban Anda disini " >'.$jwbn.'</textarea></div>';
					
					
					
				}else if($s->jenis==6){
					$arnumber = array("a"=>"1","b"=>"2","c"=>"3","d"=>"4","e"=>"5");
					if($jwbn !=""){
						
						$jawaban = explode("#",$jwbn);

					}

					$html .= '<hr><div class="funkyradio">';
					for ($j = 0; $j < 5; $j++) {
						$opsi 			= "opsi_".$arr_opsi[$j];
						$file 			= "file_".$arr_opsi[$j];
						$checked 		= "";

						if($jwbn !=""){
										
							if (in_array(strtoupper($arr_opsi[$j]), $jawaban)){
							   
								   $checked="checked";


							   }
						   
						   
						   
						   
				   }



						$pilihan_opsi 	= !empty($s->$opsi) ? $s->$opsi : "";
						if($pilihan_opsi !=""){
						$html .= '<div class="funkyradio-success">
							<input type="checkbox" id="opsi_'.strtolower($arr_opsi[$j]).'_'.$s->id_soal.'"  disabled class="ayolah" nomor="'.$no.'"  name="jawaban_'.$no.'[]" value="'.strtoupper($arr_opsi[$j]).'" '.$checked.' > <label for="opsi_'.strtolower($arr_opsi[$j]).'_'.$s->id_soal.'"><div class="huruf_opsi">'.$arnumber[$arr_opsi[$j]].'</div> <p>'.$pilihan_opsi.'</p></label></div>';
						}
					}
					
					



							
				}else if($s->jenis==7){
                    //echo 
					if($jwbn !=""){
						
						$jawaban = explode("#",$jwbn);

					}

					$html .= '<hr><div class="row">';
					$html .= '<hr>  <div class="table-responsive"> <table class="table-hover table  ">';
					$html .= '<tr>';
					$html .= '<td width="60%"></td>';
					$html .= ' <td align="center">'.$s->subjek1.'</td>';
					$html .= ' <td align="center">'.$s->subjek2.'</td>';
					$html .= '</tr>';
					$pilihanganda = array("A"=>"opsi_a","B"=>"opsi_b","C"=>"opsi_c","D"=>"opsi_d","E"=>"opsi_e");
						$jawabanganda = array("A"=>"file_a","B"=>"file_b","C"=>"file_c","D"=>"file_d","E"=>"file_e");
						$nt =0;
						foreach($pilihanganda as $ig=>$pg){
							$jj = $jawabanganda[$ig];

							
							
							$penanda = $nt;
							$subjek1 = (!empty($s->subjek1)) ? $s->subjek1.$penanda : "s1".$penanda; 
							$subjek2 = (!empty($s->subjek2))? $s->subjek2.$penanda : "s2".$penanda; 

							if(!empty($s->$pg)){
								$checked1="";
								$checked2="";
								
								if($jwbn !=""){
										
									     if (in_array($subjek1, $jawaban)){
											
												$checked1="checked";


											}
										
										if (in_array($subjek2, $jawaban)){
											
											$checked2="checked";


										}
									
										
										
								}
					          
					          $html .= '<tr>';
					          $html .= '<td>'.$s->$pg.'</td>';
							  $html .= '<td align="center"><input type="checkbox" disabled  class="ayolah" nomor="'.$no.'" name="jawaban_'.$no.'[]" '.$checked1.' value="'.$subjek1.'" ></td>';
					          $html .= '<td align="center"><input type="checkbox" disabled  class="ayolah" nomor="'.$no.'" name="jawaban_'.$no.'[]" '.$checked2.' value="'.$subjek2.'" ></td>';
					      
							  $html .= '<tr>';
							  $nt++;
							  

							}
							
							

						}
						$html .= '</table></div>';
						
					}else if($s->jenis==8){
							
					   $html .= '<hr><div class="row">';
						$html .= ' <div class="table-responsive"> <table class="table-hover table  ">';

						if($jwbn !=""){
						
							$jwbn8 = explode("#",$jwbn);
	
						}
					
						 
						  $pertanyaan   = explode("<br />",$s->jawaban_essay);
					  
							foreach($pertanyaan as $ig=>$per){

								
	
								if(!empty($per)){
									$jw ="";
									 if(isset($jwbn8[$ig])){
										$jw = (!empty($jwbn8[$ig])) ? $jwbn8[$ig] :"";

									 }
									
						
								  $html .= '<tr>';
								 
								  $html .= '<td align="left">'.$per.'</td>';
								  $html .= '<td align="left"><input type="text" disabled class="form-control ayolah"  nomor="'.$no.'" name="jawaban_'.$no.'[]" value="'.$jw.'" placeholder="Ketik Jawaban Anda disini " ></td>';
								
								 
								  $html .= '<tr>';
								}
	
	
	
							}
							$html .= '</table></div>';
					}
				
				
				
				$html .= '</div></div>';
				$no++;
			}
		}

		$id_tes = base64_encode($detail_tes->id);

		$data = [
			
			'judul'		=> 'Ujian',
			'subjudul'	=> 'Lembar Ujian',
			'soal'		=> $detail_tes,
			'no' 		=> $no,
			'html' 		=> $html,
			'id_tes'	=> $id_tes
		];
		 $data['data']	      = $siswa;
		 $data['literasi']	      = $literasi;
		 $data['ujian']	      = $this->Reff->get_where("tm_ujian",array("id"=>$id));
		 $data['title']	      = "UJIAN - ".$data['ujian']->nama;
		 
		 $this->load->view("cbthasil/header",$data);
		 $this->load->view('cbthasil/sheet');
		 
	}
	
	public function hasilcebt(){
	    
	    	 		 
			
		$data['siswa']      = $this->db->query(" select * from   v_siswa where id='".($_GET['tmsiswa_id'])."'")->row();
			
		$pdf_filename = 'Hasil_Kompetensi.pdf';	 
	
		$user_info = $this->load->view('cetakhasil', $data, true);
	
		 $output = $user_info;
		
		
	
		generate_pdf($output, $pdf_filename,TRUE);
		


   }
	 
	 
	 
}
