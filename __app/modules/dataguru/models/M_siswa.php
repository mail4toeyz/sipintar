<?php

class M_siswa extends CI_Model {
 
    public function __construct() {
        parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
    }

    
	public function grid($paging){
       
	    $key = $this->input->get_post("keyword");
	   
		$this->db->select("*");
        $this->db->from('tm_guru');
	  
		
	
	    if(!empty($key)){  $this->db->where("UPPER(nama) LIKE '%".strtoupper($key)."%'");    }
	 
		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 $this->db->order_by("provinsi","ASC");
					
					
		
			 }
      
		
		
		return $this->db->get();
		
	}
	
	
	
	public function insert(){
		
		$form  			  = $this->security->xss_clean($this->input->get_post("f"));
		$no_aksi          = strtoupper($this->Reff->randomangka(6));
	    
		$this->db->set("password",enkrip($_POST['password']));
		$this->db->set("no_aksi",$no_aksi);
		
		$this->db->insert("tm_siswa",$form);
		                                                
		
		
		     if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
					 return false;
				   
				} else {
						
					$this->db->trans_commit();
					 return true;
					
				}
		
		
		
	}
	
	public function update($id){
		
		$form  			  = $this->security->xss_clean($this->input->get_post("f"));
		  
							
		
		$this->db->set("password",enkrip($_POST['password']));
	
		$this->db->where("id",$id);
		$this->db->update("tm_siswa",$form);
		                                                
		
		
		     if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
					 return false;
				   
				} else {
						
					$this->db->trans_commit();
					 return true;
					
				}
		
		
		
	}
	
}
