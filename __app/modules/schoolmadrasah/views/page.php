
<div class="row">
  <div class="col-md-8">
              <div class="card">
                <div class="card-header card-header-success">
                  <h4 class="card-title">Profile Madrasah</h4>
                  <p class="card-category">Data Madrasah Anda </p>
                </div>
                <div class="card-body">
                  <form id="simpantanparespon" method="post" url="<?php echo site_url("schoolmadrasah/save"); ?>">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">NSM </label>
                          <input type="text" class="form-control" disabled value="<?php c($data->nsm); ?>">
                        </div>
                      </div>
                     
                      
                    </div>
                    <div class="row">
                       <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Nama Madrasah</label>
                          <input type="text" class="form-control"  name="f[nama]" value="<?php echo c($data->nama); ?>">
                        </div>
                      </div>
                    </div>

                    <div class="row">
                       <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Alamat </label>
                          <input type="text" class="form-control"  name="f[alamat]"  value="<?php echo c($data->alamat); ?>">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                       <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Nama Kepala Madrasah </label>
                          <input type="text" class="form-control"  name="f[kepala]"  value="<?php echo c($data->kepala); ?>">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                       <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Telepon </label>
                          <input type="text" class="form-control"  name="f[telepon]"  value="<?php echo c($data->telepon); ?>">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="bmd-label-floating">Password</label>
                          <input type="text" class="form-control"  name="f[password]" value="<?php echo ($data->password); ?>">
                        </div>
                      </div>
                     
                      
                    </div>
				
					
				
					
					
				
				
					
					
					
					
                    <button type="submit" class="btn btn-success pull-right" > Update Profile</button>
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card card-profile">
                <div class="card-avatar">
                  <a href="#pablo">
				   <?php 
				  
					   $logo = "__statics/img/logo.png";
					   
				   
				   ?>
                    <img class="img" src="<?php echo base_url(); ?><?php echo $logo; ?>" id="blah" />
                  </a>
                </div>
                <div class="card-body">
                  <form action="javascript:void(0)" method="post" id="simpangambarnorespon" name="simpangambarnorespon" url="<?php echo site_url("schoolmadrasah/save_logo"); ?>" enctype="multipart/form-data">	
                
                  <p class="card-description">
                                     <div class="form-group">
							
                                    </div>
									
									
						
								 
								  <div id="loadingmodal" style="display:none">
								      <div class="col-sm-12">
								  <center>  <i class="fa  fa-spinner fa-spin"></i>   <br> Data sedang disimpan, Mohon Tunggu ...</center>
								   </div>
								 </div>
								 
								 
                  </p>
                 </form>
                </div>
              </div>
            </div>
</div>


	