  <div class="col-md-8">
              <div class="card">
                <div class="card-header card-header-success">
                  <h4 class="card-title">Statistik dan Grafik </h4>
                  <p class="card-category">Aktifitas dan Data Elearning</p>
                </div>
                <div class="card-body">
                    <div class="row">
            <div class="col-md-12">
              <div class="card card-chart">
                <div class="card-header card-header-success">
                  <div class="ct-chart" id="dailySalesChart"></div>
                </div>
                <div class="card-body">
                  <h4 class="card-title">Aktifitas Elearning</h4>
                  <p class="card-category">
                    <span class="text-success"><i class="fa fa-long-arrow-up"></i> 20% </span> Aktifitas pada hari ini  </p>
                </div>
                <div class="card-footer">
                  <div class="stats">
                     Line Realtime
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="card card-chart">
                <div class="card-header card-header-warning">
                  <div class="ct-chart" id="websiteViewsChart"></div>
                </div>
                <div class="card-body">
                  <h4 class="card-title">Users Aplikasi</h4>
                  <p class="card-category">Grafik Mingguan </p>
                </div>
                <div class="card-footer">
                  <div class="stats">
                   Grafik realtime
                  </div>
                </div>
              </div>
            </div>
          
          </div>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="card card-profile">
                <div class="card-avatar">
                  <a href="#pablo">
                    <img class="img" src="<?php echo base_url(); ?>__statics/img/elearning.png" />
                  </a>
                </div>
                <div class="card-body">
                 
                  <h4 class="card-title">Statistik dan Grafik</h4>
                  <p class="card-description">
                     Menampilkan Aktifitas Guru dan Siswa setiap perhari
                  </p>
                 
                </div>
              </div>
            </div>