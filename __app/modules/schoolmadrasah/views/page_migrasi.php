  <div class="row">
  <div class="col-md-12">
              <div class="card">
                <div class="card-header card-header-success">
                  <h4 class="card-title">Version Control Migration</h4>
                  <p class="card-category"> Jika Migrasi otomatis ketika update patch atau versi silahkan lakukan migrasi manual   </p>
                </div>
                <div class="card-body">
                
				
				
				<div class="card">
                <div class="card-header card-header-success">
                  <h4 class="card-title">Migrasi pada versi 1.5.0 dan 2.0.0</h4>
                  <p class="card-category"> </p>
				  
				    
					
                </div>
                <div class="card-body">
                  
				 <h4> 1. Migrasi Rombel </h4>
				    <?php 
					 
						if ($this->db->field_exists('ajaran', 'tr_ruangkelas')==1){
							?>
							 <div class="alert alert-danger">
							  Status migrasi pada rombel berhasil secara otomatis
							 </div>
							
							
							<?php 
							
						}else{
							
							?>
							 <div class="alert alert-danger">
							  Migrasi ini harus dilakukan secara manual, silahkan klik tombol dibawah ini
							 <br>
							 <center>
							     <button type="button" id="migrasi" jenis="1" class="btn btn-success"> Migrasi Rombel </button>
					         </center>
							</div>
							
							
					
							<?php 
							
						}
						
				  ?>
							
                    
                 <h4> 2. Migrasi Pembuatan Tabel 1 </h4>
				    <?php 
					 
						if($this->db->table_exists("tr_alumni")==1){
							?>
							 <div class="alert alert-danger">
							  Status migrasi pembuatan tabel 1 berhasil secara otomatis
							 </div>
							
							
							<?php 
							
						}else{
							
							?>
							 <div class="alert alert-danger">
							  Migrasi ini harus  dilakukan secara manual, silahkan klik tombol dibawah ini
							 <br>
							 <center>
							     <button type="button" id="migrasi" jenis="2" class="btn btn-success"> Migrasi Pembuatan Tabel  </button>
					         </center>
							</div>
							
							
					
							<?php 
							
						}
						
				  ?>    
					
				
				    
                 <h4> 3. Troubleshoot Rombel tidak muncul setelah update patch </h4>
				   
							 <div class="alert alert-danger">
							  Jika rombel tidak muncul setelah update patch 1.5.0, Silahkan klik tombol dibawah ini untuk memecahkan masalah tersebut, <br>
							  Semua rombel akan di set ke tahun pelajaran 2019/2020, jika Anda sudah terlanjur mengisi rombel di tahun pelajaran 2020/2021, silahkan klik tombol pensil pada menu ruang kelas untuk di lakukan update secara manual
							 <br>
							 <center>
							     <button type="button" id="migrasi" jenis="3" class="btn btn-success"> Pecahkan Masalah ini </button>
					         </center>
							</div>
							
							
					 <h4> 4. Migrasi Video Conference </h4>
				    <?php 
					 
						if($this->db->table_exists("tr_vicon")==1){
							?>
							 <div class="alert alert-danger">
							  Status migrasi pembuatan tabel 2 berhasil secara otomatis
							 </div>
							
							
							<?php 
							
						}else{
							
							?>
							 <div class="alert alert-danger">
							  Migrasi ini harus  dilakukan secara manual, silahkan klik tombol dibawah ini
							 <br>
							 <center>
							     <button type="button" id="migrasi" jenis="4" class="btn btn-success"> Migrasi Pembuatan Video Conference  </button>
					         </center>
							</div>
							
							
					
							<?php 
							
						}
						
				  ?>    
					
                   
                </div>
              </div>
				
				
                </div>
              </div>
            </div>
			
			
			
		   
     </div>
          
			
<script>
  
   $(document).off("click","#migrasi").on("click","#migrasi",function(){
	   var jenis = $(this).attr("jenis");
	   alertify.confirm("Apakah Anda yakin akan melakukan migrasi ?",function(){
		   
		   loading();
		   $.post("<?php echo site_url('schoolmadrasah/lakukanmigrasi'); ?>",{jenis:jenis},function(){
			   
			   
			 alertify.alert("Migrasi Berhasil");
			 location.reload();
			 jQuery.unblockUI({ }); 
			   
		   })
		   
		   
		   
	   })
	   
	   
   })
    

</script>
           