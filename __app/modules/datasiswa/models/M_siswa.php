<?php

class M_siswa extends CI_Model {
 
    public function __construct() {
        parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
    }

    
	public function grid($paging){
       
	    $key           = $this->input->get_post("keyword");
	    $tmmadrasah_id = $this->input->get_post("tmmadrasah_id");
	    $kategori         = $this->input->get_post("kategori");
		$sesi          = $this->input->get_post("sesi");
		$pengaturan_id  = $this->Reff->set();
		$this->db->select("*");
        $this->db->from('tm_siswa');
		$this->db->where("pengaturan_id",$pengaturan_id);
		
		
		
		if($_SESSION['role'] ==1){
			$this->db->where("pengawas",$_SESSION['aksi_id']);

		}

	
	    if(!empty($key)){  $this->db->where("(UPPER(nama) LIKE '%".strtoupper($key)."%' or (no_test) LIKE '%".strtoupper($key)."%')");    }
	  
	    if(!empty($kategori)){  $this->db->where("madrasah_peminatan",$kategori);    }
	   
	
		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 $this->db->order_by("madrasah_peminatan","ASC");
					 $this->db->order_by("nama","ASC");
					
					
		
			 }
      
		
		
		return $this->db->get();
		
	}
	
	
	
	public function insert(){
		
		$form  			  = $this->security->xss_clean($this->input->get_post("f"));
		$pengaturan_id  = $this->Reff->set();
		$this->db->set("pengaturan_id",$pengaturan_id);
		$this->db->insert("tm_siswa",$form);
		                                                
		
		
		     if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
					 return false;
				   
				} else {
						
					$this->db->trans_commit();
					 return true;
					
				}
		
		
		
	}
	
	public function update($id){
		
		$form  			  = $this->security->xss_clean($this->input->get_post("f"));
		  
		
		$this->db->where("id",$id);
		$this->db->update("tm_siswa",$form);
		                                                
		
		
		     if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
					 return false;
				   
				} else {
						
					$this->db->trans_commit();
					 return true;
					
				}
		
		
		
	}
	
}
