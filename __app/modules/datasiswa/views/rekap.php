

        <div class="col-xl-12">
          <div class="card ">
            <div class="card-header bg-transparent">
              <div class="row align-items-center">
              
                
              </div>
            </div>
            <div class="card-body">
              <!-- Chart -->
              <div class="" id="container_grafik">
                 				   
<button id="btnExport" onclick="nilai_akhirdendiganteng();" class="btn btn-primary btn-sm" style="background-color:#252b87;color:white;"><i class="fa fa-file-excel"></i> EXPORT EXCEL </button>
<iframe id="txtArea1" style="display:none"></iframe>
<script>
function nilai_akhirdendiganteng()
{
    var tab_text="<table border='2px' ><tr>";
    var textRange; var j=0;
    tab = document.getElementById('table1'); // id of table

    for(j = 0 ; j < tab.rows.length ; j++) 
    {     
        tab_text=tab_text+tab.rows[j].innerHTML+"</tr>";
        //tab_text=tab_text+"</tr>";
    }

    tab_text=tab_text+"</table>";
    tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
    tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
    tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE "); 

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
    {
        txtArea1.document.open("txt/html","replace");
        txtArea1.document.write(tab_text);
        txtArea1.document.close();
        txtArea1.focus(); 
        sa=txtArea1.document.execCommand("SaveAs",true,"cetak.xls");
    }  
    else                 //other browser not tested on IE 11
        sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));  

    return (sa);
}

</script>

                 <button onclick="btn_export()" class="btn btn-primary btn-sm">
            Cetak Excel 2030 
          </button>
                   <div class="table-responsive">
                         <table class="table table-hover table-bordered" id="table1" >
                             <thead>
                                 <tr>
                                      <th rowspan="4">No</th>
                                      <th rowspan="4">Provinsi</th>
                                      <th colspan="60">Hasil Fasilitator Provinsi</th>
                                      <th rowspan="4">Total </th>
                                      <th rowspan="4">Pengawas Madrasah </th>
                                      <th rowspan="4">Kepala Madrasah </th>
                                 </tr>
                                 
                                 <tr style="background:#60cba6;color:white">
                                      <th colspan="12">MI</th>
                                      <th colspan="16">MTs</th>
                                      <th colspan="32">MA</th>
                                 </tr>
                                 
                                 <tr style="background:#b6c631;color:white">
                                      <?php 
                                       foreach($this->db->get_where("literasi",array("jenjang"=>1))->result() as $rd){
                                            ?>  <th colspan="4"><?php echo $rd->nama; ?></th><?php 
                                       }
                                       ?>
                                      
                                      <?php 
                                       foreach($this->db->get_where("literasi",array("jenjang"=>2))->result() as $rd){
                                            ?>  <th colspan="4"><?php echo $rd->nama; ?></th><?php 
                                       }
                                       ?>
                                     
                                      
                                      <?php 
                                       foreach($this->db->get_where("literasi",array("jenjang"=>3))->result() as $rd){
                                            ?>  <th  colspan="4"><?php echo $rd->nama; ?></th><?php 
                                       }
                                       ?>
                                     
                                 </tr>

                                 <tr style="background:#3196c6;color:white">
                                      <?php 
                                       foreach($this->db->get_where("literasi",array("jenjang"=>1))->result() as $rd){
                                            ?>  <th>K</th><?php 
                                            ?>  <th>P</th><?php 
                                            ?>  <th>R</th><?php 
                                            ?>  <th>L</th><?php 
                                       }
                                       ?>
                                      
                                      <?php 
                                       foreach($this->db->get_where("literasi",array("jenjang"=>2))->result() as $rd){
                                        ?>  <th>K</th><?php 
                                        ?>  <th>P</th><?php 
                                        ?>  <th>R</th><?php 
                                        ?>  <th>L</th><?php  
                                       }
                                       ?>
                                
                                      <?php 
                                       foreach($this->db->get_where("literasi",array("jenjang"=>3))->result() as $rd){
                                        ?>  <th>K</th><?php 
                                        ?>  <th>P</th><?php 
                                        ?>  <th>R</th><?php 
                                        ?>  <th>L</th><?php 
                                       }
                                       ?>
                                      
                                 </tr>
                             </thead>
                             
                             <tbody>
                                 
                                 <?php 
                                 $no=1;
                                 $total=0;
                                 
                                   foreach($this->db->query("select *  from provinsi order by status desc")->result() as $row){

                                    $pengawasmadrasah = $this->db->query("select count(id) as jml from tm_siswa where posisi=2 and provinsi='".$row->id."' and kelulusan=1 and unsur='3' ")->row();
                                    $kepalamadrasah   = $this->db->query("select count(id) as jml from tm_siswa where posisi=2 and provinsi='".$row->id."' and kelulusan=1 and unsur='7' ")->row();

                                       ?>
                                       <tr>
                                           <td><?php echo $no++; ?></td>
                                           <td><?php echo $row->nama; ?>  <?php echo ($row->status==1) ? " (*)":""; ?></td>
                                           
                                           <?php 
                                            $total1=0;
                                               foreach($this->db->get_where("literasi",array("jenjang"=>1))->result() as $rd){
                                                     $pes     = $this->db->query("select count(id) as jml from tm_siswa where provinsi='".$row->id."' and posisi=2 and literasi='".$rd->id."' and nilai IS NOT NULL")->row();
                                                     $jml     = $this->db->query("select count(id) as jml from tm_siswa where provinsi='".$row->id."' and posisi=2 and literasi='".$rd->id."' and keputusan=1")->row();
                                                     $jm_lulus = $this->db->query("select count(id) as jml from tm_siswa where provinsi='".$row->id."' and posisi=2 and literasi='".$rd->id."' and keputusan=1 and kelulusan=1")->row();
                                                     $jml = $jml->jml;
                                                     $total1 = $total1 + $jm_lulus->jml;
                                                     $kolom = "s".$rd->id;
                                                       $style="";
                                                         if($jm_lulus->jml==0){
                                                             
                                                          $style='style="background:red;"';    
                                                         }else if($jm_lulus->jml < $row->$kolom){
                                                          $style='style="background:yellow;"'; 

                                                         }

                                                         
                                                         ?>
                                                         
                                                       <td <?php echo $style; ?>><?php echo $row->$kolom; ?></td>
                                                      <td <?php echo $style; ?>><?php echo $pes->jml; ?></td>
                                                      <td <?php echo $style; ?>><?php echo $jml; ?></td>
                                                      <td <?php echo $style; ?>><?php echo $jm_lulus->jml; ?></td>
                                                      
                                                      <?php 
                                               }
                                               ?>
                                              
                                         <?php 
                                            $total2=0;
                                               foreach($this->db->get_where("literasi",array("jenjang"=>2))->result() as $rd){
                                                $pes     = $this->db->query("select count(id) as jml from tm_siswa where provinsi='".$row->id."' and posisi=2 and literasi='".$rd->id."' and nilai IS NOT NULL")->row();
                                                $jml     = $this->db->query("select count(id) as jml from tm_siswa where provinsi='".$row->id."' and posisi=2 and literasi='".$rd->id."' and keputusan=1")->row();
                                                $jm_lulus = $this->db->query("select count(id) as jml from tm_siswa where provinsi='".$row->id."' and posisi=2 and literasi='".$rd->id."' and keputusan=1 and kelulusan=1")->row();
                                                $jml = $jml->jml;
                                                $total2 = $total2 + $jm_lulus->jml;
                                                $kolom = "s".$rd->id;
                                                   $style="";
                                                   if($jm_lulus->jml==0){
                                                             
                                                    $style='style="background:red;"';    
                                                   }else if($jm_lulus->jml < $row->$kolom){
                                                    $style='style="background:yellow;"'; 

                                                   }
                                                         ?>
                                                         
                                                         <td <?php echo $style; ?>><?php echo $row->$kolom; ?></td>
                                                      <td <?php echo $style; ?>><?php echo $pes->jml; ?></td>
                                                      <td <?php echo $style; ?>><?php echo $jml; ?></td>
                                                      <td <?php echo $style; ?>><?php echo $jm_lulus->jml; ?></td>
                                                      
                                                      
                                                      <?php 
                                               }
                                               ?>
                                              
                                              
                                             <?php 
                                            $total3=0;
                                               foreach($this->db->get_where("literasi",array("jenjang"=>3))->result() as $rd){
                                                $pes     = $this->db->query("select count(id) as jml from tm_siswa where provinsi='".$row->id."' and posisi=2 and literasi='".$rd->id."' and nilai IS NOT NULL")->row();
                                                $jml     = $this->db->query("select count(id) as jml from tm_siswa where provinsi='".$row->id."' and posisi=2 and literasi='".$rd->id."' and keputusan=1")->row();
                                                $jm_lulus = $this->db->query("select count(id) as jml from tm_siswa where provinsi='".$row->id."' and posisi=2 and literasi='".$rd->id."' and keputusan=1 and kelulusan=1")->row();
                                                $jml = $jml->jml;
                                                $total3 = $total3 + $jm_lulus->jml;
                                                $kolom = "s".$rd->id;
                                                   $style="";
                                                   if($jm_lulus->jml==0){
                                                             
                                                    $style='style="background:red;"';    
                                                   }else if($jm_lulus->jml < $row->$kolom){
                                                    $style='style="background:yellow;"'; 

                                                   }
                                                         ?>
                                                         
                                                      <td <?php echo $style; ?>><?php echo $row->$kolom; ?></td>
                                                      <td <?php echo $style; ?>><?php echo $pes->jml; ?></td>
                                                      <td <?php echo $style; ?>><?php echo $jml; ?></td>
                                                      <td <?php echo $style; ?>><?php echo $jm_lulus->jml; ?></td>
                                                      <?php 
                                               }
                                               ?>
                                             
                                              
                                              <td><?php echo ($total1+$total2+$total3); ?> </td>
                                              <td><?php echo ($pengawasmadrasah->jml); ?> </td>
                                              <td><?php echo ($kepalamadrasah->jml); ?> </td>
                                       </tr>
                                       
                                       
                                       
                                       
                                       <?php 
                                       
                                       
                                   }
                                 ?>
                             </tbody>
                             
                         </table>
                       
                       
                       
                   </div>
                
              </div>
            </div>
          </div>
  

<script src="https://emadrasah.kemenag.go.id/e-ponsel/__statics/js/excel/xlsx.extendscript.js"></script>
<script src="https://emadrasah.kemenag.go.id/e-ponsel/__statics/js/excel/xlsx-style/xlsx.full.min.js"></script>

<script src="https://emadrasah.kemenag.go.id/e-ponsel/__statics/js/excel/export.js"></script>
<script type="text/javascript">


    function btn_export() {
        var table1 = document.querySelector("#table1");
        var opt = {
            rowIndex: 3
        }; //开头空4行
        var sheet = XLSX2.utils.table_to_sheet(table1, opt);

  
     

        openDownloadDialog(sheet2blob(sheet), ' Rekap-Pendaftar-Provinsi.xlsx');
    }


</script>