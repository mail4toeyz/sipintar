<style>
input[type="file"] {
    display: none;
}
.custom-file-upload {
    border: 1px solid #ccc;
    display: inline-block;
    padding: 6px 12px;
    cursor: pointer;
	background:red;
	color:white;
}
</style>
						
						 <form action="javascript:void(0)" method="post" id="simpangambarmodal" name="simpangambarmodal" url="<?php echo site_url("schoolsiswa/save_foto"); ?>">	
								
                         <input type="hidden" name="id" value="<?php echo $data->id; ?>">
								<div class="row clearfix">
                                 <div class="col-sm-12">
								 
								  <div class="alert alert-danger">
								  Keterangan : <br>
									<ol>
									  <li> Maksimal foto yang diupload adalah 2 MB </li>
									  <li> Format Foto jpg/png/jpeg </li>
								    </div>
									
									
								 
                                    <div class="form-group">
									<center>
									<label for="file-upload" class="custom-file-upload">
											<i class="fa fa-cloud-upload"></i> Pilih Foto Dari Komputer
										</label>
										<input id="file-upload" type="file" required name="file" accept="image/x-png,image/gif,image/jpeg" >

																			
									</center>
                                    </div>
                                    
                                 </div>
                                </div>
								
								<div class="row clearfix">
                                 <div class="col-sm-12">
								 
								  
								 
                                    <div class="form-group">
									<center>
										
									   <?php 
										 $src = "#";
										   if(!empty($data->foto)){
											   $src = base_url().$data->folder."/".$data->foto;
										   }
										   ?>
									   <img id="blah" src="<?php echo $src; ?>" alt="Preview Foto" class="img-responsive img-thumbnail" style="max-height:250px" />
																			
									</center>
                                 
                                    
                                 </div>
                                </div>
                                </div>
								
								
								
								 <div class=" " id="aksimodal">
								    <div class="col-sm-12">
						       <center>
								   <button type="submit" id="butonsubmit" style="display:none" class="btn btn-success btn-sm ">
										<i class="fa fa-save"></i>
										<span> Upload Foto  </span>
                                    </button>
								    
									<button type="button" class="btn btn-success btn-sm " data-dismiss="modal">
										
										<span>Close   </span>
                                    </button>
									
								   </center>
								
								 </div>
								 </div>
								 
								  <div id="loadingmodal" style="display:none">
								      <div class="col-sm-12">
								  <center>  <i class="fa  fa-spinner fa-spin"></i>   <br> Data sedang disimpan, Mohon Tunggu ...</center>
								   </div>
								 </div>
							  
					</form>	   

<script>
	$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
	
	
	
    });
	
	function readURL(input) {
	  if (input.files && input.files[0]) {
		var reader = new FileReader();
		
		reader.onload = function(e) {
		  $('#blah').attr('src', e.target.result);
		}
		
		reader.readAsDataURL(input.files[0]);
	  }
	}

	$("#file-upload").change(function() {
	  readURL(this);
	  $("#butonsubmit").show("slow");
	
	});

	</script>					
	