<?php

class M_siswa extends CI_Model {
 
    public function __construct() {
        parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
    }



	public function grid($paging){
       
	    $key           = $this->input->get_post("keyword");
	    $tmmadrasah_id = $this->input->get_post("tmmadrasah_id");
	    $kategori         = $this->input->get_post("kategori");
		$status           = $this->input->get_post("status");
        $tmujian_id       = $this->input->get_post("tmujian_id");
		$pengaturan_id  = $this->Reff->set();
		$this->db->select("*");
        $this->db->from('tm_siswa');
		$this->db->where("pengaturan_id",$pengaturan_id);
		if(!empty($status)){
            if($status=="1"){
                $this->db->where("id NOT IN (select tmsiswa_id from h_ujian where tmujian_id='".$tmujian_id."')");
            }else{
                $this->db->where("id IN (select tmsiswa_id from h_ujian where tmujian_id='".$tmujian_id."' and status='".$status."')");
            }
           
      }
      if($_SESSION['role'] ==1){
        $this->db->where("pengawas",$_SESSION['aksi_id']);

    }

	
	    if(!empty($key)){  $this->db->where("(UPPER(nama) LIKE '%".strtoupper($key)."%' or (no_test) LIKE '%".strtoupper($key)."%')");    }
	  
	    if(!empty($kategori)){  $this->db->where("madrasah_peminatan",$kategori);    }
	   
	
		
		
         if($paging==true){
				     $this->db->limit($_REQUEST['length'],$_REQUEST['start']);
					 $this->db->order_by("madrasah_peminatan","ASC");
					 $this->db->order_by("nama","ASC");
					
					
		
			 }
      
		
		
		return $this->db->get();
		
	}
	

	public function getUjianById($id)
    {
        $this->db->select('*');
        $this->db->from('tm_ujian a');
      
        $this->db->where('a.id', $id);
        return $this->db->get()->row();
    }
	
	 public function getSoal($id)
    {
        $ujian = $this->getUjianById($id);
        $order = $ujian->jenis==="acak" ? 'rand()' : 'urutan';

        $this->db->select('id as id_soal, soal,opsi_a, opsi_b, opsi_c, opsi_d, opsi_e, jawaban,jenis');
        $this->db->from('tr_soal');
  
        $this->db->where('tmujian_id', $id);
        $this->db->order_by($order);
        
        return $this->db->get()->result();
    }
	
	 public function HslUjian($id, $tmsiswa_id)
    {
        $this->db->select('*, UNIX_TIMESTAMP(tgl_selesai) as waktu_habis');
        $this->db->from('h_ujian');
        $this->db->where('tmujian_id', $id);
        $this->db->where('tmsiswa_id', $tmsiswa_id);
        return $this->db->get();
    }
	
	
	  public function ambilSoal($pc_urut_soal1, $pc_urut_soal_arr)
    {
        $this->db->select("*,id as id_soal, {$pc_urut_soal1} AS jawaban");
        $this->db->from('tr_soal');
        $this->db->where('id', $pc_urut_soal_arr);
         $data =  $this->db->get()->row();
         if(!is_null($data)){
            return '';
         }else{

            return $data;
         }
    }
	
	  public function getJawaban($id_tes)
    {
        $this->db->select('list_jawaban');
        $this->db->from('h_ujian');
        $this->db->where('id', $id_tes);
        return $this->db->get()->row()->list_jawaban;
    }
	
	 public function getSoalById($id)
    {
        return $this->db->get_where('tr_soal', ['id' => $id])->row();
    }
	
	
	


}
