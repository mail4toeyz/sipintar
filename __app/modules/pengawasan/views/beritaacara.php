<link href="<?php echo base_url(); ?>__statics/js/gal/css/lightgallery.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>__statics/js/gal/js/lightgallery-all.min.js"></script>
<div class="container-fluid">
        <div class="row">
    <div class="col-md-12 mb-3">
        <a href="<?php echo base_url(); ?>pengawasan/ujian?id=1&kategori=1" class="btn btn-social btn-danger">
            <i class="fa fa-arrow-left"></i> Kembali
          </a>
      </div>

        <div class="col-md-5">
            
            <!-- Default card -->
            <div class="card">
                <div class="card-header with-border">
                    <h3 class="card-title">Identitas Peserta</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6 text-center">
							<img src="https://appmadrasah.kemenag.go.id/seleksiptk/file_upload/peserta/<?php echo $peserta->nik; ?>/<?php echo $peserta->foto; ?>" class="img-circle" width="35%"  alt="User Image" >
                        </div>
                        <div class="col-md-6">
                            <strong>Nomor Peserta</strong><br>
                            <?php echo $peserta->no_test; ?>                            
                            <br>
                            <strong>Nama Peserta</strong><br>
                            <?php echo $peserta->nama; ?>                        </div>
                    </div>
                    <div class="row">
                        <?php 
                         $hasilUjian = $this->db->query("SELECT * from h_ujian where tmsiswa_id='{$peserta->id}' and id='".$data->id."'")->row();
                         $status = "<span class='badge badge-sm bg-gradient-secondary'> Belum Mengerjakan </span>";
                         $stop ="";
                       
                         if(!is_null($hasilUjian)){
                         if($hasilUjian->status=="Y"){
         
                           $status = "<span class='badge badge-sm bg-gradient-warning'>Sedang mengerjakan </span>";				
         
                           $stop   ="<a href='javascript:void(0)' class='btn btn-danger btn-sm forcestop' tmsiswa_id='".$peserta->id."' tmujian_id='".$hasilUjian->id."'><span class='fa fa-stop'></span>  Force stop </a>";
                         
         
                         }else if($hasilUjian->status=="N"){
         
                           $status = "<span class='badge badge-sm bg-gradient-success'>Selesai mengerjakan </span>";
                           $stop   ="<a href='javascript:void(0)' class='btn btn-danger btn-sm resetujian' tmsiswa_id='".$peserta->id."' tmujian_id='".$hasilUjian->id."'><span class='fa fa-check'></span> Reset Ujian </a>";
         
                         }

                        }
                        echo $status; 
                        echo $stop;
                         ?>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
                
            
            <div class="card">
                <div class="card-header with-border">
                    <h3 class="card-title">Foto Rekam Peserta</h3>
                </div>
                <div class="card-body">
					<div class="row">
                                    
                        <!--a href=""-->
                        
						<div class="demo-gallery" >
																<ul id="lightgallery" class="list-unstyled row">

                                                                <?php 
                                                                                
                                                                                if($peserta->foto_awal !=""){
																		    		$file    = base_url()."__statics/upload/".$peserta->foto_awal;
                                                                                ?>
                                                                                
                                                                                    <li   data-responsive="<?php echo $file; ?>" data-src="<?php echo $file; ?>" >
                                                                                                                                
                                                                                                <a href="#">
                                                                                                    <img class="img-responsive center img-thumbnail" alt="Foto Tes" src="<?php echo $file; ?>" >
                                                                                                                                    
                                                                                                </a>
                                                                                    </li>


                                                                          <?php
                                                                                }else{
                                                                                    
                                                                                    ?>
                                                                                
                                                                                    <li  data-responsive="" data-src="" data-sub-html=''>
                                                                                                                                
                                                                                                <center><a href="#">
                                                                                                Peserta belum merekam foto  awal                                  
                                                                                                </a></center>
                                                                                    </li> 
                                                                        <?php 


                                                                                }

                                                                                

                           
                                                                           if($data->foto !=""){

                                                                              $foto = explode(",",$data->foto);
                                                                                foreach($foto as $rp){
                                                                                    if($rp !=""){
 
																				$file    = base_url()."__statics/upload/".$rp;
                                                                                ?>
                                                                                
                                                                                <li   data-responsive="<?php echo $file; ?>" data-src="<?php echo $file; ?>" data-sub-html=''>
                                                                                                                            
                                                                                            <a href="#">
                                                                                                <img class="img-responsive center img-thumbnail" alt="Foto Peserta" src="<?php echo $file; ?>" >
                                                                                                                                
                                                                                            </a>
                                                                                </li> 
                                                                            <?php 
                                                                                    }
                                                                                }
                                                                           }else{

                                                                            ?>
                                                                                
                                                                            <li   data-responsive="" data-src="" data-sub-html=''>
                                                                                                                        
                                                                                        <center><a href="#">
                                                                                          Foto pelaksanaan ujian peserta belum tersedia                                   
                                                                                        </a></center>
                                                                            </li> 
                                                                        <?php 
                                                                           }
                                                                        ?>

                                                                           
                                                                </ul>
        </div>
       
     
                      
                				</div>
                </div>
            </div>
            <!-- /.card -->

			
            <!-- /.card -->
            
        </div>
        <div class="col-md-7">
            <!-- Default card -->
            <div class="card">
                <div class="card-header with-border">
                    <h3 class="card-title">Teguran ke Peserta</h3>
                </div>
                <div class="card-body">
                    
                   

                    <div class="row clearfix">
                                 <div class="col-sm-12">
                                    <div class="form-group">
                                        <div class="form-line">
										
                                        <input type="text" class="form-control" id="option-teguran"  placeholder="Ketik Teguran disini .. " >
                                        </div>
                                    </div>
                                    
                                 </div>
                     </div>


                    <button id="btn-teguran"  class="btn btn-warning btn-social pull-right"><i class="fa fa-warning"></i> Tegur Peserta</button>
                </div>
            </div>
            <div class="card">
                <div class="card-header with-border">
                    <h3 class="card-title">Berita Acara Ujian Peserta</h3>
                </div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <tr>
                            <th style="border: 1px solid black" class="active">1. Nomor Peserta</th>
                            <td style="border: 1px solid black"> <?php echo $peserta->no_test; ?> </td>
                        </tr>
                        <tr>
                            <th style="border: 1px solid black" class="active">2. Nama Peserta</th>
                            <td style="border: 1px solid black"><?php echo $peserta->nama; ?> </td>
                        </tr>
                       
                    </table>
                        
                    <div class="table-responsive">
                    <table class="table table-bordered table-hover tanle-bordered ">
                        <thead>
                            <tr>
                                <th> Teguran </th>
                                <th> Status </th>
                                <th> Waktu </th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php 
                         $dataTeguran = $this->db->get_where("teguran",array("tmsiswa_id"=>$peserta->id,"ujian_id"=>$data->id))->result();
                           foreach($dataTeguran as $rt){
                            ?>


                        <tr>
                            <th class="active"> <?php echo $rt->teguran; ?></th>
                            <th><small> <?php echo ($rt->status==1) ? "Belum dibaca" :"Sudah dibaca"; ?></small></th>
                            <td> <?php echo $this->Reff->formattimestamp($rt->tgl); ?> </td>
                        </tr>

                        <?php

                           }
                        ?>
                        </tbody>
                       
                       
                       
                    </table>
                        </div>
                    <br>
                        
                </div>
                <!-- /.card-body -->
            </div>
         
        </div>	
    </div>
</div>


<script>

    
			$(document).off("click",".forcestop").on("click",".forcestop",function(){
				  
				  var tmsiswa_id = $(this).attr("tmsiswa_id");
				  var tmujian_id = $(this).attr("tmujian_id");

				  Swal.fire({
					title: 'Apakah Anda yakin  ?',
					text: "Peserta akan dikeluarkan dari pelaksanaan ujian dan tidak dapat melanjutkan ujian",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Ya, Keluarkan Peserta ',
					cancelButtonText: 'Tidak '
					}).then((result) => {
					if (result.value) {
						

						 $.post("<?php echo site_url('pengawasan/forcestop'); ?>",{tmsiswa_id:tmsiswa_id,tmujian_id:tmujian_id},function(data){
		 
							Swal.fire(
							'Keterangan !',
							'Peserta sudah dikeluarkan dari pelaksnaan CBT, Jika ingin memberikan akses kembali kepada peserta silahkan klik tombol reset.',
							'success'
							);
							location.reload();

							})

						
						
						
					
						
						
						
					}
					})


			   
				
					 
				   
				 });
			

				 
				 $(document).off("click",".resetujian").on("click",".resetujian",function(){
				  
				  var tmujian_id = $(this).attr("tmujian_id");
				

				  Swal.fire({
					title: 'Apakah Anda yakin  ?',
					text: "Anda akan melakukan reset ujian peserta ini. Jangan Khawatir, Reset ini hanya akan mengaktifkan kembali paket ujian agar dapat dilanjutkan dan  hasil jawaban Peserta sebelumnya tidak akan hilang, ",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Ya, Reset Ujian ',
					cancelButtonText: 'Tidak '
					}).then((result) => {
					if (result.value) {
						

						 $.post("<?php echo site_url('pengawasan/resetujian'); ?>",{tmujian_id:tmujian_id},function(data){
		 
							Swal.fire(
							'Keterangan !',
							'Reset Ujian berhasil dilakukan',
							'success'
							);
							location.reload();

							})

						
						
						
					
						
						
						
					}
					})


			   
				
					 
				   
				 });


                 $(document).off("click","#btn-teguran").on("click","#btn-teguran",function(){
				  
				  var tmujian_id =  "<?php echo $data->id; ?>";
				  var tmsiswa_id =  "<?php echo $peserta->id; ?>";
				  var teguran    =  $("#option-teguran").val();
				
                  if(tmujian_id==""){
                    Swal.fire(
                        'Keterangan !',
                        'Peserta belum memulai ujian',
                        'success'
                        );
                      return false;
                  }

				  Swal.fire({
					title: 'Apakah Anda yakin akan menegur Peserta  ?',
					html: "Peserta akan mendapatkan notifikasi teguran <br> <b>"+teguran+"</b>",
					type: 'warning',
					showCancelButton: true,
					confirmButtonColor: '#3085d6',
					cancelButtonColor: '#d33',
					confirmButtonText: 'Ya, Berikan Teguran ',
					cancelButtonText: 'Tidak '
					}).then((result) => {
					if (result.value) {
						

						 $.post("<?php echo site_url('pengawasan/teguran'); ?>",{tmujian_id:tmujian_id,tmsiswa_id:tmsiswa_id,teguran:teguran},function(data){
		 
							Swal.fire(
							'Keterangan !',
							'Peserta diberikan teguran',
							'success'
							);
							location.reload();

							})

						
						
						
					
						
						
						
					}
					})


			   
				
					 
				   
				 });
</script>
<script type="text/javascript">
        $(document).ready(function(){
            $('#lightgallery').lightGallery();
          
        });
        </script>