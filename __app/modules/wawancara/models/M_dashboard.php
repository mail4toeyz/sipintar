<?php

class M_dashboard extends CI_Model {
 
    public function __construct() {
        parent::__construct();
    }

    
	
   
	

    public function getUjianById($id)
    {
        $this->db->select('*');
        $this->db->from('tm_ujian_wawancara a');
      
        $this->db->where('a.id', $id);
        return $this->db->get()->row();
    }
	
	 public function getSoal($id)
    {
        $ujian = $this->getUjianById($id);
        //$order = $ujian->jenis==="acak" ? 'rand()' : 'urutan';

        $this->db->select('id as id_soal, soal,opsi_a, opsi_b, opsi_c, opsi_d, opsi_e, jawaban,jenis');
        $this->db->from('tm_wawancara');
  
        $this->db->where('tmujian_id', $id);
        $this->db->order_by("urutan","asc");
        
        return $this->db->get()->result();
    }
	
	 public function HslUjian($id, $tmsiswa_id)
    {
        $this->db->select('*, UNIX_TIMESTAMP(tgl_selesai) as waktu_habis');
        $this->db->from('h_ujian_w');
        $this->db->where('tmujian_id', $id);
        $this->db->where('tmsiswa_id', $tmsiswa_id);
        return $this->db->get();
    }
	
	
	  public function ambilSoal($pc_urut_soal1, $pc_urut_soal_arr)
    {
        $this->db->select("*,id as id_soal, {$pc_urut_soal1} AS jawaban");
        $this->db->from('tm_wawancara');
        $this->db->where('id', $pc_urut_soal_arr);
        return $this->db->get()->row();
    }
	
	  public function getJawaban($id_tes)
    {
        $this->db->select('list_jawaban');
        $this->db->from('h_ujian_w');
        $this->db->where('id', $id_tes);
        return $this->db->get()->row()->list_jawaban;
    }
	
	 public function getSoalById($id)
    {
        return $this->db->get_where('tm_wawancara', ['id' => $id])->row();
    }
	
	
	
}
