<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Referensi extends CI_Controller {

	 public function __construct()
      {
        parent::__construct();
		 
		  isLogin();
		  $this->load->model('M_referensi','m');
		
	  }
	  
   function _template($data)
	{
	  $this->load->view('admin/page_header',$data);	
	}
		
	public function sekertariat()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		
		 $data['title']   = "Data Sekertariat  ";
	     if(!empty($ajax)){
					    
			 $this->load->view('sekertariat/page',$data);
		
		 }else{
			 
		     $data['konten'] = "sekertariat/page";
			 
			 $this->_template($data);
		 }
	

	}
	
	 public function gridSekertariat(){
		
		  $iTotalRecords = $this->m->gridSekertariat(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->gridSekertariat(true)->result_array();
		   
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
			    
			   
				$no = $i++;
				$records["data"][] = array(
					$no,
			
              	
				
					$val['nama'],
					$val['nip'],
					$val['golongan'],
					$val['jabatan'],
					$val['kategori'],
					
					' 
					<div class="btn-group" role="group">
					<button type="button" class="btn btn-success btn-sm ubahmodal" datanya="'.$val['id'].'" urlnya="'.site_url("referensi/formSekertariat").'" target="#loadform" data-toggle="modal" data-target="#defaultModal">
                                    <i class="fa fa-pencil"></i>
                      </button>
					  <button type="button"  class="btn btn-success btn-sm hapus" datanya="'.$val['id'].'" urlnya="'.site_url("referensi/hapusSekertariat").'">
                                    <i class="fa fa-trash"></i>
					  </button>
					  </div>
					 
					  '
				  
				    

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
	
	public function form(){
		
		$id = $this->input->get_post("id");
		$data = array();
		   if(!empty($id)){
			   
			  $data['data']  = $this->Reff->get_where("tm_kategori",array("id"=>$id));
			   
		   }
		$this->load->view("form",$data);
		
	}
	
	
	
	public function save(){
     
        $this->form_validation->set_message('required', '{field} Wajib diisi. ');
         $this->form_validation->set_message('is_unique', '{field} Sudah terdaftar , silahkan masukan NISN lain.');
			    $this->form_validation->set_message('min_length', '%s: Minimal  %s digit');
         $this->form_validation->set_message('max_length', '%s: Maksimal  %s digit');
    
		 
			
				$config = array(
				    
				   
				    array('field' => 'f[nama]', 'label' => 'Nama Kategori  ', 'rules' => 'trim|required'),
				  
				   
			
				   
				  
				   
				   
	
					
				);
				
				$this->form_validation->set_rules($config);	
        
        if ($this->form_validation->run() == true) {
			  
			    $id = $this->input->get_post("id",true);
			    $f  = xssArray($this->input->get_post("f",true));
				
				
				 
				
							 if(empty($id)){
								
								 
								   $this->m->insert();
								   echo "Data Berhasil disimpan";	
								 
								
								
							 }else{
								$this->m->update($id); 
								echo "Data Berhasil disimpan";	
								 
							 }
							 
						
							
						
		
		
							
			     
			    
												 
	     } else {
	         
           
                header('Content-Type: application/json');
                echo json_encode(array('error' => true, 'message' => validation_errors()));
            
        }

		
	
	
	}

	public function hapus(){
		
		$id = $this->input->get_post("id",true);
		
		$this->db->delete("tm_kategori",array("id"=>$id));
		echo "sukses";
	}
	

	// Auditor

	public function auditor()
	{  
	    
		  
		  
	    		
         $ajax            = $this->input->get_post("ajax",true);	
		
		 $data['title']   = "Data Auditor  ";
	     if(!empty($ajax)){
					    
			 $this->load->view('auditor/page',$data);
		
		 }else{
			 
		     $data['konten'] = "auditor/page";
			 
			 $this->_template($data);
		 }
	

	}
	
	 public function gridAuditor(){
		
		  $iTotalRecords = $this->m->gridAuditor(false)->num_rows();
		  
		  $iDisplayLength = intval($_REQUEST['length']);
		  $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength; 
		  $iDisplayStart = intval($_REQUEST['start']);
		  $sEcho = intval($_REQUEST['draw']);
		  
		  $records = array();
		  $records["data"] = array(); 

		  $end = $iDisplayStart + $iDisplayLength;
		  $end = $end > $iTotalRecords ? $iTotalRecords : $end;
		  
		  $datagrid = $this->m->gridAuditor(true)->result_array();
		   
		   $i= ($iDisplayStart +1);
		   foreach($datagrid as $val) {
			    
			   
				$no = $i++;
				$records["data"][] = array(
					$no,
			
              	
				
					$val['nama'],
					$val['nip'],
					$val['golongan'],
					$val['jabatan'],
					$val['bagian'],
					
					' 
					<div class="btn-group" role="group">
					<button type="button" class="btn btn-success btn-sm ubahmodal" datanya="'.$val['id'].'" urlnya="'.site_url("referensi/formSekertariat").'" target="#loadform" data-toggle="modal" data-target="#defaultModal">
                                    <i class="fa fa-pencil"></i>
                      </button>
					  <button type="button"  class="btn btn-success btn-sm hapus" datanya="'.$val['id'].'" urlnya="'.site_url("referensi/hapusSekertariat").'">
                                    <i class="fa fa-trash"></i>
					  </button>
					  </div>
					 
					  '
				  
				    

				  );
			  }
		
		  $records["draw"] = $sEcho;
		  $records["recordsTotal"] = $iTotalRecords;
		  $records["recordsFiltered"] = $iTotalRecords;
		  
		  echo json_encode($records);
	}
	
	 
}
