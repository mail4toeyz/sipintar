</section>
<!-- /.content -->
</div>
<!-- /.container -->
</div>
<!-- /.content-wrapper -->
<footer class="main-footer">
	<div class="container">
	    <div style="float:left"> 2021 - <?php echo $this->Reff->owner(); ?></div>
		<div style="float:right"><a href="https://www.instagram.com/dndnrzizz/" target="_blank"> Dikembangkan oleh GTK Madrasah </a></div>
		
	</div>
	<!-- /.container -->
</footer>
</div>
<!-- ./wrapper -->

<script src="<?php echo base_url() ?>__statics/cbt/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>__statics/cbt/dist/js/adminlte.min.js"></script>
<script src="<?php echo base_url() ?>__statics/cbt/bower_components/pace/pace.min.js"></script>

</body>

</html>


<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Butuh Bantuan ? Silahkan sampaikan kepanitia </h4>
      </div>
      <div class="modal-body" id="loadbody">
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>


<script type="text/javascript">
  $(document).off("click",".bantuan").on("click",".bantuan",function(){

    $.post("<?php echo site_url('cbt/bantuan'); ?>",function(data){


        $("#loadbody").html(data);
    })


  })

</script>